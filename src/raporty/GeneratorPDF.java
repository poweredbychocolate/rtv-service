package raporty;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.czynnosci.Diagnoza;
import zlecenia.czynnosci.Naprawa;

/**
 * Klasa generujaca prosty dokument PDF na podstawie wybranych danych
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class GeneratorPDF {
	private Raport raport;
	private String nazwa;
	private String sciezka = "";
	PDDocument nowyPDF;
	PDPage strona;
	PDPageContentStream zawartosc;
	TextPDF przetwornik = new TextPDF();

	/**
	 * Tworzy prosty plik PDF na podstawie zbudowanego{@link raporty.Raport}
	 * 
	 * @param raport
	 *            Zbudowany raport
	 */
	public GeneratorPDF(Raport raport) {
		this.raport = raport;
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		this.nazwa = format.format(date) + "_"
				+ przetwornik.zmienNaASCII(raport.getZlecenie().getZlecenieId().toString()) + "_"
				+ przetwornik.zmienNaASCII(raport.getZlecenie().getKlienta().getNazwisko());
		this.sciezka = new String(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop");
	}

	/**
	 * Nazwa z jaka zostanie zapisany plik
	 * 
	 * @param nazwa
	 *            Nazwa pliku
	 */
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa + ".pdf";
	}

	/**
	 * Lokalizacja pliku
	 * 
	 * @param sciezka
	 *            Sciezka zapisu pliku
	 */
	public void setSciezka(String sciezka) {
		this.sciezka = sciezka + System.getProperty("file.separator");
	}

	/**
	 * generuje plik pdf
	 * 
	 * @return Stan wykonej operacji
	 */
	public Boolean budujPDF() {
		Boolean b = false;
		nowyPDF = new PDDocument();
		PDDocumentInformation infoPDF = nowyPDF.getDocumentInformation();
		String string = new String(przetwornik.zmienNaASCII(this.raport.getPracownik().getImie()) + " "
				+ przetwornik.zmienNaASCII(this.raport.getPracownik().getNazwisko()));
		infoPDF.setAuthor(string);
		infoPDF.setTitle(this.nazwa);
		strona = new PDPage();
		///
		try {
			b = true;
			wypelnijStrone();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}
		///
		nowyPDF.addPage(strona);
		return b;
	}

	/**
	 * Zapisanie pliku
	 * 
	 * @return Stan Operacji
	 */
	public Boolean zapiszPDF() {
		Boolean b = false;
		try {
			nowyPDF.save(this.sciezka + this.nazwa);
			nowyPDF.close();
			b = true;
		} catch (IOException e) {
			return false;

		}
		return b;
	}

	private void wypelnijStrone() throws IOException {

		Integer pozycjaX = 300;
		Integer pozycjaY = 775;
		Integer rozmiar = 18;
		Integer krok = rozmiar + 2;
		Integer wiekszyRozmiar = 22;
		Integer wiekszyKrok = wiekszyRozmiar + 2;
		try {
			zawartosc = new PDPageContentStream(nowyPDF, strona);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		////
		zawartosc.beginText();

		// Wstawienie miejscowosci i daty
		zawartosc.setFont(PDType1Font.TIMES_ROMAN, rozmiar);
		zawartosc.newLineAtOffset(pozycjaX, pozycjaY);
		zawartosc.showText(przetwornik.zmienNaASCII(raport.getData()));
		zawartosc.newLineAtOffset(-pozycjaX, 0);

		/// wstawianie naglowka
		ArrayList<String> naglowek = przetwornik
				.potnijString(przetwornik.zmienNaASCII(przetwornik.zmienNaASCII(raport.getNaglowek())));
		pozycjaX = 20;
		zawartosc.newLineAtOffset(pozycjaX, 0);
		for (String s : naglowek) {
			zawartosc.showText(s);
			zawartosc.newLineAtOffset(0, -krok);
		}
		// wstawianie zlecanie klienta i urzadzenia
		zawartosc.setFont(PDType1Font.TIMES_ROMAN, wiekszyRozmiar);
		zawartosc.newLineAtOffset(-pozycjaX, 0);
		pozycjaX = 180;
		zawartosc.newLineAtOffset(pozycjaX, -wiekszyKrok);
		String string = new String("Zlecenie nr. " + raport.getZlecenie().getZlecenieId().toString());
		zawartosc.showText(string);
		////
		zawartosc.setFont(PDType1Font.TIMES_ROMAN, rozmiar);
		zawartosc.newLineAtOffset(0, -krok);
		string = new String(przetwornik.zmienNaASCII(raport.getZlecenie().getKlienta().getImie()) + "    "
				+ przetwornik.zmienNaASCII(raport.getZlecenie().getKlienta().getNazwisko()));
		zawartosc.showText(string);
		zawartosc.newLineAtOffset(0, -krok);
		string = new String(przetwornik.zmienNaASCII(raport.getZlecenie().getUrzadzenie().getMarka()) + "    "
				+ przetwornik.zmienNaASCII(raport.getZlecenie().getUrzadzenie().getModel()));
		zawartosc.showText(string);
		zawartosc.newLineAtOffset(-pozycjaX, -krok);
		///
		// tu wstawianie czynnosci
		Czynnosci czynnosci = raport.getCzynnosci();
		pozycjaX = 20;
		zawartosc.newLineAtOffset(pozycjaX, 0);

		Integer tmpx = 0, wierszX = 50, liczba = 1;
		zawartosc.newLineAtOffset(0, -krok);
		string = new String("L.P. ");
		zawartosc.showText(string);

		zawartosc.newLineAtOffset(0, -krok);
		for (int i = 0; i < czynnosci.iloscCzynosci(); i++) {
			if (czynnosci.getCzynnosc(i) instanceof Diagnoza) {
				tmpx = 0;
				wierszX = 30;
				string = new String((liczba++) + ". ");
				zawartosc.showText(string);
				zawartosc.newLineAtOffset(wierszX, 0);
				tmpx += wierszX;
				string = przetwornik.zmienNaASCII(czynnosci.getCzynnosc(i).getOpis());
				if (string.length() > 60)
					string = string.substring(0, 60);
				zawartosc.showText(string);
				// zawartosc.newLineAtOffset(wierszX,0);
				///
				zawartosc.newLineAtOffset(-tmpx, -krok);
			}
		}
		zawartosc.newLineAtOffset(0, -krok);
		for (int i = 0; i < czynnosci.iloscCzynosci(); i++) {
			if (czynnosci.getCzynnosc(i) instanceof Naprawa) {
				tmpx = 0;
				wierszX = 30;
				string = new String((liczba++) + ". ");
				zawartosc.showText(string);
				zawartosc.newLineAtOffset(wierszX, 0);
				tmpx += wierszX;
				string = przetwornik.zmienNaASCII(czynnosci.getCzynnosc(i).getOpis());
				if (string.length() > 60)
					string = string.substring(0, 60);
				zawartosc.showText(string);
				wierszX = 500;
				zawartosc.newLineAtOffset(wierszX, 0);
				tmpx += wierszX;
				string = new String(czynnosci.getCzynnosc(i).getKoszt().toString());
				zawartosc.showText(string);
				///
				zawartosc.newLineAtOffset(-tmpx, -krok);
			}
		}
		///
		zawartosc.endText();
		////

		zawartosc.close();
	}

}
