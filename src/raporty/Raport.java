package raporty;

import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;

/**
 * Klasa raportu
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Raport {
	private String naglowek, data;
	private Zlecenie zlecenie;
	private Pracownik pracownik;
	private Czynnosci czynnosci;

	public Raport() {
	}

	public String getNaglowek() {
		return naglowek;
	}

	public void setNaglowek(String naglowek) {
		this.naglowek = naglowek;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Zlecenie getZlecenie() {
		return zlecenie;
	}

	public Klient getKlient() {
		return zlecenie.getKlienta();
	}

	public void setZlecenie(Zlecenie zlecenie) {
		this.zlecenie = zlecenie;
	}

	public Pracownik getPracownik() {
		return pracownik;
	}

	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}

	public Czynnosci getCzynnosci() {
		return czynnosci;
	}

	public void setCzynnosci(Czynnosci czynnosci) {
		this.czynnosci = czynnosci;
	}

}
