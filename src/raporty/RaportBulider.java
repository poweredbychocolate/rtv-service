package raporty;

import java.text.SimpleDateFormat;
import java.util.Date;

import Uzytkownicy.Pracownik;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;

/**
 * Klasa sluzaca do generowania {@link raporty.Raport} na podstawie przekaznych
 * danych
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class RaportBulider {
	private Raport raport;

	public RaportBulider() {
		this.raport = new Raport();
	}

	/**
	 * Zwaraca zbudowany raport {@link raporty.Raport}
	 * 
	 * @return Zbudowany raport
	 */
	public Raport getRaport() {
		return raport;
	}

	public void setNaglowek(String naglowek) {
		this.raport.setNaglowek(naglowek);
	}

	public void setData(String data) {
		this.raport.setData(data);
	}

	public void setData() {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		this.raport.setData(format.format(date));
	}

	public void setZlecenie(Zlecenie zlecenie) {
		this.raport.setZlecenie(zlecenie);
	}

	public void setPracownik(Pracownik pracownik) {
		this.raport.setPracownik(pracownik);
	}

	public void setCzynnosci(Czynnosci czynnosci) {
		this.raport.setCzynnosci(czynnosci);
	}
}
