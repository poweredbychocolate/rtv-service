package raporty;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Prosta klasa pozwala na zamiane polskich znakow diakrytycznych na znaki kodowanie kodem ASCII
 *  
 * @author Dawid Brelak
 * @version 1.0
 */
public class TextPDF {
	private HashMap<String, String> tablicaZnakow;

	public TextPDF() {
		tablicaZnakow = new HashMap<String, String>(18);
		initTablicaZnakow();
	}

	private void initTablicaZnakow() {
		tablicaZnakow.put("�", "a");
		tablicaZnakow.put("�", "c");
		tablicaZnakow.put("�", "e");
		tablicaZnakow.put("�", "l");
		tablicaZnakow.put("�", "n");
		tablicaZnakow.put("�", "o");
		tablicaZnakow.put("�", "s");
		tablicaZnakow.put("�", "z");
		tablicaZnakow.put("�", "z");
		tablicaZnakow.put("�", "A");
		tablicaZnakow.put("�", "C");
		tablicaZnakow.put("�", "E");
		tablicaZnakow.put("�", "L");
		tablicaZnakow.put("�", "N");
		tablicaZnakow.put("�", "O");
		tablicaZnakow.put("�", "S");
		tablicaZnakow.put("�", "Z");
		tablicaZnakow.put("�", "Z");
	}
	/**
	 * Zamienia znaki na zaniaki w kodzie ascii
	 * @param string ciag znakow do przetworzenia
	 * @return String wyjsciowy ciag znakow 
	 */
	public String zmienNaASCII(String string) {
		String s1 =string;
		String s2 = new String();
		for(int i=0;i<s1.length();i++)
		{
			String tmp = s1.substring(i, i+1);
			String pob;
			if((pob=tablicaZnakow.get(tmp))==null)
			s2+=tmp;
			else
			s2+=pob;	
		}
		return s2;
	}
/**
 * Dzieli ciag zanakow na mniejsze ciagi znakow jezeli ciaga wesciowy zawiera zanaki nowej lini 
 * @param naglowek ciag wejsciowy
 * @return lista podzielonych ciagow znakow 
 */
	public ArrayList<String> potnijString(String naglowek)
	{
		String ciety =naglowek;
		String kawalek=new String();
		char znak;
		ArrayList<String> kawalki = new ArrayList<String>();
		for(int i=0;i<ciety.length();i++)
		{
			znak=ciety.charAt(i);
			if(znak=='\n')		{
				kawalki.add(kawalek);
				kawalek= new String();
			}
			if(znak=='\r'){
				kawalki.add(kawalek);
				kawalek= new String();
				i++;
			}				
			if((znak!='\n')&&(znak!='\r')) kawalek+=znak;
		}
		return kawalki;
	}

}
