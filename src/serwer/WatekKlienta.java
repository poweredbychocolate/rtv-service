package serwer;

import java.io.IOException;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import Uzytkownicy.UzytkownikSystemu;
import cennik.Cennik;
import cennik.PozycjaCennika;
import database.DBMenadzer;
import database.DBMenadzerProxy;
import siec.GniazdoSerwera;
import siec.Komenda;
import zlecenia.Urzadzenie;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.statusy.Status;

/**
 * Klasa obslugi klienta podlaczonego do serwera {@link serwer.Serwer}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class WatekKlienta extends Thread {
	private Socket klientSocket = null;
	private Komenda komenda = null;
	private GniazdoSerwera gniazdo = null;
	private DBMenadzer bazadanych = null;
	private DBMenadzerProxy bazaProxy = null;

	/**
	 * Tworzy {@link serwer.WatekKlienta}
	 * 
	 * @param klientSocket
	 *            gniazdo podlaczonego klienta
	 */
	public WatekKlienta(Socket klientSocket) {
		this.klientSocket = klientSocket;
		try {
			this.bazadanych = DBMenadzer.getInstance();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			this.gniazdo = new GniazdoSerwera(klientSocket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (!this.klientSocket.isClosed()) {
			try {
				komenda = gniazdo.odczytajKomende();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			obslugaKomend();
		} // glowna petla
	}// end run
	
	private void obslugaKomend() {
		if (this.komenda.getOperacja().equals(Komenda.ZAMKNIJ)) {
			zamkijPolaczenie();
		} /// Zamknij
		else if (this.komenda.getOperacja().equals(Komenda.SPRAWDZ_UZYTKOWNIKA)) {
			sprawdzUzytkownika();
		} else if (this.komenda.getOperacja().equals(Komenda.LOGOWANIE)) {
			sprawdzDaneLogowaia();
		} else if (this.komenda.getOperacja().equals(Komenda.WCZYTAJ_KLIENTA)) {
			wczytajKlienta();
		} else if (this.komenda.getOperacja().equals(Komenda.WCZYTAJ_UZYTKOWNIKA)) {
			wczytajUzytkownika();
		} else if (this.komenda.getOperacja().equals(Komenda.WCZYTAJ_PRACOWNIKA)) {
			wczytajPracownika();
		} else if (this.komenda.getOperacja().equals(Komenda.LISTA_PRACOWNIKOW)) {
			listaPracownikow();
		} else if (this.komenda.getOperacja().equals(Komenda.DODAJ_PRACOWNIKA)) {
			dodajPracownika();
		} else if (this.komenda.getOperacja().equals(Komenda.DODAJ_KLIENTA)) {
			dodajKlienta();
		} else if (this.komenda.getOperacja().equals(Komenda.RESET_LOGOWANIA)) {
			resetKlienta();
		} else if (this.komenda.getOperacja().equals(Komenda.DODAJ_STATUS)) {
			dodajStatus();
		} else if (this.komenda.getOperacja().equals(Komenda.LISTA_KLIENTOW)) {
			listaKlientow();
		} else if (this.komenda.getOperacja().equals(Komenda.DODAJ_URZADZENIE)) {
			dodajUrzadzenie();
		} else if (this.komenda.getOperacja().equals(Komenda.DODAJ_ZLECENIE)) {
			dodajZlecenie();
		} else if (this.komenda.getOperacja().equals(Komenda.LISTA_ZLECEN_KLIENTA)) {
			listaZlecenKlienta();
		} else if (this.komenda.getOperacja().equals(Komenda.WCZYTAJ_ZLECENIE)) {
			wczytajZlecenie();
		} else if (this.komenda.getOperacja().equals(Komenda.LISTA_ZLECEN)) {
			listaZlecen();
		} else if (this.komenda.getOperacja().equals(Komenda.ZMIEN_STATUS)) {
			zmienStatusZlecena();
		} else if (this.komenda.getOperacja().equals(Komenda.CENNIK_DODAJ)) {
			zapiszPozycjeCennika();
		} else if (this.komenda.getOperacja().equals(Komenda.CENNIK_LISTA)) {
			wyslijCennik();
		} else if (this.komenda.getOperacja().equals(Komenda.PELNA_LISTA_ZLECEN_KLIENTA)) {
			listaZlecen(gniazdo.listaZlecenKlientaOdczytaj());
		} else if (this.komenda.getOperacja().equals(Komenda.ANULUJ_ZLECENIE)) {
			anulujZlecenie();
		} else if (this.komenda.getOperacja().equals(Komenda.CENNIK_USUN)) {
			usunPozycjeCennika();
		} else if (this.komenda.getOperacja().equals(Komenda.STATYSTYKA)) {
			statystyki();
		} else if (this.komenda.getOperacja().equals(Komenda.STATUSY_ZLECENIA)) {
			statusyZlecenia();
		} else if (this.komenda.getOperacja().equals(Komenda.ZAPISZ_CZYNNOSCI)) {
			zapisCzynnosci();
		} else if (this.komenda.getOperacja().equals(Komenda.WCZYTAJ_CZYNNOSCI)) {
			wczytanieCzynnosci();
		} else if (this.komenda.getOperacja().equals(Komenda.LICZBA_ZLECEN)) {
			liczbaZlecen();
		}

		/// else if(this.komenda.getOperacja().equals(Komenda. ))
	}// end obsluga komend

	private synchronized void zamkijPolaczenie() {
		this.gniazdo.dispose();
		this.gniazdo = null;
		try {
			this.klientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Zamknieto : " + klientSocket.getRemoteSocketAddress().toString());
	}

	private synchronized void sprawdzUzytkownika() {
		String[] logowanie = new String[2];
		Boolean status = new Boolean(false);
		logowanie = gniazdo.sprawdzUzytkownikaOdczytaj();
		try {
			status = bazadanych.sprawdzUzytkownika(logowanie[0], logowanie[1]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				this.klientSocket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		gniazdo.sprawdzUzytkownikaZapisz(status);
	}

	private synchronized void sprawdzDaneLogowaia() {
		String[] logowanie = new String[2];
		Integer id = new Integer(0);
		logowanie = gniazdo.sprawdzDaneLogowaniaOdczytaj();
		try {
			id = bazadanych.sprawdzDaneLogowania(logowanie[0], logowanie[1]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				this.klientSocket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		gniazdo.sprawdzDaneLogowaniaZapisz(id);
	}

	private synchronized void wczytajKlienta() {
		Klient klient = null;
		Integer id = new Integer(0);
		id = gniazdo.wczytajKlientaOdczytaj();
		try {
			klient = bazadanych.wczytajKlienta(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				this.klientSocket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		gniazdo.wczytajKlientaZapisz(klient);
	}

	private synchronized void wczytajUzytkownika() {
		Uzytkownik u = null;
		Integer id = new Integer(0);
		id = gniazdo.wczytajUzytkownikaOdczytaj();
		try {
			u = bazadanych.wczytajUzytkownika(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		gniazdo.wczytajUzytkownikaZapisz(u);
	}

	private synchronized void wczytajPracownika() {
		Pracownik p = null;
		Integer id = new Integer(0);
		id = gniazdo.wczytajUzytkownikaOdczytaj();
		try {
			p = bazadanych.wczytajPracownika(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		gniazdo.wczytajPracownikaZapisz(p);
	}

	private synchronized void listaPracownikow() {
		ResultSet rs = null;
		try {
			rs = bazadanych.listaPracownikow();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.listaPracownikow(rs);
	}

	private synchronized void listaKlientow() {
		ResultSet rs = null;
		try {
			rs = bazadanych.listaKlientow();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.listaKlientow(rs);
	}

	private synchronized void dodajPracownika() {
		Boolean b = new Boolean(false);
		UzytkownikSystemu[] us = new UzytkownikSystemu[2];
		us = gniazdo.dodajPracownikaOdczytaj();
		try {
			bazadanych.dodajPracownika((Uzytkownik) us[0], (Pracownik) us[1]);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.dodajPracownikaZapisz(b);
	}

	private synchronized void dodajKlienta() {
		Boolean b = new Boolean(false);
		UzytkownikSystemu[] us = new UzytkownikSystemu[2];
		us = gniazdo.dodajKlientaOdczytaj();
		try {
			bazadanych.dodajKlienta((Uzytkownik) us[0], (Klient) us[1]);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.dodajKlientaZapisz(b);
	}

	private synchronized void resetKlienta() {
		Boolean b = new Boolean(false);
		Klient k = null;
		String[] s = new String[2];
		k = gniazdo.resetKlientaOdczytajKlienta();
		s = gniazdo.resetKlientaOdczytajLogowanie();
		try {
			bazadanych.resetujDaneLogowania(k, s[0], s[1]);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.resetKlientaZapisz(b);
	}

	private synchronized void dodajStatus() {
		Boolean b = new Boolean(false);
		Status status = null;
		status = gniazdo.dodajStatusOdczytaj();
		try {
			bazadanych.dodajStatus(status);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void dodajUrzadzenie() {
		Boolean b = new Boolean(false);
		Urzadzenie urzadzenie = null;
		urzadzenie = gniazdo.dodajUrzadzenieOdczytaj();
		try {
			bazadanych.dodajUrzadzenie(urzadzenie);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void dodajZlecenie() {
		Boolean b = new Boolean(false);
		Zlecenie zlecenie = null;
		zlecenie = gniazdo.dodajZlecenieOdczytaj();
		try {
			bazadanych.dodajZlecenie(zlecenie);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			b = false;
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void listaZlecenKlienta() {
		ResultSet rs = null;
		Klient k = gniazdo.listaZlecenKlientaOdczytaj();
		try {
			rs = bazadanych.listaZlecenKlienta(k);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.listaZlecenKlientaZapisz(rs);
	}

	private synchronized void wczytajZlecenie() {

		Integer id = gniazdo.wczytajZlecenieOdczytaj();
		Zlecenie zl = null;
		try {
			zl = bazadanych.wczytajZlecenie(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.wczytajZlecenieZapisz(zl);
	}

	private synchronized void listaZlecen() {
		try {
			gniazdo.listaZlecenZapisz(bazadanych.listaZlecenAL(bazadanych.listaZlecenRS()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private synchronized void listaZlecen(Klient klient) {
		try {
			bazaProxy = new DBMenadzerProxy(klient);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			gniazdo.listaZlecenZapisz(bazaProxy.listaZlecen());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private synchronized void zmienStatusZlecena() {
		Boolean b = new Boolean(false);
		Zlecenie zlecenie = null;
		zlecenie = gniazdo.dodajZlecenieOdczytaj();
		Status status = null;
		status = gniazdo.dodajStatusOdczytaj();
		try {
			bazadanych.zmienStatusZlecenia(zlecenie, status);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void zapiszPozycjeCennika() {

		Boolean b = false;
		PozycjaCennika pz = gniazdo.wczytajPozycjeCennika();
		try {
			bazadanych.dodajDoCennika(pz);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void wyslijCennik() {
		Cennik c = null;
		try {
			c = bazadanych.cennik();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.wyslijCennik(c);

	}

	private synchronized void anulujZlecenie() {
		Boolean b = new Boolean(false);
		Klient klient = gniazdo.listaZlecenKlientaOdczytaj();
		Zlecenie zlecenie = gniazdo.dodajZlecenieOdczytaj();

		try {
			bazaProxy = new DBMenadzerProxy(klient);
			bazaProxy.AnulujZlecenie(zlecenie);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void usunPozycjeCennika() {
		Boolean b = new Boolean(false);
		PozycjaCennika pc = gniazdo.wczytajPozycjeCennika();
		try {
			bazadanych.usunPozycjecennika(pc);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszStanOperacji(b);
	}

	private synchronized void statystyki() {
		Integer typ = 0;
		ResultSet rs = null;
		typ = gniazdo.statystykaOdczytaj();
		try {
			rs = bazadanych.statystyki(typ);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.statystykaZapisz(rs, typ);
	}

	private synchronized void statusyZlecenia() {
		Integer id = 0;
		ArrayList<Status> lista = null;
		id = gniazdo.statusyZleceniaOdczytaj();
		try {
			lista = bazadanych.wczytajStausy(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.statusyZleceniaZapisz(lista);
	}

	private synchronized void zapisCzynnosci() {
		Boolean b = false;
		Integer id = 0;
		Czynnosci cz = null;
		try {
			id = gniazdo.wczytajInt();
			cz = gniazdo.wczytajCzynnosci();
			bazadanych.zapiszCzynnosci(cz, id);
			b = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszStanOperacji(b);

	}

	private synchronized void wczytanieCzynnosci() {
		Integer id = 0;
		ResultSet cz = null;
		try {
			id = gniazdo.wczytajInt();
			cz = bazadanych.wczytajCzynnosci(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gniazdo.zapiszCzynnosci(cz);

	}

	private synchronized void liczbaZlecen() {
		Integer liczba = 0;
		Klient kl = null;
		try {
			kl = gniazdo.listaZlecenKlientaOdczytaj();
			liczba = bazadanych.wczytajLiczbeZlecen(kl);
			gniazdo.zapiszInt(liczba);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}// end Watekklienta
