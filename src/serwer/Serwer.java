package serwer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Glowna klasa serwera oczekuje na polaczenie, tworzy nowy watek dla
 * klienta{@link serwer.WatekKlienta}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Serwer {
	/**
	 * Wybrany port nasluchu
	 */
	private static final Integer PORT = 5555;

	private static ServerSocket serwerSocket;
	private static Socket klientSocket;

	private static ExecutorService watkiSerwera;

	public static void main(String[] args) {
		watkiSerwera = Executors.newCachedThreadPool();
		try {
			serwerSocket = new ServerSocket(PORT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Serwer Start");
		while (true) {
			try {
				klientSocket = serwerSocket.accept();
				System.out.println("Podlaczono : " + klientSocket.getRemoteSocketAddress().toString());
				watkiSerwera.execute(new WatekKlienta(klientSocket));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
