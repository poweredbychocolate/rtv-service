package statystyki;

import java.util.ArrayList;

/**
 * Interfejs definujacy podstawowe metody obslugi statystyk
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public interface Statystyka {

	public void dodaj(ElementStatystyki element);

	public void dodaj(ArrayList<ElementStatystyki> elenenty);

	public ElementStatystyki get(Integer pozycja);

	public ArrayList<ElementStatystyki> get();

	public String getDataWygenerowania();

	public void setDataWygenerowania();

	public Integer LiczbaElementow();
}
