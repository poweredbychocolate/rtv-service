package statystyki;

import java.io.Serializable;

/**
 * Postawowa klasa bedaca pojedynczym elementem statystyki
 * 
 * @author Dawid Brelak
 * @version 1.0
 *
 */
public class ElementStatystyki implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nazwa;
	private Double wartosc;
	public ElementStatystyki() {		
	}
	public ElementStatystyki(String nazwa,Double wartosc) {	
		this.nazwa=nazwa;
		this.wartosc=wartosc;
	}
	public void setNazwa(String nazwa)
	{
		this.nazwa=nazwa;
	}
	public void setWartosc(Double wartosc)
	{
		this.wartosc=wartosc;
	}
	public String getNawa()
	{
		return this.nazwa;
	}
	public Double getWartosc()
	{
		return this.wartosc;
	}
}
