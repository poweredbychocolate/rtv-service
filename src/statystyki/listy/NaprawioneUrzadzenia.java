package statystyki.listy;

import java.io.Serializable;
import java.util.ArrayList;

import klientGUI.KlientDane;
import statystyki.ElementStatystyki;
import statystyki.Statystyka;

/**
 * Wlasciwa {@link statystyki.Statystyka} - liczba naprawaiych urzadzen wedlug
 * marki
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class NaprawioneUrzadzenia implements Serializable, Statystyka {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<ElementStatystyki> lista;
	private String dataWygenerowania;

	public NaprawioneUrzadzenia() {
		lista = new ArrayList<ElementStatystyki>();
	}

	@Override
	public void dodaj(ElementStatystyki element) {
		lista.add(element);

	}

	@Override
	public void dodaj(ArrayList<ElementStatystyki> elenenty) {
		lista.addAll(elenenty);

	}

	@Override
	public ElementStatystyki get(Integer pozycja) {
		// TODO Auto-generated method stub
		return lista.get(pozycja);
	}

	@Override
	public ArrayList<ElementStatystyki> get() {
		// TODO Auto-generated method stub
		return lista;
	}

	@Override
	public String getDataWygenerowania() {
		// TODO Auto-generated method stub
		return this.dataWygenerowania;
	}

	@Override
	public Integer LiczbaElementow() {
		// TODO Auto-generated method stub
		return lista.size();
	}

	@Override
	public void setDataWygenerowania() {
		dataWygenerowania = KlientDane.getBiezacyCzas();

	}

}
