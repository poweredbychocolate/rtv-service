package statystyki;

/**
 * Interfejs strategi dla {@link statystyki.GeneratorStatystyk}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public interface KalkulatorStatystyk {
	public Statystyka generowanieStatystyki();
}
