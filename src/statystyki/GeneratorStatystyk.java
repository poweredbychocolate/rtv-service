package statystyki;

import java.io.Serializable;

/**
 * Klasa dostarcza metod generowania statystyk
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class GeneratorStatystyk implements Serializable {
	private static final long serialVersionUID = 1L;
	private KalkulatorStatystyk stategiaStatystyk;
	private Statystyka statystyka;

	public GeneratorStatystyk() {
	}

	/**
	 * Pozwala na wybor odpowiedniej strategi generowania statystyk z urzyciem
	 * {@link statystyki.KalkulatorStatystyk} badz klas rozszerzajcych
	 * 
	 * @param strategia
	 *            Wybrana strategia
	 */
	public void wyborStrategi(KalkulatorStatystyk strategia) {
		this.stategiaStatystyk = strategia;
	}

	/**
	 * Generuje {@link statystyki.Statystyka} na postawie wybrane strategi
	 * 
	 * @return Wygenerowana statystyka
	 */
	public Statystyka wykonaj() {
		statystyka = stategiaStatystyk.generowanieStatystyki();
		statystyka.setDataWygenerowania();
		return statystyka;
	}

	/**
	 * Zwaraca ostatnio wygenerowana statystyke
	 * 
	 * @return Statystyka
	 */
	public Statystyka getStatystyka() {
		return statystyka;
	}

}
