package statystyki.strategie;

import java.io.Serializable;

import siec.GniazdoKlienta;
import statystyki.KalkulatorStatystyk;
import statystyki.Statystyka;
import statystyki.TypStatystyk;
import statystyki.listy.NaprawioneUrzadzenia;

/**
 * Wlasciwy {@link statystyki.KalkulatorStatystyk} - strategia wygenerowania
 * {@link statystyki.listy.NaprawioneUrzadzenia}
 * 
 * @author Dawid Brelak
 * @version 1.0
 *
 */
public class KalkulatorNaprawianychUrzadzen implements KalkulatorStatystyk, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NaprawioneUrzadzenia statystyka;
	private GniazdoKlienta gniazdo;

	public KalkulatorNaprawianychUrzadzen(GniazdoKlienta gniazdo) {
		this.gniazdo = gniazdo;
	}

	@Override
	public Statystyka generowanieStatystyki() {
		// statystyka = new NaprawioneUrzadzenia();
		///
		statystyka = (NaprawioneUrzadzenia) gniazdo.statystyka(TypStatystyk.NAPRAWIANE_URZADZENIA);
		///
		return statystyka;
	}

}
