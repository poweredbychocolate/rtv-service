package database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Uzytkownicy.FabrykaUzytkownikow;
import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import cennik.Cennik;
import cennik.PozycjaCennika;
import statystyki.TypStatystyk;
import zlecenia.Urzadzenie;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosc;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.czynnosci.Diagnoza;
import zlecenia.statusy.Anulowane;
import zlecenia.statusy.Przyjete;
import zlecenia.statusy.Status;
import zlecenia.statusy.Zakonczone;

/**
 * Klasa obslugi bazych danych zawira metody do wykonania operacji kozysta z
 * {@link database.BazaDanych}
 * 
 * @author Dawid Brelak
 * @version 1.2
 */
public class DBMenadzer {
	private static DBMenadzer instance = null;
	private BazaDanych database = null;
	private Connection c = null;

	private DBMenadzer() throws SQLException {
		database = new BazaDanych();
		c = database.getConnection();
	}

	/**
	 * Metoda tworzy instancje kalasy przy pierwszym uruchomieniu
	 * 
	 * @return {@link DBMenadzer}
	 * @throws SQLException
	 *             {@link SQLException}
	 */
	public synchronized static DBMenadzer getInstance() throws SQLException {
		if (instance == null)
			instance = new DBMenadzer();
		return instance;
	}

	public synchronized void dodajPracownika(Uzytkownik uzytkownik, Pracownik pracownik) throws SQLException {
		String dodaj = "CALL Dodaj_Pracownika(?,?,?,?,?,?)";
		CallableStatement wywolaj = c.prepareCall(dodaj);
		wywolaj.registerOutParameter(1, java.sql.Types.NUMERIC);
		wywolaj.setString(2, pracownik.getImie());
		wywolaj.setString(3, pracownik.getNazwisko());
		wywolaj.setString(4, uzytkownik.getLogin());
		wywolaj.setString(5, uzytkownik.getHaslo());
		wywolaj.setInt(6, uzytkownik.getPoziomUprawnien());
		wywolaj.execute();
		uzytkownik.setUzytkownikId(wywolaj.getInt(1));
		c.commit();
		wywolaj.close();
	}

	public synchronized void dodajKlienta(Uzytkownik uzytkownik, Klient klient) throws SQLException {
		String dodaj = "CALL Dodaj_Klienta(?,?,?,?,?,?)";
		CallableStatement wywolaj = c.prepareCall(dodaj);
		wywolaj.registerOutParameter(1, java.sql.Types.NUMERIC);
		wywolaj.setString(2, klient.getImie());
		wywolaj.setString(3, klient.getNazwisko());
		wywolaj.setString(4, uzytkownik.getLogin());
		wywolaj.setString(5, uzytkownik.getHaslo());
		wywolaj.setString(6, klient.getTelefon());
		wywolaj.execute();
		uzytkownik.setUzytkownikId(wywolaj.getInt(1));
		c.commit();
		wywolaj.close();
	}

	public synchronized Uzytkownik resetujDaneLogowania(Klient klient, String login, String haslo) throws SQLException {
		Uzytkownik u = null;
		String zapytanie;
		PreparedStatement wywolaj;
		ResultSet r = null;
		Integer id;

		zapytanie = "SELECT KlientId FROM Klienci " + "WHERE Imie=? AND Nazwisko=? AND Telefon= ?";
		wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, klient.getImie());
		wywolaj.setString(2, klient.getNazwisko());
		wywolaj.setString(3, klient.getTelefon());
		wywolaj.executeQuery();
		r = wywolaj.getResultSet();
		if (r.next()) {
			id = r.getInt("KlientId");
			////
			zapytanie = "UPDATE Uzytkownicy SET Login=?, Haslo=? WHERE UzytkownikId=?";
			wywolaj = c.prepareStatement(zapytanie);
			wywolaj.setString(1, login);
			wywolaj.setString(2, haslo);
			wywolaj.setInt(3, id);
			wywolaj.executeQuery();
			////
			zapytanie = "SELECT Login,Haslo FROM Uzytkownicy WHERE UzytkownikId =?";
			wywolaj = c.prepareStatement(zapytanie);
			wywolaj.setInt(1, id);
			wywolaj.executeQuery();
			r = wywolaj.getResultSet();
			if (r.next()) {
				u = (Uzytkownik) FabrykaUzytkownikow.getUzytkownik("uzytkownik");
				u.setLogin(r.getString("Login"));
				u.setHaslo(r.getString("Haslo"));
			}
		}
		////
		c.commit();
		r.close();
		wywolaj.close();
		return u;
	}

	public synchronized void usunPracownika(Pracownik pracownik) throws SQLException {
		String zapytanie;
		PreparedStatement wywolaj;
		ResultSet r = null;
		Integer id;

		zapytanie = "SELECT PracownikId FROM Pracownicy WHERE Imie=? AND Nazwisko=?";
		wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, pracownik.getImie());
		wywolaj.setString(2, pracownik.getNazwisko());
		wywolaj.executeQuery();
		r = wywolaj.getResultSet();
		if (r.next()) {
			id = r.getInt("PracownikId");
			////
			zapytanie = "UPDATE Pracownicy SET Aktywny=? WHERE PracownikId=?";
			wywolaj = c.prepareStatement(zapytanie);
			wywolaj.setString(1, "NIE");
			wywolaj.setInt(2, id);
			wywolaj.executeQuery();
			////
			c.commit();
		}
		r.close();
		wywolaj.close();
	}

	public Integer sprawdzDaneLogowania(String login, String haslo) throws SQLException {
		Integer id = 0;
		////
		String zapytanie = "SELECT UzytkownikId FROM Uzytkownicy WHERE Login=? AND Haslo=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, login);
		wywolaj.setString(2, haslo);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next())
			id = r.getInt("UzytkownikId");
		wywolaj.close();
		return id;
	}

	public boolean sprawdzUzytkownika(String login, String haslo) throws SQLException {
		Integer l = sprawdzDaneLogowania(login, haslo);
		if (l == 0)
			return false;
		else
			return true;
	}

	public Integer sprawdzDanePracownika(Pracownik p) throws SQLException {
		Integer id = 0;
		////
		String zapytanie = "SELECT PracownikId FROM Pracownicy WHERE Imie=? AND Nazwisko=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, p.getImie());
		wywolaj.setString(2, p.getNazwisko());
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next())
			id = r.getInt("PracownikId");
		wywolaj.close();
		return id;
	}

	public Integer sprawdzDaneKlienta(Klient k) throws SQLException {
		Integer id = 0;
		////
		String zapytanie = "SELECT KlientId FROM Klienci WHERE Imie=? AND Nazwisko=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, k.getImie());
		wywolaj.setString(2, k.getNazwisko());
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next())
			id = r.getInt("KlientId");
		wywolaj.close();
		r.close();
		return id;
	}

	public Uzytkownik wczytajUzytkownika(Integer id) throws SQLException {
		Uzytkownik u = null;
		String zapytanie = "SELECT Login,Haslo,Uprawnienia FROM Uzytkownicy WHERE UzytkownikId=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, id);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next()) {
			u = (Uzytkownik) FabrykaUzytkownikow.getUzytkownik("uzytkownik");
			u.setLogin(r.getString("Login"));
			u.setHaslo(r.getString("Haslo"));
			u.setPoziomUprawnien(r.getInt("Uprawnienia"));
		}
		r.close();
		wywolaj.close();
		return u;
	}

	public Klient wczytajKlienta(Integer id) throws SQLException {
		Klient k = null;
		String zapytanie = "SELECT Imie,Nazwisko,Telefon FROM Klienci WHERE klientId=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, id);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next()) {
			k = (Klient) FabrykaUzytkownikow.getUzytkownik("klienT");
			k.setImie(r.getString("Imie"));
			k.setNazwisko(r.getString("Nazwisko"));
			k.setTelefon(r.getString("Telefon"));
		}
		r.close();
		wywolaj.close();
		return k;
	}

	public Integer wczytajLiczbeZlecen(Klient k) throws SQLException {
		Integer liczba = 0;
		String zapytanie = "SELECT COUNT(zl.ZlecenieId) FROM Klienci kl, Zlecenia zl "
				+ "WHERE kl.KlientId = zl.KlientId AND kl.KLIENTID = ? GROUP BY kl.KlientId";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, sprawdzDaneKlienta(k));
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next()) {
			liczba = r.getInt(1);
		}
		r.close();
		wywolaj.close();
		return liczba;
	}

	public Pracownik wczytajPracownika(Integer id) throws SQLException {
		Pracownik p = null;
		String zapytanie = "SELECT Imie,Nazwisko,Aktywny FROM Pracownicy WHERE PracownikId=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, id);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next()) {
			p = (Pracownik) FabrykaUzytkownikow.getUzytkownik("Pracownik");
			p.setImie(r.getString("Imie"));
			p.setNazwisko(r.getString("Nazwisko"));
			if (r.getString("Aktywny").equals("TAK")) {
				p.setAktywny();
			} else {
				p.setNieaktywny();
			}
		}
		r.close();
		wywolaj.close();
		return p;
	}

	public ResultSet listaPracownikow() throws SQLException {
		String zapytanie = "SELECT Imie,Nazwisko,Aktywny FROM Pracownicy ORDER BY Nazwisko ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		return r;
	}

	public ResultSet listaKlientow() throws SQLException {
		String zapytanie = "SELECT Imie,Nazwisko,Telefon FROM Klienci ORDER BY Nazwisko ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		return r;
	}

	private java.sql.Date stringDateToSQLDate(String dataString) {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date data = null;
		try {
			data = format.parse(dataString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new java.sql.Date(data.getTime());

	}

	private String SQLDateToString(java.sql.Date dataSQL) {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date data = new Date();
		data.setTime(dataSQL.getTime());
		String dataString = null;
		dataString = format.format(data);
		return dataString;

	}

	public synchronized void dodajStatus(Status status) throws SQLException {
		String dodaj = "CALL Dodaj_Status(?,?,?,?)";
		CallableStatement wywolaj = c.prepareCall(dodaj);
		String s = null, d = null;
		Integer id = 0;
		/// uzupelnic o nowe stausy
		if (status instanceof Przyjete)
			s = new String("PRZYJETE");
		else if (status instanceof Zakonczone)
			s = new String("ZAKONCZONE");
		else if (status instanceof Anulowane)
			s = new String("ANULOWANE");
		///
		d = new String(status.getDataRozpoczecia());
		id = sprawdzDanePracownika(status.getPracownik());
		wywolaj.registerOutParameter(1, java.sql.Types.NUMERIC);
		wywolaj.setString(2, s);
		wywolaj.setDate(3, stringDateToSQLDate(d));
		wywolaj.setInt(4, id);
		wywolaj.execute();
		status.setStatusId(wywolaj.getInt(1));
		c.commit();
		wywolaj.close();
	}

	private synchronized void dodajNoweUrzadzenie(Urzadzenie urzadzenie) throws SQLException {
		String dodaj = "CALL Dodaj_Urzadenie(?,?)";
		PreparedStatement wywolaj = c.prepareCall(dodaj);
		wywolaj.setString(1, urzadzenie.getMarka());
		wywolaj.setString(2, urzadzenie.getModel());
		wywolaj.executeQuery();
		c.commit();
		wywolaj.close();
	}

	private synchronized void zwiekszLicznikUzadzenia(Integer urzadzenieId) throws SQLException {
		Integer id = 0;
		String dodaj = "SELECT Ilosc_Serwisowanych FROM Urzadzenia WHERE UrzadzenieId = ?";
		PreparedStatement wywolaj = c.prepareStatement(dodaj);
		wywolaj.setInt(1, urzadzenieId);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next()) {
			id = r.getInt("Ilosc_Serwisowanych");
			id++;
			dodaj = "UPDATE Urzadzenia SET Ilosc_Serwisowanych =? WHERE UrzadzenieId = ?";
			wywolaj = c.prepareStatement(dodaj);
			wywolaj.setInt(1, id);
			wywolaj.setInt(2, urzadzenieId);
			wywolaj.executeQuery();
		}
		c.commit();
		wywolaj.close();
	}

	public synchronized void dodajUrzadzenie(Urzadzenie urzadzenie) throws SQLException {
		Integer id = 0;
		ResultSet rs = null;
		String dodaj = "SELECT UrzadzenieId FROM Urzadzenia WHERE Marka =? AND Model_Urzadzenia = ?";
		PreparedStatement wywolaj = c.prepareStatement(dodaj);
		wywolaj.setString(1, urzadzenie.getMarka());
		wywolaj.setString(2, urzadzenie.getModel());
		wywolaj.executeQuery();
		rs = wywolaj.getResultSet();
		if (rs.next())
			id = rs.getInt("UrzadzenieId");
		if (id == 0)
			dodajNoweUrzadzenie(urzadzenie);
		else
			zwiekszLicznikUzadzenia(id);
		c.commit();
		wywolaj.close();
	}

	public Integer sprawdzUrzadzenie(Urzadzenie urzadzenie) throws SQLException {
		Integer id = 0;
		////
		String zapytanie = "SELECT UrzadzenieId FROM Urzadzenia WHERE Marka=? AND Model_Urzadzenia=?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, urzadzenie.getMarka());
		wywolaj.setString(2, urzadzenie.getModel());
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next())
			id = r.getInt("UrzadzenieId");
		wywolaj.close();
		return id;
	}

	public Urzadzenie wczytajUrzadzenie(Integer urzadzenieId) throws SQLException {
		Urzadzenie urzadzenie = null;
		////
		String zapytanie = "SELECT Marka, Model_Urzadzenia FROM Urzadzenia WHERE UrzadzenieId =?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, urzadzenieId);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		if (r.next())
			urzadzenie = new Urzadzenie();
		urzadzenie.setMarka(r.getString("Marka"));
		urzadzenie.setModel(r.getString("Model_Urzadzenia"));
		wywolaj.close();
		return urzadzenie;
	}

	public synchronized void dodajZlecenie(Zlecenie zlecenie) throws SQLException {
		// Dodaj_Zlecenie(zlecenieId OUT NUMBER,klientId IN NUMBER,urzadzenieId
		// IN NUMBER,statusId IN NUMBER,gwarancja IN VARCHAR2)
		Integer id = -2;
		String zapytanie = "CALL Dodaj_Zlecenie(?,?,?,?,?)";
		CallableStatement wykonaj = c.prepareCall(zapytanie);
		wykonaj.registerOutParameter(1, java.sql.Types.NUMERIC);
		wykonaj.setInt(2, sprawdzDaneKlienta(zlecenie.getKlienta()));
		wykonaj.setInt(3, sprawdzUrzadzenie(zlecenie.getUrzadzenie()));
		wykonaj.setInt(4, zlecenie.getStatus().getStatusId());
		if (zlecenie.getGwaracjaProducenta())
			wykonaj.setString(5, "TAK");
		else
			wykonaj.setString(5, "NIE");
		wykonaj.execute();
		id = wykonaj.getInt(1);
		zlecenie.setZlecenieId(id);
		c.commit();
		wykonaj.close();
	}

	public ResultSet listaZlecenKlienta(Klient klient) throws SQLException {
		String zapytanie = "SELECT zl.ZlecenieId,kl.Imie,kl.Nazwisko,urz.Marka,urz.Model_Urzadzenia "
				+ "FROM Zlecenia zl, Klienci kl,Urzadzenia urz WHERE zl.KlientId = kl.KlientId "
				+ "AND zl.UrzadzenieId = urz.UrzadzenieId AND kl.KlientId = ? ORDER BY urz.Marka ASC ,urz.Model_Urzadzenia ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, sprawdzDaneKlienta(klient));
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		return r;
	}

	public Zlecenie wczytajZlecenie(Integer zlecenieId) throws SQLException {
		Zlecenie zlecenie = new Zlecenie();
		Klient klient = null;
		Urzadzenie urzadzenie = null;
		String zapytanie = "SELECT ZlecenieId,KlientId,UrzadzenieId,Gwarancja FROM Zlecenia WHERE ZlecenieId = ?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, zlecenieId);
		wywolaj.executeQuery();
		ResultSet rs = wywolaj.getResultSet();
		if (rs.next()) {
			zlecenie = new Zlecenie();
			urzadzenie = wczytajUrzadzenie(rs.getInt("UrzadzenieId"));
			klient = wczytajKlienta(rs.getInt("KlientId"));

			zlecenie.setZlecenieId(rs.getInt("ZlecenieId"));
			zlecenie.setGwaracjaProducenta(rs.getString("Gwarancja").equals("TAK") ? true : false);
			zlecenie.setUrzadzenie(urzadzenie);
			zlecenie.setKlient(klient);

		}
		return zlecenie;
	}

	public ResultSet listaZlecenRS() throws SQLException {
		String zapytanie = "SELECT zl.ZlecenieId, kl.Imie ,kl.Nazwisko, urz.Marka, urz.Model_Urzadzenia,"
				+ " zl.Gwarancja, sta.StatusID, sta.PracownikId, sta.Status, sta.Data_Rozpoczecia "
				+ "FROM Zlecenia zl, Klienci kl, Urzadzenia urz, Statusy sta, Statusy_Zlecenia stz "
				+ "WHERE zl.KlientId = kl.KlientId AND zl.UrzadzenieId = urz.UrzadzenieId "
				+ "AND zl.ZlecenieId = stz.ZlecenieId AND stz.StatusId = sta.StatusId "
				+ "AND sta.StatusId IN ( SELECT MAX (Statusy.StatusId) FROM Statusy, Statusy_Zlecenia, Zlecenia "
				+ "WHERE Statusy_Zlecenia.StatusId = Statusy.StatusId "
				+ "AND Statusy_Zlecenia.ZlecenieId = Zlecenia.ZlecenieId GROUP BY Zlecenia.ZlecenieId ) "
				+ "ORDER BY sta.Status ASC, kl.Imie ASC, kl.Nazwisko ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.executeQuery();
		ResultSet rs = wywolaj.getResultSet();
		// wywolaj.close();
		return rs;
	}

	public ResultSet listaZlecenRS(Klient klient) throws SQLException {
		String zapytanie = "SELECT zl.ZlecenieId, kl.Imie ,kl.Nazwisko, urz.Marka, urz.Model_Urzadzenia,"
				+ " zl.Gwarancja, sta.StatusID, sta.PracownikId, sta.Status, sta.Data_Rozpoczecia "
				+ "FROM Zlecenia zl, Klienci kl, Urzadzenia urz, Statusy sta, Statusy_Zlecenia stz "
				+ "WHERE zl.KlientId = kl.KlientId AND zl.UrzadzenieId = urz.UrzadzenieId "
				+ "AND zl.ZlecenieId = stz.ZlecenieId AND stz.StatusId = sta.StatusId "
				+ "AND sta.StatusId IN ( SELECT MAX (Statusy.StatusId) FROM Statusy, Statusy_Zlecenia, Zlecenia "
				+ "WHERE Statusy_Zlecenia.StatusId = Statusy.StatusId "
				+ "AND Statusy_Zlecenia.ZlecenieId = Zlecenia.ZlecenieId GROUP BY Zlecenia.ZlecenieId )"
				+ " AND kl.KlientId = ? ORDER BY  sta.Status ASC, kl.Imie ASC, kl.Nazwisko ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, sprawdzDaneKlienta(klient));
		wywolaj.executeQuery();
		ResultSet rs = wywolaj.getResultSet();
		// wywolaj.close();
		return rs;
	}

	public ArrayList<Zlecenie> listaZlecenAL(ResultSet zbiorZlecen) throws SQLException {
		// 1 zl.ZlecenieId
		// 2 kl.Imie, 3 kl.Nazwisko
		// 4 urz.Marka, 5 urz.Model_Urzadzenia
		// 6 zl.Gwarancja
		// 7 sta.StatusID, 8 sta.PracownikId,
		// 9 sta.Status, 10 sta.Data_Rozpoczecia "
		ArrayList<Zlecenie> zlecenia = new ArrayList<Zlecenie>();
		ResultSet rs = zbiorZlecen;

		Zlecenie ztmp = null;
		Klient ktmp = null;
		Urzadzenie utmp = null;
		Status stmp = null;
		try {
			while (rs.next()) {
				ztmp = new Zlecenie();
				ktmp = (Klient) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.KLIENT);
				utmp = new Urzadzenie();

				ztmp.setZlecenieId(rs.getInt(1));
				ktmp.setImie(rs.getString(2));
				ktmp.setNazwisko(rs.getString(3));
				utmp.setMarka(rs.getString(4));
				utmp.setModel(rs.getString(5));
				ztmp.setGwaracjaProducenta(rs.getString(6).equals("TAK") ? true : false);
				String s = rs.getString(9);

				// rozszezyc o pozostale statusy
				if (s.equals("PRZYJETE"))
					stmp = new Przyjete();
				else if (s.equals("ZAKONCZONE"))
					stmp = new Zakonczone();
				else if (s.equals("ANULOWANE"))
					stmp = new Anulowane();
				stmp.setStatusId(rs.getInt(7));
				stmp.setPracownik(wczytajPracownika(rs.getInt(8)));
				stmp.setDataRozpoczecia(SQLDateToString(rs.getDate(10)));
				///
				ztmp.setUrzadzenie(utmp);
				ztmp.setKlient(ktmp);
				ztmp.setStatus(stmp);
				zlecenia.add(ztmp);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return zlecenia;

	}

	public synchronized void zmienStatusZlecenia(Zlecenie zlecenie, Status status) throws SQLException {
		String zapytanie = "UPDATE Statusy SET Data_Zakonczenia = ? WHERE StatusId =?";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setDate(1, stringDateToSQLDate(status.getDataRozpoczecia()));
		wywolaj.setInt(2, zlecenie.getStatus().getStatusId());
		wywolaj.executeQuery();
		dodajStatus(status);
		zapytanie = "INSERT INTO Statusy_Zlecenia VALUES(?,?)";
		wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, zlecenie.getZlecenieId());
		wywolaj.setInt(2, status.getStatusId());
		wywolaj.executeQuery();
		c.commit();
		wywolaj.close();
	}

	public synchronized void dodajDoCennika(PozycjaCennika pozycja) throws SQLException {
		String dodaj = "CALL Dodaj_Pozycje_Cennika(?,?,?)";
		CallableStatement wywolaj = c.prepareCall(dodaj);

		wywolaj.setString(1, pozycja.getUsluga());
		wywolaj.setInt(2, pozycja.getKoszt());
		wywolaj.setInt(3, pozycja.getCzas());
		wywolaj.execute();

		c.commit();
		wywolaj.close();
	}

	public Cennik cennik() throws SQLException {
		Cennik ce = new Cennik();
		String zapytanie = "SELECT Czynnosc,Cenna,Czas FROM Cennik ORDER BY Czynnosc ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.executeQuery();
		ResultSet r = wywolaj.getResultSet();
		while (r.next()) {
			PozycjaCennika pz = new PozycjaCennika();
			pz.setUsluga(r.getString("Czynnosc"));
			pz.setKoszt(r.getInt("Cenna"));
			pz.setCzasRealizacji(r.getInt("Czas"));
			ce.dodajUsluge(pz);
		}
		r.close();
		return ce;
	}

	public synchronized void usunPozycjecennika(PozycjaCennika pozycja) throws SQLException {
		String zapytanie = "DELETE FROM Cennik WHERE Czynnosc = ? AND Cenna = ? AND Czas = ? ";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setString(1, pozycja.getUsluga());
		wywolaj.setInt(2, pozycja.getKoszt());
		wywolaj.setInt(3, pozycja.getCzas());
		wywolaj.executeQuery();
		wywolaj.close();
	}

	public ResultSet statystyki(Integer typ) throws SQLException {
		String zapytanie;
		PreparedStatement wywolaj;
		ResultSet rs = null;
		if (typ.equals(TypStatystyk.NAPRAWIANE_URZADZENIA)) {
			zapytanie = "SELECT Marka,SUM(Ilosc_Serwisowanych) AS Ilosc FROM Urzadzenia GROUP BY Marka ORDER BY Ilosc DESC";
			wywolaj = c.prepareStatement(zapytanie);
			wywolaj.executeQuery();
			rs = wywolaj.getResultSet();
		}
		return rs;
	}

	public ArrayList<Status> wczytajStausy(Integer id) throws SQLException {
		String zapytanie = "SELECT s.Data_Rozpoczecia,s.Status,s.PracownikId,s.Data_Zakonczenia "
				+ "FROM Zlecenia z,Statusy s ,Statusy_Zlecenia sz "
				+ "WHERE z.ZlecenieId = sz.ZlecenieId AND s.StatusId = sz.StatusId AND z.ZlecenieId = ? "
				+ "ORDER BY s.Data_Rozpoczecia, s.StatusId ASC";
		PreparedStatement wywolaj = c.prepareStatement(zapytanie);
		wywolaj.setInt(1, id);
		wywolaj.executeQuery();
		ResultSet rs = wywolaj.getResultSet();

		ArrayList<Status> lista = new ArrayList<Status>();
		Status tmp = null;
		while (rs.next()) {
			String stmp = rs.getString("Status");
			if (stmp.equals("PRZYJETE"))
				tmp = new Przyjete();
			else if (stmp.equals("ANULOWANE"))
				tmp = new Anulowane();
			else if (stmp.equals("ZAKONCZONE"))
				tmp = new Zakonczone();

			tmp.setDataRozpoczecia(SQLDateToString(rs.getDate("Data_Rozpoczecia")));
			java.sql.Date dataZ = rs.getDate("Data_Zakonczenia");
			if (dataZ != null)
				tmp.setDataZakonczenia(SQLDateToString(dataZ));
			else
				tmp.setDataZakonczenia(null);
			tmp.setPracownik(wczytajPracownika(rs.getInt("PracownikId")));
			lista.add(tmp);
		}

		return lista;
	}

	private synchronized void dodajCzynnosc(Czynnosc czynnosc, Integer zlecenieId) throws SQLException {
		// Dodaj_Czynnosc(Typ IN VARCHAR2,Opis IN VARCHAR2, Koszt IN NUMBER,
		// ZlecenieId IN NUMBER)
		String dodaj = "CALL Dodaj_Czynnosc(?,?,?,?)";
		CallableStatement wywolaj = c.prepareCall(dodaj);
		String typ;
		if (czynnosc instanceof Diagnoza)
			typ = "DIAGNOZA";
		else
			typ = "NAPRAWA";
		wywolaj.setString(1, typ);
		wywolaj.setString(2, czynnosc.getOpis());
		wywolaj.setInt(3, czynnosc.getKoszt());
		wywolaj.setInt(4, zlecenieId);
		wywolaj.executeQuery();
		c.commit();
		wywolaj.close();
	}

	public synchronized void zapiszCzynnosci(Czynnosci czynnosci, Integer zlecenieId) throws SQLException {
		Integer liczba = czynnosci.iloscCzynosci();
		for (int i = 0; i < liczba; i++) {
			dodajCzynnosc(czynnosci.getCzynnosc(i), zlecenieId);
		}

	}

	public  ResultSet wczytajCzynnosci(Integer id) throws SQLException {
		String sql = "SELECT Typ,Opis,Koszt FROM Czynnosci " + "WHERE ZlecenieId = ? ORDER BY Typ ASC";
		PreparedStatement wywolaj = c.prepareStatement(sql);
		wywolaj.setInt(1, id);
		wywolaj.execute();
		ResultSet rs = wywolaj.getResultSet();
		return rs;
	}
}
