package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Klasa zawierajca dane dostepu do bazy danych
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class BazaDanych {
	private String dbUrl = "jdbc:oracle:thin:@localhost:1521:orcl";
	private String user = "system";
	private String password = "sys";

	private Connection c = null;

	public BazaDanych() throws SQLException {
		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
		c = DriverManager.getConnection(dbUrl, user, password);
		c.setAutoCommit(true);
	};

	public Connection getConnection() throws SQLException {
		return c;
	}
}
