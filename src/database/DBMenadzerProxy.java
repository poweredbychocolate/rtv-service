package database;

import java.sql.SQLException;
import java.util.ArrayList;
import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import zlecenia.Zlecenie;
import zlecenia.statusy.Anulowane;

/**
 * Klasa posredniczaca w dostepie do bazy danych przez klienta
 * {@link database.DBMenadzer}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class DBMenadzerProxy {
	private Klient klient;
	DBMenadzer dbm;

	public DBMenadzerProxy(Klient klient) throws SQLException {
		this.klient = klient;
		this.dbm = DBMenadzer.getInstance();
	}

	public void AnulujZlecenie(Zlecenie zlecenie) throws SQLException {
		Anulowane status = new Anulowane();
		status.setDataRozpoczecia(KlientDane.getBiezacyCzas());
		status.setPracownik(zlecenie.getStatus().getPracownik());
		dbm.zmienStatusZlecenia(zlecenie, status);
	}

	public ArrayList<Zlecenie> listaZlecen() throws SQLException {
		return dbm.listaZlecenAL(dbm.listaZlecenRS(klient));
	}
}
