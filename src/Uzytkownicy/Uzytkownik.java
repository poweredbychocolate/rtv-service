package Uzytkownicy;

import java.io.Serializable;

/**
 * Klasa {@link UzytkownikSystemu} dostarcza danych logowania oraz typ uzytkowka
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Uzytkownik implements UzytkownikSystemu, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String login = null, haslo = null;
	private Integer uprawnienia = 1;
	private Integer uzytkownikId = 0;

	public static final Integer KLIENT = 1;
	public static final Integer PRACOWNIK = 2;
	public static final Integer ADMINISTRATOR = 3;

	Uzytkownik() {
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}

	public void setPoziomUprawnien(Integer poziom) {
		if (poziom == 3 || poziom == 2)
			this.uprawnienia = poziom;
		else
			this.uprawnienia = 1;
	}

	public void setUzytkownikId(Integer id) {
		this.uzytkownikId = id;
	}

	public String getLogin() {
		return this.login;
	}

	public String getHaslo() {
		return this.haslo;
	}

	public Integer getPoziomUprawnien() {
		return this.uprawnienia;
	}

	public Boolean isKlient() {
		if (this.uprawnienia == 1)
			return true;
		else
			return false;
	}

	public Integer getUzytkownikId() {
		return this.uzytkownikId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return new String(
				"[ " + this.uzytkownikId + " : " + this.login + " : " + this.haslo + " : " + this.uprawnienia + " ]");
	}
}
