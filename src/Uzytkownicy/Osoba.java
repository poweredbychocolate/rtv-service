package Uzytkownicy;

/**
 * Interfejs uzytkownikow konkretnych
 * 
 * @author Dawid Brelak
 * @version 1.0
 *
 */
public interface Osoba {
	public void setImie(String imie);

	public void setNazwisko(String nazwisko);

	public String getImie();

	public String getNazwisko();

}
