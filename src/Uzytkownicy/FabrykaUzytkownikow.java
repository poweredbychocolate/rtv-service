package Uzytkownicy;

/**
 * Klasa tworzaca {@link Uzytkownicy.UzytkownikSystemu}
 * @author Dawid Brelak
 *	@version 1.1
 */
public class FabrykaUzytkownikow {
	public static final String UZYTKOWNIK = "uzytkownik";
	public static final String KLIENT ="klient";
	public static final String PRACOWNIK = "pracownik";
	/**
	 * Metoda tworzaca wybranego uzytkownika 
	 * @param uzytkownik Rodzaj wybranego uzytkownika
	 * @return Obiekt wybranego uzytkownika 
	 */
	public static UzytkownikSystemu getUzytkownik(String uzytkownik) {
		if (uzytkownik.toLowerCase().equals("uzytkownik"))
			return new Uzytkownik();
		else if (uzytkownik.toLowerCase().equals("klient"))
			return new Klient();
		else if (uzytkownik.toLowerCase().equals("pracownik"))
			return new Pracownik();
		else
			return null;
	}
}
