package Uzytkownicy;

import java.io.Serializable;

/**
 * Reprezentuje Klienta Systemu
 * 
 * @author Dawid Brelak
 *
 */
public class Klient implements UzytkownikSystemu, Osoba, Serializable, Cloneable {
	Klient() {
		// TODO Auto-generated constructor stub
	}

	String imie = null, nazwisko = null, telefon = null;
	private static final long serialVersionUID = 1L;

	@Override
	public void setImie(String imie) {
		this.imie = imie;

	}

	@Override
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	@Override
	public String getImie() {
		return this.imie;
	}

	@Override
	public String getNazwisko() {
		// TODO Auto-generated method stub
		return this.nazwisko;
	}

	//
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getTelefon() {
		// TODO Auto-generated method stub
		return this.telefon;
	}

	@Override
	public String toString() {
		return new String("[ " + this.imie + " : " + this.nazwisko + " : " + this.telefon + " ]");
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
