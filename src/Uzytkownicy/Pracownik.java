package Uzytkownicy;

import java.io.Serializable;
/**
 * reprezentuje Pracownika Systemu
 * @author Dawid Brelak
 *
 */
public class Pracownik implements UzytkownikSystemu, Osoba, Serializable {
	Pracownik() {
		// TODO Auto-generated constructor stub
	}

	private String imie = null, nazwisko = null;
	private Boolean aktywny = true;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setImie(String imie) {
		this.imie = imie;

	}

	@Override
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public void setAktywny() {
		this.aktywny = true;
	}

	public void setNieaktywny() {
		this.aktywny = false;
	}

	@Override
	public String getImie() {
		return this.imie;
	}

	@Override
	public String getNazwisko() {
		// TODO Auto-generated method stub
		return this.nazwisko;
	}

	public Boolean getAktywny() {
		return this.aktywny;
	}

	@Override
	public String toString() {
		return new String("[ " + this.imie + " : " + this.nazwisko + " : aktywny = " + this.aktywny + " ]");
	}
	@Override
	public boolean equals(Object obj) {
		Integer i=0;
		if(obj instanceof Pracownik )
		{
			i++;
			Pracownik p =(Pracownik) obj;
			if(p.getImie().equals(this.imie))i++;
			if(p.getNazwisko().equals(this.nazwisko))i++;
			if(p.getAktywny().equals(this.aktywny))i++;
		}
		return i==4 ? true : false;
	}
}
