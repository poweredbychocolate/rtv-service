package zlecenia.Iterator;

import zlecenia.Zlecenie;

public interface Iterator {
	public Boolean poprzedni();
	public Boolean nastepny();
	public void reset();
	public Zlecenie zlecenie();
}
