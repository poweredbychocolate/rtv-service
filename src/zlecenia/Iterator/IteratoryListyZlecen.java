package zlecenia.Iterator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import siec.GniazdoKlienta;
import zlecenia.Zlecenie;
import zlecenia.statusy.Anulowane;
import zlecenia.statusy.Przyjete;
import zlecenia.statusy.Zakonczone;

public class IteratoryListyZlecen implements Serializable, ListaZlecen {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Integer PRZYJETE = 1;
	public static final Integer ANULOWANE = 2;
	public static final Integer ZAKONCZONE = 3;

	private Boolean dostepne = false;


	private Iterator przyjete = null;
	private Iterator anulowane = null;
	private Iterator zakonczone = null;

	private ArrayList<Zlecenie> zlecenia = null;

	private GniazdoKlienta gniazdko = null;
	private ExecutorService watki = null;

	// public static final Integer = ;
	public IteratoryListyZlecen(GniazdoKlienta gniazdo) {
		gniazdko = gniazdo;
			zaladuj();
	}

	private synchronized void zaladuj()

	{
		watki = Executors.newSingleThreadExecutor();
		watki.execute(new WczytajZlecenia());
		watki.execute(new Wytnij());
		watki.shutdown();
	}

	@Override
	public synchronized Iterator getIterator(Integer typ) {
		if (typ.equals(ZAKONCZONE) && dostepne) {
					return zakonczone;
		} else if (typ.equals(ANULOWANE) && dostepne) {
				return anulowane;
		} else if (typ.equals(PRZYJETE) && dostepne) {
					return przyjete;
		}
		return null;
	}


	public synchronized Boolean postepne() {
			return dostepne;
	}

	class WczytajZlecenia extends Thread

	{
		public WczytajZlecenia() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {
			zlecenia=gniazdko.listaZlecen();
		}
	}

	class Wytnij extends Thread {
		public Wytnij() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void run() {
			ArrayList<Zlecenie> listaAnulowanych = new ArrayList<Zlecenie>();
			ArrayList<Zlecenie> listaPrzyjetych = new ArrayList<Zlecenie>();
			ArrayList<Zlecenie> listaZakonczonych = new ArrayList<Zlecenie>();
			for(Zlecenie z :zlecenia)
			{
				if (z.getStatus() instanceof Anulowane)
					listaAnulowanych.add(z);
				else if (z.getStatus() instanceof Przyjete)
					listaPrzyjetych.add(z);
				if (z.getStatus() instanceof Zakonczone)
					listaZakonczonych.add(z);
			}
			przyjete = new IteratorPrzyjetychZlecen(listaPrzyjetych);
			anulowane = new IteratorAnulowanychZlecen(listaAnulowanych);
			zakonczone = new IteratorZakonczonychZlecen(listaZakonczonych);
			dostepne = true;
		}// run end
	}


}
