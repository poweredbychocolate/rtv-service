package zlecenia.Iterator;

import java.io.Serializable;
import java.util.ArrayList;

import zlecenia.Zlecenie;

public class IteratorPrzyjetychZlecen implements Iterator,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer aktualny =-1;
	private Integer rozmiar=0;
	private ArrayList<Zlecenie> lista=null;
	
	public IteratorPrzyjetychZlecen(ArrayList<Zlecenie> zlecenia) {
		this.lista=zlecenia;
		this.rozmiar=zlecenia.size()-1;
	}
	@Override
	public Boolean poprzedni() {
		if(aktualny>0)
		{
			aktualny--;
			return true;
		}
		return false;
	}

	@Override
	public Boolean nastepny() {
		if(aktualny<rozmiar)
		{
			aktualny++;
			return true;
		}
		return false;
	}

	@Override
	public void reset() {
		aktualny =0;
	}

	@Override
	public Zlecenie zlecenie() {
		if(aktualny==-1) return lista.get(0);
		return lista.get(aktualny);
	}

}
