package zlecenia.czynnosci;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Grupa czynnosc serwisowych systemu moze zawierac zarowno
 * {@link zlecenia.czynnosci.Czynnosc} jak i
 * {@link zlecenia.czynnosci.Czynnosci}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Czynnosci implements Czynnosc, Serializable {
	private static final long serialVersionUID = 1L;

	private Integer koszt = 0;
	private String opis = null;
	private ArrayList<Czynnosc> lista;

	public Czynnosci() {
		lista = new ArrayList<Czynnosc>();
	}

	@Override
	public void setKoszt(Integer koszt) {
		this.koszt = koszt;

	}

	@Override
	public void setOpis(String opis) {
		this.opis = opis;

	}

	@Override
	public Integer getKoszt() {
		// TODO Auto-generated method stub
		return this.koszt;
	}

	@Override
	public String getOpis() {
		// TODO Auto-generated method stub
		return this.opis;
	}

	public void dodajCzynnosc(Czynnosc c) {
		this.lista.add(c);
	}

	public void usunCzynnosc(Czynnosc c) {
		this.lista.remove(c);
	}

	public Integer iloscCzynosci() {
		return this.lista.size();
	}

	public Czynnosc getCzynnosc(Integer pozycja) {
		return this.lista.get(pozycja);
	}

	public Iterator<Czynnosc> getIterator() {
		return this.lista.iterator();
	}

}
