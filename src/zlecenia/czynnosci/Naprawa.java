package zlecenia.czynnosci;

import java.io.Serializable;

/**
 * Reprezentuje czynnosci parzawcze systemu {@link zlecenia.czynnosci.Czynnosc}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Naprawa implements Czynnosc, Serializable {
	private static final long serialVersionUID = 1L;
	private String opis = null, uwagi = null;
	private Integer koszt = 0;

	public Naprawa() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setKoszt(Integer koszt) {
		this.koszt = koszt;
	}

	@Override
	public void setOpis(String opis) {
		this.opis = opis;
	}

	public void setUwagi(String uwagi) {
		this.uwagi = uwagi;
	}

	@Override
	public Integer getKoszt() {
		// TODO Auto-generated method stub
		return this.koszt;
	}

	@Override
	public String getOpis() {
		// TODO Auto-generated method stub
		return this.opis;
	}

	public String getUwagi() {

		return this.uwagi;
	}

}
