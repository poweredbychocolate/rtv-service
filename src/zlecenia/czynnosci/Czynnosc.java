package zlecenia.czynnosci;

/**
 * Reprezentuje czynnosci naprawcze {@link zlecenia.Zlecenie}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public interface Czynnosc {
	public void setKoszt(Integer koszt);

	public void setOpis(String opis);

	public Integer getKoszt();

	public String getOpis();
}
