package zlecenia.czynnosci;

import java.io.Serializable;

/**
 * Reprezentuje czynnosci Diagnostyczne {@link zlecenia.czynnosci.Czynnosc}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Diagnoza implements Czynnosc, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer koszt=0;
	private String opis=null;
	
public Diagnoza() {
	// TODO Auto-generated constructor stub
}
	@Override
	public void setKoszt(Integer koszt) {
		this.koszt=koszt;

	}

	@Override
	public void setOpis(String opis) {
		this.opis=opis;

	}

	@Override
	public Integer getKoszt() {
		// TODO Auto-generated method stub
		return this.koszt;
	}

	@Override
	public String getOpis() {
		// TODO Auto-generated method stub
		return this.opis;
	}

}
