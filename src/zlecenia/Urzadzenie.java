package zlecenia;

import java.io.Serializable;

/**
 * Klasa reprezentujaca urzadzenie
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Urzadzenie implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	private String model;
	private String marka;

	public Urzadzenie() {
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public String getModel() {
		return this.model;
	}

	public String getMarka() {
		return this.marka;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
