package zlecenia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;

/**
 * Pozwala wczytac liste {@link zlecenia.Zlecenie} wybranego klienta i stworzyc kopie wybranego
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class KopiaZlecenia {
	private ArrayList<Zlecenie> zlecenia = null;
	private HashMap<Integer, Zlecenie> zleceniaHash;
	private GniazdoKlienta gniazdo;

	public KopiaZlecenia() {
		gniazdo = KlientDane.getGniazdoKlienta();
	}

	/**
	 * Tworzy kopie {@link zlecenia.Zlecenie}
	 * 
	 * @param zlecenieid
	 *            Identyfikator zlecenia
	 * @return kopia wybranego zlecenia
	 * @throws CloneNotSupportedException {@link CloneNotSupportedException}
	 * @throws IOException {@link IOException}
	 */
	public Zlecenie kopiujZlecenie(Integer zlecenieid) throws CloneNotSupportedException, IOException {
		return (Zlecenie) zleceniaHash.get(zlecenieid).clone();
	}

	/**
	 * Wczyanie listy zlecen wybranego {@link Uzytkownicy.Klient}
	 * 
	 * @param klient
	 *            Wybrany klient
	 */
	public void wczytajZlecenia(Klient klient) {
		Zlecenie tmp = null;
		zlecenia = gniazdo.listaZlecenKlienta(klient);
		zleceniaHash = new HashMap<Integer, Zlecenie>(zlecenia.size());
		Iterator<Zlecenie> iterator = zlecenia.iterator();
		while (iterator.hasNext()) {
			tmp = iterator.next();
			zleceniaHash.put(tmp.getZlecenieId(), tmp);
		}

	}

	/**
	 * 
	 * @return Wczytane zlecenia
	 */
	public ArrayList<Zlecenie> pobierzListe() {
		return this.zlecenia;

	}

}
