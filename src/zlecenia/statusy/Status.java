package zlecenia.statusy;

import Uzytkownicy.Pracownik;
/**
 * Interfejs okreslajacy stan {@link zlecenia.Zlecenie}
 * @author Dawid Brelak
 *	@version 1.0
 */
public interface Status {
	public void setDataRozpoczecia(String data);
	public void setDataZakonczenia(String data);
	public void setPracownik(Pracownik pracownik);
	public void setStatusId(Integer id);
	
	public String getDataRozpoczecia();
	public String getDataZakonczenia();
	public Pracownik getPracownik();
	public Integer getStatusId();

	}
