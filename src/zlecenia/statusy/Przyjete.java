package zlecenia.statusy;

import java.io.Serializable;

import Uzytkownicy.Pracownik;

/**
 * Konkretny stan {@link zlecenia.Zlecenie}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Przyjete implements Status, Serializable {

	private static final long serialVersionUID = 1L;
	private String dataRozpoczecia = null;
	private String dataZakonczenia = null;
	private Pracownik pracownik = null;
	private Integer statusId = 0;

	public Przyjete() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setDataRozpoczecia(String data) {
		this.dataRozpoczecia = data;
	}

	@Override
	public void setDataZakonczenia(String data) {
		this.dataZakonczenia = data;
	}

	@Override
	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}

	@Override
	public String getDataRozpoczecia() {
		return this.dataRozpoczecia;
	}

	@Override
	public String getDataZakonczenia() {
		return this.dataZakonczenia;
	}

	@Override
	public Pracownik getPracownik() {
		return this.pracownik;
	}

	@Override
	public void setStatusId(Integer id) {
		this.statusId = id;

	}

	@Override
	public Integer getStatusId() {
		return this.statusId;
	}

}
