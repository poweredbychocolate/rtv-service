package zlecenia;

import java.io.Serializable;

import Uzytkownicy.Klient;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.statusy.Status;

/**
 * Klasa dostarcza metod pozwalacych na stworzenie zlecenia
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Zlecenie implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	private Integer zlecenieId = 0;
	private Klient klient = null;
	private Urzadzenie urzadzenie = null;
	private Boolean gwarancjiaProducenta = false;
	private Czynnosci czynnosci = null;
	private Status statusZlecenia = null;
	// tutaj doda� obserwatora

	public Zlecenie() {
	}

	public void setKlient(Klient klient) {
		this.klient = klient;
	}

	public void setUrzadzenie(Urzadzenie urzadzenie) {
		this.urzadzenie = urzadzenie;
	}

	public void setGwaracjaProducenta(Boolean gwarancja) {
		this.gwarancjiaProducenta = gwarancja;
	}

	public void setCzynnosciSerwisowe(Czynnosci czynnosci) {
		this.czynnosci = czynnosci;
	}

	public void setStatus(Status statusZlecenia) {
		this.statusZlecenia = statusZlecenia;
	}

	public void setZlecenieId(Integer id) {
		this.zlecenieId = id;
	}

	public Klient getKlienta() {
		return this.klient;
	}

	public Urzadzenie getUrzadzenie() {
		return this.urzadzenie;
	}

	public Boolean getGwaracjaProducenta() {
		return this.gwarancjiaProducenta;
	}

	public Czynnosci getCzynnosciSerwisowe() {
		return this.czynnosci;
	}

	public Status getStatus() {
		return this.statusZlecenia;
	}

	public Integer getZlecenieId() {
		return this.zlecenieId;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Zlecenie zlecenie = new Zlecenie();

		zlecenie.setZlecenieId(new Integer(this.zlecenieId));
		zlecenie.setGwaracjaProducenta(new Boolean(this.gwarancjiaProducenta));

		zlecenie.setKlient((Klient) this.klient.clone());
		zlecenie.setUrzadzenie((Urzadzenie) this.urzadzenie.clone());
		return zlecenie;
	}
}
