package cennik;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Klasa reperentujaca cennik zawiera liste {@link cennik.PozycjaCennika}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Cennik implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<PozycjaCennika> uslugi = null;

	public Cennik() {
		uslugi = new ArrayList<PozycjaCennika>(20);
	}

	public void dodajUsluge(PozycjaCennika usluga) {
		uslugi.add(usluga);
	}

	public Iterator<PozycjaCennika> iterator() {
		return uslugi.iterator();
	}

	public void usunUsluge(PozycjaCennika usluga) {
		Iterator<PozycjaCennika> iterator = uslugi.iterator();
		PozycjaCennika pz = null;
		while (iterator.hasNext()) {
			pz = iterator.next();
			if (pz.equals(usluga))
				uslugi.remove(pz);
		}
	}
}
