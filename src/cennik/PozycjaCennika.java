package cennik;

import java.io.Serializable;

/**
 * Kalasa reprezentujaca pojedyncza pozycje {@link cennik.Cennik}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class PozycjaCennika implements Serializable {

	private static final long serialVersionUID = 1L;
	private String usluga = null;
	private Integer koszt = 0, czas = 0;

	public PozycjaCennika() {
	}

	public void setUsluga(String usluga) {
		this.usluga = usluga;
	}

	public void setCzasRealizacji(Integer czas) {
		this.czas = czas;
	}

	public void setKoszt(Integer koszt) {
		this.koszt = koszt;
	}

	public String getUsluga() {
		return this.usluga;
	}

	public Integer getCzas() {
		return this.czas;
	}

	public Integer getKoszt() {
		return this.koszt;
	}

	@Override
	public String toString() {
		return new String(this.usluga + " " + this.koszt + " " + this.czas);
	}

	@Override
	public boolean equals(Object obj) {
		Integer i = 0;
		// ((PozycjaCennika)obj).
		if (this.usluga.equals(((PozycjaCennika) obj).getUsluga()))
			i++;
		if (this.koszt.equals(((PozycjaCennika) obj).getKoszt()))
			i++;
		if (this.czas.equals(((PozycjaCennika) obj).getCzas()))
			i++;
		return i == 3 ? true : false;
	}
}
