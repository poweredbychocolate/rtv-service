package siec;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Uzytkownicy.FabrykaUzytkownikow;
import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import Uzytkownicy.UzytkownikSystemu;
import cennik.Cennik;
import cennik.PozycjaCennika;
import database.DBMenadzer;
import statystyki.ElementStatystyki;
import statystyki.Statystyka;
import statystyki.TypStatystyk;
import statystyki.listy.NaprawioneUrzadzenia;
import zlecenia.Urzadzenie;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosc;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.czynnosci.Diagnoza;
import zlecenia.czynnosci.Naprawa;
import zlecenia.statusy.Status;

/**
 * Obsluga gniazda sieciowego po stronie serwera {@link serwer.Serwer} Zawiera
 * metody ocztytu i/lub zapisu dla wszystki operacji
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class GniazdoSerwera {
	private Socket gniazdo = null;
	private ObjectInputStream obIn;
	private ObjectOutputStream obOut;
	private Komenda komenda = null;
	/**
	 * Tworzy obiekt wraz ze strumieniami obiektow
	 * 
	 * @param socket {@link Socket}
	 * @throws IOException {@link IOException}
	 */
	public GniazdoSerwera(Socket socket) throws IOException {
		this.gniazdo = socket;
		this.obOut = new ObjectOutputStream(gniazdo.getOutputStream());
		this.obIn = new ObjectInputStream(new BufferedInputStream(gniazdo.getInputStream()));
		try {
			DBMenadzer.getInstance();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	////
	////
	/**
	 * Medoda odczytujaca {@link siec.Komenda}
	 * @return Obiekt zawieracjy polecenie 
	 * @throws ClassNotFoundException {@link ClassNotFoundException}
	 * @throws IOException {@link IOException}
	 */
	public synchronized Komenda odczytajKomende() throws ClassNotFoundException, IOException {
		komenda = (Komenda) obIn.readObject();
		return komenda;
	}

	/////
	/////
	public synchronized  String[] sprawdzUzytkownikaOdczytaj() {
		String[] logowanie = new String[2];
		String tmpl = new String();
		String tmph = new String();
		try {
			tmpl = this.obIn.readUTF();
			tmph = this.obIn.readUTF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logowanie[0] = tmpl;
		logowanie[1] = tmph;
		return logowanie;
	}

	public synchronized  void sprawdzUzytkownikaZapisz(Boolean status) {
		try {
			obOut.writeBoolean(status);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  String[] sprawdzDaneLogowaniaOdczytaj() {
		String[] logowanie = new String[2];
		String tmpl = new String();
		String tmph = new String();
		try {
			tmpl = this.obIn.readUTF();
			tmph = this.obIn.readUTF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logowanie[0] = tmpl;
		logowanie[1] = tmph;
		return logowanie;
	}

	public synchronized  void sprawdzDaneLogowaniaZapisz(Integer id) {
		try {
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  Integer wczytajKlientaOdczytaj() {
		Integer id = 0;
		try {
			id = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	public synchronized  Klient listaZlecenOdczytaj() {
		Klient k=null;
		try {
			k = (Klient) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return k;
	}

	public synchronized  void wczytajKlientaZapisz(Klient klient) {
		try {
			obOut.writeObject(klient);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  Integer wczytajUzytkownikaOdczytaj() {
		Integer id = 0;
		try {
			id = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public synchronized  void wczytajUzytkownikaZapisz(Uzytkownik uzytkownik) {
		try {
			obOut.writeObject(uzytkownik);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  Integer wczytajPracownikaOdczytaj() {
		Integer id = 0;
		try {
			id = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public synchronized  void wczytajPracownikaZapisz(Pracownik pracownik) {
		try {
			obOut.writeObject(pracownik);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  void listaPracownikow(ResultSet rs) {
		ArrayList<Pracownik> lista = new ArrayList<Pracownik>();
		try {
			while (rs.next()) {
				Pracownik tmpp = (Pracownik) FabrykaUzytkownikow.getUzytkownik("Pracownik");
				tmpp.setImie(rs.getString("Imie"));
				tmpp.setNazwisko(rs.getString("Nazwisko"));
				if (rs.getString("Aktywny").equals("TAK"))
					tmpp.setAktywny();
				else
					tmpp.setNieaktywny();
				lista.add(tmpp);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			obOut.writeObject(lista);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public  synchronized  void listaKlientow(ResultSet rs) {
		ArrayList<Klient> lista = new ArrayList<Klient>();
		try {
			while (rs.next()) {
				Klient tmpk = (Klient) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.KLIENT);
				tmpk.setImie(rs.getString("Imie"));
				tmpk.setNazwisko(rs.getString("Nazwisko"));
				tmpk.setTelefon(rs.getString("Telefon"));
				lista.add(tmpk);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			obOut.writeObject(lista);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  UzytkownikSystemu[] dodajPracownikaOdczytaj() {
		UzytkownikSystemu[] uz = new UzytkownikSystemu[2];
		try {
			uz[0] = (UzytkownikSystemu) obIn.readObject();
			uz[1] = (UzytkownikSystemu) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return uz;
	}

	public synchronized  void dodajPracownikaZapisz(Boolean status) {
		try {
			obOut.writeBoolean(status);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized  UzytkownikSystemu[] dodajKlientaOdczytaj() {
		UzytkownikSystemu[] uz = new UzytkownikSystemu[2];
		try {
			uz[0] = (UzytkownikSystemu) obIn.readObject();
			uz[1] = (UzytkownikSystemu) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return uz;
	}

	public synchronized  void dodajKlientaZapisz(Boolean status) {
		try {
			obOut.writeBoolean(status);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized  Klient resetKlientaOdczytajKlienta() {
		Klient klient = null;
		try {
			klient = (Klient) obIn.readObject();

		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return klient;
	}

	public synchronized  String[] resetKlientaOdczytajLogowanie() {
		String[] logowanie = new String[2];
		try {
			logowanie[0] = obIn.readUTF();
			logowanie[1] = obIn.readUTF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return logowanie;
	}

	public synchronized  void resetKlientaZapisz(Boolean status) {
		try {
			obOut.writeBoolean(status);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized  Status dodajStatusOdczytaj() {
		Status s = null;
		try {
			s = (Status) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

	public synchronized Urzadzenie dodajUrzadzenieOdczytaj() {
		Urzadzenie u = null;
		try {
			u = (Urzadzenie) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}

	public synchronized Zlecenie dodajZlecenieOdczytaj() {
		Zlecenie z = null;
		try {
			z = (Zlecenie) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return z;
	}

	public  synchronized Klient listaZlecenKlientaOdczytaj() {
		Klient klient = null;
		try {
			klient = (Klient) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return klient;
	}

	public synchronized  void listaZlecenKlientaZapisz(ResultSet rs) {
		ArrayList<Zlecenie> lista = new ArrayList<Zlecenie>();
		Zlecenie ztmp = null;
		Klient ktmp = null;
		Urzadzenie utmp = null;

		try {
			while (rs.next()) {
				ztmp = new Zlecenie();
				ktmp = (Klient) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.KLIENT);
				utmp = new Urzadzenie();

				ztmp.setZlecenieId(rs.getInt("ZlecenieId"));
				ktmp.setImie(rs.getString("Imie"));
				ktmp.setNazwisko(rs.getString("Nazwisko"));
				utmp.setMarka(rs.getString("Marka"));
				utmp.setModel(rs.getString("Model_Urzadzenia"));

				ztmp.setUrzadzenie(utmp);
				ztmp.setKlient(ktmp);
				lista.add(ztmp);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			obOut.writeObject(lista);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized Integer wczytajZlecenieOdczytaj() {
		Integer id = null;
		try {
			id = obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public synchronized void wczytajZlecenieZapisz(Zlecenie zlecenie) {
	
		try {
			obOut.writeObject(zlecenie);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// lista zlecen do poprawienia
	public  synchronized void listaZlecenZapisz(ArrayList<Zlecenie> zlecenia) {

		try {
			obOut.writeObject(zlecenia);
			obOut.flush();
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized PozycjaCennika wczytajPozycjeCennika() {
		PozycjaCennika pz = null;
		try {
			pz = (PozycjaCennika) obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pz;
	}

	public synchronized  void wyslijCennik(Cennik cennik) {
		try {
			obOut.writeObject(cennik);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public  synchronized Integer statystykaOdczytaj() {
		Integer typ = 0;
		try {
			typ = obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return typ;
	}

	public synchronized  void statystykaZapisz(ResultSet rs, Integer typ) {
		Statystyka st = null;
		ElementStatystyki estmp = null;

		try {
			if (typ == TypStatystyk.NAPRAWIANE_URZADZENIA) {
				st = new NaprawioneUrzadzenia();
				while (rs.next()) {
					estmp = new ElementStatystyki();

					estmp.setNazwa(rs.getString("Marka"));

					estmp.setWartosc(rs.getDouble("Ilosc"));
					st.dodaj(estmp);
				}
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			obOut.writeObject(st);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  Integer statusyZleceniaOdczytaj() {
		Integer id = 0;
		try {
			id = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public synchronized  void statusyZleceniaZapisz(ArrayList<Status> statusy) {

		try {
			obOut.writeObject(statusy);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized  Czynnosci wczytajCzynnosci() {
		Czynnosci cz = null;
		try {
			cz = (Czynnosci) obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cz;
	}

	public synchronized  void zapiszCzynnosci(ResultSet czynnosciRS) {
		Czynnosci czynnosci = new Czynnosci();
		Czynnosc cz = null;
		try {
			while (czynnosciRS.next()) {
				if (czynnosciRS.getString("Typ").equals("DIAGNOZA"))
					cz = new Diagnoza();
				else
					cz = new Naprawa();
				cz.setKoszt(czynnosciRS.getInt("Koszt"));
				cz.setOpis(czynnosciRS.getString("Opis"));
				czynnosci.dodajCzynnosc(cz);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obOut.writeObject(czynnosci);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			czynnosciRS.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	////
	////
	public synchronized  Integer wczytajInt() {
		Integer id = 0;
		try {
			id = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	public synchronized  void zapiszInt(Integer integer) {
		try {
			obOut.writeInt(integer);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void zapiszStanOperacji(Boolean stan) {
		try {
			obOut.writeBoolean(stan);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	////
	public synchronized  void dispose() {
		try {
			this.obIn.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.obOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
