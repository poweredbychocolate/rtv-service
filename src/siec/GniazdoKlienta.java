package siec;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import cennik.Cennik;
import cennik.PozycjaCennika;
import statystyki.Statystyka;
import zlecenia.Urzadzenie;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.statusy.Przyjete;
import zlecenia.statusy.Status;

/**
 * Obsluga gniazda sieciowego po stronie klienta Zawiera metody ocztytu i/lub
 * zapisu dla wszystki operacji
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class GniazdoKlienta {
	private Socket gniazdo = null;
	private ObjectInputStream obIn;
	private ObjectOutputStream obOut;

	/**
	 * Tworzy obiekt wraz ze strumieniami obiektow
	 * 
	 * @param socket
	 *            {@link Socket}
	 * @throws IOException
	 *             {@link IOException}
	 */
	public GniazdoKlienta(Socket socket) throws IOException {
		this.gniazdo = socket;
		this.obOut = new ObjectOutputStream(new BufferedOutputStream(gniazdo.getOutputStream()));
		this.obIn = new ObjectInputStream(new BufferedInputStream(gniazdo.getInputStream()));
	}

	/**
	 * Komenda zamkniecia strumieni i polaczenia
	 * 
	 * @throws IOException
	 *             {@link IOException}
	 */
	public synchronized  void zamknij() throws IOException {
		Komenda k = new Komenda();
		k.setOperacja(Komenda.ZAMKNIJ);
		obOut.writeObject(k);
		obOut.flush();
	}

	public  synchronized Boolean sprawdzUzytkownika(String login, String haslo) {
		Boolean b = false;
		Komenda k = new Komenda();
		k.setOperacja(Komenda.SPRAWDZ_UZYTKOWNIKA);
		try {
			obOut.writeObject(k);
			obOut.writeUTF(login);
			obOut.writeUTF(haslo);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			b = this.obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public  synchronized Integer sprawdzDaneLogowania(String login, String haslo) {
		Integer i = 0;

		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LOGOWANIE);
			obOut.writeObject(k);
			obOut.writeUTF(login);
			obOut.writeUTF(haslo);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			i = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

	public  synchronized Integer wczytajLiczbeZlecen(Klient klient) {
		Integer i = 0;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LICZBA_ZLECEN);
			obOut.writeObject(k);
			obOut.writeObject(klient);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			i = this.obIn.readInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;
	}

	public synchronized  Klient wczytajKlienta(Integer id) {
		Klient kl = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.WCZYTAJ_KLIENTA);
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			kl = (Klient) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return kl;
	}

	public synchronized  Uzytkownik wczytajUzytkownika(Integer id) {
		Uzytkownik u = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.WCZYTAJ_UZYTKOWNIKA);
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			u = (Uzytkownik) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}

	public synchronized  Pracownik wczytajPracownika(Integer id) {
		Pracownik p = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.WCZYTAJ_PRACOWNIKA);
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			p = (Pracownik) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}

	public  synchronized ArrayList<Pracownik> listaPracownikow() {
		ArrayList<Pracownik> lista = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LISTA_PRACOWNIKOW);
			obOut.writeObject(k);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista = (ArrayList<Pracownik>) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	public synchronized  ArrayList<Klient> listaKlientow() {
		ArrayList<Klient> lista = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LISTA_KLIENTOW);
			obOut.writeObject(k);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista = (ArrayList<Klient>) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	public synchronized  Boolean dodajPracownika(Uzytkownik uzytkownik, Pracownik pracownik) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.DODAJ_PRACOWNIKA);
		try {
			obOut.writeObject(k);
			obOut.writeObject(uzytkownik);
			obOut.writeObject(pracownik);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean dodajKlienta(Uzytkownik uzytkownik, Klient klient) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.DODAJ_KLIENTA);
		try {
			obOut.writeObject(k);
			obOut.writeObject(uzytkownik);
			obOut.writeObject(klient);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean resetujKlienta(Klient klient, String login, String haslo) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.RESET_LOGOWANIA);
		try {
			obOut.writeObject(k);
			obOut.writeObject(klient);
			obOut.writeUTF(login);
			obOut.writeUTF(haslo);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean dodajStatus(Status status) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.DODAJ_STATUS);
		try {
			obOut.writeObject(k);
			if (status instanceof Przyjete)
				obOut.writeObject(((Przyjete) status));
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean dodajUrzadzenie(Urzadzenie urzadzenie) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.DODAJ_URZADZENIE);
		try {
			obOut.writeObject(k);
			obOut.writeObject(urzadzenie);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean dodajZlecenie(Zlecenie zlecenie) {
		Boolean b = new Boolean(false);
		Komenda k = new Komenda();
		k.setOperacja(Komenda.DODAJ_ZLECENIE);
		try {
			obOut.writeObject(k);
			obOut.writeObject(zlecenie);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  ArrayList<Zlecenie> listaZlecenKlienta(Klient klient) {
		ArrayList<Zlecenie> lista = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LISTA_ZLECEN_KLIENTA);
			obOut.writeObject(k);
			obOut.writeObject(klient);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista = (ArrayList<Zlecenie>) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	public synchronized  Zlecenie wczytajZlecenie(Integer zlecenieId) {
		Zlecenie z = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.WCZYTAJ_ZLECENIE);
			obOut.writeObject(k);
			obOut.writeInt(zlecenieId);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			z = (Zlecenie) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return z;
	}

	public synchronized  ArrayList<Zlecenie> listaZlecen() {
		ArrayList<Zlecenie> lista = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.LISTA_ZLECEN);
			obOut.writeObject(k);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista = (ArrayList<Zlecenie>) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;

	}

	public synchronized  ArrayList<Zlecenie> pelnaListaZlecenKlienta(Klient klient) {
		ArrayList<Zlecenie> lista = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.PELNA_LISTA_ZLECEN_KLIENTA);
			obOut.writeObject(k);
			obOut.writeObject(klient);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			lista = (ArrayList<Zlecenie>) this.obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;

	}

	public synchronized  Boolean zmienStatusZlecenia(Zlecenie zlecenie, Status status) {
		Boolean b = false;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.ZMIEN_STATUS);
			obOut.writeObject(k);
			obOut.writeObject(zlecenie);
			obOut.writeObject(status);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;

	}

	public synchronized  Boolean dodajPozycjecennika(PozycjaCennika pozycja) {
		Boolean b = false;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.CENNIK_DODAJ);
			obOut.writeObject(k);
			obOut.writeObject(pozycja);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;

	}

	public synchronized  Cennik wczytajcennik() {
		Cennik cennik = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.CENNIK_LISTA);
			obOut.writeObject(k);
			obOut.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			cennik = (Cennik) obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cennik;
	}

	public synchronized  Boolean anulujZlecenie(Klient klient, Zlecenie zlecenie) {
		Boolean b = false;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.ANULUJ_ZLECENIE);
			obOut.writeObject(k);
			obOut.writeObject(klient);
			obOut.writeObject(zlecenie);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean usunPozycjecennika(PozycjaCennika pozycja) {
		Boolean b = false;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.CENNIK_USUN);
			obOut.writeObject(k);
			obOut.writeObject(pozycja);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public synchronized  Boolean zapiszCzynosci(Czynnosci czynnosci, Integer id) {
		Boolean b = false;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.ZAPISZ_CZYNNOSCI);
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.writeObject(czynnosci);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b = obIn.readBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	public  synchronized Statystyka statystyka(Integer typStatystyki) {
		Statystyka st = null;
		try {
			Komenda k = new Komenda();
			k.setOperacja(Komenda.STATYSTYKA);
			obOut.writeObject(k);
			obOut.writeInt(typStatystyki);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			st = (Statystyka) obIn.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return st;
	}

	public synchronized  ArrayList<Status> statusyzlecenia(Integer id) {
		ArrayList<Status> lista = null;
		Komenda k = new Komenda();
		k.setOperacja(Komenda.STATUSY_ZLECENIA);
		try {
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			lista = (ArrayList<Status>) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	public synchronized  Czynnosci wczytajCzynnosci(Integer id) {
		Czynnosci czy = null;
		Komenda k = new Komenda();
		k.setOperacja(Komenda.WCZYTAJ_CZYNNOSCI);
		try {
			obOut.writeObject(k);
			obOut.writeInt(id);
			obOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			czy = (Czynnosci) obIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return czy;
	}
}
