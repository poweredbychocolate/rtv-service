package siec;
/**
 * Interfejs sterowania serwera {@link serwer.Serwer}
 * @author Dawid Brelak
 *	@version 1.0
 */
public interface Sterowanie {
	public Integer getOperacja();
	public void setOperacja(Integer operacja);
}
