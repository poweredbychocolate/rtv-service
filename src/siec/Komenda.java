package siec;

import java.io.Serializable;

/**
 * Sterowanie praca {@link serwer.Serwer} implementuje {@link siec.Sterowanie}
 * Zawiera pola statyczne bedace rodzajem operacji
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class Komenda implements Serializable, Sterowanie {

	private static final long serialVersionUID = 1L;
	// public static final Integer = ;
	public static final Integer ZAMKNIJ = 0;
	public static final Integer LOGOWANIE = 1;
	public static final Integer DODAJ_PRACOWNIKA = 2;
	public static final Integer USUN_PRACONIKA = 3;
	public static final Integer SPRAWDZ_UZYTKOWNIKA = 4;
	public static final Integer WCZYTAJ_KLIENTA = 5;
	public static final Integer WCZYTAJ_PRACOWNIKA = 6;
	public static final Integer WCZYTAJ_UZYTKOWNIKA = 7;
	public static final Integer LISTA_PRACOWNIKOW = 8;
	public static final Integer DODAJ_KLIENTA = 9;
	public static final Integer RESET_LOGOWANIA = 10;
	public static final Integer DODAJ_STATUS = 11;
	public static final Integer LISTA_KLIENTOW = 12;
	public static final Integer DODAJ_URZADZENIE = 14;
	public static final Integer DODAJ_ZLECENIE = 15;
	public static final Integer LISTA_ZLECEN_KLIENTA = 16;
	public static final Integer WCZYTAJ_ZLECENIE = 17;
	public static final Integer LISTA_ZLECEN = 18;
	public static final Integer ZMIEN_STATUS = 19;
	public static final Integer CENNIK_USUN = 20;
	public static final Integer CENNIK_LISTA = 21;
	public static final Integer CENNIK_DODAJ = 22;
	public static final Integer PELNA_LISTA_ZLECEN_KLIENTA = 23;
	public static final Integer ANULUJ_ZLECENIE = 24;
	public static final Integer STATYSTYKA = 25;
	public static final Integer STATUSY_ZLECENIA = 26;
	public static final Integer ZAPISZ_CZYNNOSCI = 27;
	public static final Integer WCZYTAJ_CZYNNOSCI = 28;
	public static final Integer LICZBA_ZLECEN = 29;
	// public static final Integer = ;

	//
	private Integer operacja = -1;

	public Komenda() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Integer getOperacja() {
		// TODO Auto-generated method stub
		return this.operacja;
	}

	@Override
	public void setOperacja(Integer operacja) {
		this.operacja = operacja;

	}

}
