package klientGUI;

import java.awt.Dimension;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import siec.GniazdoKlienta;
/**
 * Klasa zawieraca statyczne czesto wykorzystywane pola
 * @author Dawid Brelak
 * @version 1.0
 */
public class KlientDane {
	private static Uzytkownik uzytkownik = null;
	private static Pracownik pracownik = null;
	private static String adresSerwera = new String("192.168.13.45");
	private static String lokalnyadresSerwera = new String("127.0.0.1");
	private static Integer portSerwera = 5555;
	private static GniazdoKlienta gniazdoKlienta = null;
	private static String nl = System.lineSeparator();
	private static String miejscosc="Kielce";
	private static String naglowekRaportu = "Serwis RTV "+nl+
			"ul.Nieokreślona 00" +nl+
			"00-000 Nieokreślone" + nl+
			"tel. +48 000 000 000" +nl +
			"e-mail : email@poczta.pl" +nl;

	public static String getNl() {
		return nl;
	}

	public static String getNaglowekRaportu() {
		return naglowekRaportu;
	}

	public static void setUzytkownik(Uzytkownik u) {
		KlientDane.uzytkownik = u;
	}

	public static void setPracownik(Pracownik p) {
		KlientDane.pracownik = p;
	}

	public static Uzytkownik getUzytkownik() {
		return uzytkownik;
	}

	public static Pracownik getPracownik() {
		return pracownik;
	}

	public static String getSerwerAdres() {
		return adresSerwera;
	}
	public static String getLokalnySerwerAdres() {
		return lokalnyadresSerwera;
	}
	public static String getAdminAdres() {
		return new String("admin@poczta.pl");
	}

	public static InetAddress getSerwerInetAdress() {
		InetAddress a = null;
		try {
			a = InetAddress.getByName(adresSerwera);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return a;
	}
	public static InetAddress getLokalnySerwerInetAdress() {
		InetAddress a = null;
		try {
			a = InetAddress.getByName(lokalnyadresSerwera);
		} catch (UnknownHostException e) {

			e.printStackTrace();
		}
		return a;
	}

	public static Integer getSerwerPort() {
		return portSerwera;
	}

	public static void setGniazdoKlienta(GniazdoKlienta gniazdo) {
		gniazdoKlienta = gniazdo;
	}

	public static GniazdoKlienta getGniazdoKlienta() {
		return gniazdoKlienta;
	}
	
	public static String getBiezacyCzas()
	{
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		return format.format(date);
	}
	public static String getCzasIMiejscowosc()
	{
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		return new String(miejscosc+", "+format.format(date)+"  ");
	}
	public static void ustawRozmiarPrzyciskow(JButton przycisk, Dimension buttonSize) {
		przycisk.setSize(buttonSize);
		przycisk.setPreferredSize(buttonSize);
		przycisk.setMinimumSize(buttonSize);
		przycisk.setMaximumSize(buttonSize);
	}
}
