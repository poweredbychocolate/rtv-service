package klientGUI.poziom1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import klientGUI.zlecenieG.GraficzneZlecenie;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;


public class ZleceniaKlienta extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Klient klient = null;
	private Zlecenie zlecenie = null;
	private GniazdoKlienta gniazdo = null;
	private KartaZlecenia wybranaKarta = null;
	private Dimension rozmiarScroll = new Dimension(300, 600);
	private GraficzneZlecenie zlecenieG;
	/////
	/////
	private JPanel topBar, zleceniePanel;
	private JScrollPane zlecenieScrollPanel;
	private JLabel topLabel;


	public ZleceniaKlienta(Klient klient) {
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.klient = klient;
		this.setLayout(new BorderLayout());
		initTopBar();
		initKlientScrollPanel();
	}

	private void initTopBar() {
		topBar = new JPanel();
		topLabel = new JLabel("Lista Zlecen ");
		topBar.setLayout(new BoxLayout(topBar, BoxLayout.X_AXIS));

		topBar.add(Box.createHorizontalGlue());
		topBar.add(topLabel);
		topBar.add(Box.createHorizontalGlue());
		this.add(topBar, BorderLayout.NORTH);
	}

	private void initKlientScrollPanel() {
		zlecenieScrollPanel = new JScrollPane();
		zlecenieScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		zlecenieScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		zleceniePanel = new JPanel();
		zleceniePanel.setLayout(new GridLayout(0, 6, 4, 4));
		wczytajZlecenia();
		zlecenieScrollPanel.getViewport().add(zleceniePanel);
		zlecenieScrollPanel.setPreferredSize(rozmiarScroll);
		this.add(zlecenieScrollPanel, BorderLayout.CENTER);
	}

	private synchronized void wczytajZlecenia() {
		ArrayList<Zlecenie> zlecenia = null;
		zlecenia = gniazdo.pelnaListaZlecenKlienta(klient);
		Iterator<Zlecenie> iteratorZlecen = zlecenia.iterator();
		while (iteratorZlecen.hasNext()) {
			zleceniePanel.add(new KartaZlecenia(iteratorZlecen.next()));
		}
	}

	

	class KartaZlecenia extends JPanel {
		private JButton wybierzButton = new JButton("Wybierz");
		private Color kolor = Color.YELLOW.brighter();
		private Color kolorWybranego = Color.GREEN.brighter();
		private JLabel imieLabel, nazwiskoLabel, markaLabel, modelLabel, statusLabel;
		private Dimension buttonSize = new Dimension(80, 20);
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Zlecenie z = null;
		private KartaZlecenia kz = null;

		public KartaZlecenia(Zlecenie wczytaneZlecenie) {
			this.z = wczytaneZlecenie;

			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			this.kz = this;
			initElementy();
			initPanel();

		}

		private void initElementy() {
			imieLabel = new JLabel(z.getKlienta().getImie());
			nazwiskoLabel = new JLabel(z.getKlienta().getNazwisko());
			modelLabel = new JLabel(z.getUrzadzenie().getModel());
			markaLabel = new JLabel(z.getUrzadzenie().getMarka());
			statusLabel = new JLabel(z.getStatus().getClass().getSimpleName());
			////
			wybierzButton.addActionListener(wybierzListener);
			wybierzButton.setMinimumSize(buttonSize);
		}

		private void initPanel() {
			this.add(new JLabel("KLIENT"));
			this.add(imieLabel);
			this.add(nazwiskoLabel);
			this.add(new JLabel("URZADZENIE"));
			this.add(markaLabel);
			this.add(modelLabel);
			this.add(statusLabel);
			this.add(wybierzButton);
		}

		void reset() {
			this.setBackground(kolor);
		}

		ActionListener wybierzListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (wybranaKarta == null) {
					wybranaKarta = kz;
					zlecenie = z;
				} else {
					wybranaKarta.reset();
					wybranaKarta = kz;
					zlecenie = z;
					zlecenieG.zamkijOkno();
				}
				kz.setBackground(kolorWybranego);

				zlecenieG=new GraficzneZlecenie(zlecenie);
			}
		};

	}
}
