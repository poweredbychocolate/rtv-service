package klientGUI.poziom1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import cennik.Cennik;
import cennik.PozycjaCennika;
import klientGUI.KlientDane;
import klientGUI.poziom3.cennik.GraficznaPozycjaCennika;
import siec.GniazdoKlienta;

public class CennikPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GniazdoKlienta gniazdo = null;
	////

	////
	private JPanel srodkowyPanel;
	private JScrollPane srodkowyScrollPanel;
	private Dimension rozmiarPanelu = new Dimension(300, 450);
	
	public CennikPanel() {
		gniazdo=KlientDane.getGniazdoKlienta();
		this.setLayout(new GridLayout(0, 1));
		initSrodkowyPanel();
		
	}
	
	private void initSrodkowyPanel() {
		srodkowyPanel = new JPanel();
		srodkowyPanel.setLayout(new GridLayout(0, 1));
		srodkowyScrollPanel = new JScrollPane();
		srodkowyScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		srodkowyScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		srodkowyPanel.add(new NaglowekPanel());
		wczytajCennik();
		srodkowyScrollPanel.getViewport().add(srodkowyPanel);
		srodkowyScrollPanel.setPreferredSize(rozmiarPanelu);
		// srodkowyPanel.add(menuPanel,BorderLayout.NORTH);
		this.add(srodkowyScrollPanel, BorderLayout.CENTER);
	}
	private synchronized void wczytajCennik()
	{
		Cennik c = gniazdo.wczytajcennik();
		Iterator<PozycjaCennika> citerator =c.iterator();
		while (citerator.hasNext()) {
			srodkowyPanel.add(new GraficznaPozycjaCennika(citerator.next(),false));
			
		}
	}

	private class NaglowekPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JLabel uslugaLabel,kosztLabel,czasLabel;
		public NaglowekPanel() {
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			init();
		}
	private void init()
	{
		uslugaLabel = new JLabel("Usluga");
		kosztLabel = new JLabel("KosztUslugi");
		czasLabel = new JLabel("Czas realizacji");
		this.add(Box.createHorizontalStrut(5));
		this.add(uslugaLabel);
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(5));
		this.add(kosztLabel);
		this.add(Box.createHorizontalStrut(9));
		this.add(czasLabel);
		this.add(Box.createHorizontalStrut(5));
	}
	}

}
