package klientGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Panel informujacy o nie dostepnosci uslugi
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class KlientBrakDostepu extends JFrame {
	private static final long serialVersionUID = 1L;
	private Dimension size = new Dimension(400, 300);
	private JFrame mainFrame;
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	private JLabel infoLabel = new JLabel("Serwis niedostepny ");
	private JPanel gornyPanel, logoBox, infoBox, panel;
	////

	public KlientBrakDostepu() {
		initUI();
	}
	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		mainFrame.setMaximumSize(size);
		mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());
		///
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));
		infoBox.setBackground(Color.RED);
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu

		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
		///
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(Box.createVerticalStrut(50));
		String str = new String("    Serwis tymczasowo niedost�pny, spr�buj ponownie p�niej");
		panel.add(new JLabel(str));
		panel.add(Box.createVerticalStrut(3));
		str = new String("    b�d� skontaktuj sie z administratorem : "+KlientDane.getAdminAdres());
		panel.add(new JLabel(str));
		panel.add(Box.createVerticalStrut(3));
	
		panel.add(Box.createVerticalGlue());
		mainFrame.add(panel, BorderLayout.CENTER);
		mainFrame.pack();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new KlientBrakDostepu());
	}


}
