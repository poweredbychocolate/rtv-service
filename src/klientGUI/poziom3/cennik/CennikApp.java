package klientGUI.poziom3.cennik;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import cennik.Cennik;
import cennik.PozycjaCennika;
import klientGUI.KlientDane;
import klientGUI.KlientPracownik;
import siec.GniazdoKlienta;

public class CennikApp extends JFrame {

	private static final long serialVersionUID = 1L;
	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private GniazdoKlienta gniazdo = null;
	////

	private static JFrame mainFrame;
	private Dimension size = new Dimension(600, 500);
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	////
	private JLabel infoLabel;
	private JPanel gornyPanel, logoBox, infoBox, srodkowyPanel;
	private JScrollPane srodkowyScrollPanel;
	private PozycjaPanel pozycjaPanel;
	private Dimension rozmiarPanelu = new Dimension(300, 450);

	public CennikApp(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.pracownik = pracownik;
		this.uzytkownik = uzytkownik;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		// mainFrame.setMaximumSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());

		initGornyPanel();
		initSrodkowyPanel();

		///
		mainFrame.pack();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new CennikApp(u, p));
	}

	private void initGornyPanel() {
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
	}

	private void initSrodkowyPanel() {
		srodkowyPanel = new JPanel();
		srodkowyPanel.setLayout(new GridLayout(0, 1));
		srodkowyScrollPanel = new JScrollPane();
		srodkowyScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		srodkowyScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		srodkowyPanel.add(new NaglowekPanel());
		wczytajCennik();
		pozycjaPanel = new PozycjaPanel(this);
		srodkowyPanel.add(pozycjaPanel);
		srodkowyScrollPanel.getViewport().add(srodkowyPanel);
		srodkowyScrollPanel.setPreferredSize(rozmiarPanelu);
		// srodkowyPanel.add(menuPanel,BorderLayout.NORTH);
		mainFrame.add(srodkowyScrollPanel, BorderLayout.CENTER);
	}
	private void wczytajCennik()
	{
		Cennik c = gniazdo.wczytajcennik();
		Iterator<PozycjaCennika> citerator =c.iterator();
		while (citerator.hasNext()) {
			srodkowyPanel.add(new GraficznaPozycjaCennika(citerator.next(),true));
			
		}
	}
	public void wyswietlNowaPozycje(PozycjaCennika pozycja)
	{
		
		srodkowyPanel.remove(pozycjaPanel);
		srodkowyPanel.add(new GraficznaPozycjaCennika(pozycja,true));
		srodkowyPanel.add(pozycjaPanel);
		srodkowyPanel.validate();
		mainFrame.validate();
		srodkowyPanel.repaint();
		mainFrame.repaint();
	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			new KlientPracownik(uzytkownik, pracownik);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};
	private class NaglowekPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JLabel uslugaLabel,kosztLabel,czasLabel;
		public NaglowekPanel() {
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			init();
		}
	private void init()
	{
		uslugaLabel = new JLabel("Usluga");
		kosztLabel = new JLabel("KosztUslugi");
		czasLabel = new JLabel("Czas realizacji");
		this.add(Box.createHorizontalStrut(5));
		this.add(uslugaLabel);
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(5));
		this.add(kosztLabel);
		this.add(Box.createHorizontalStrut(9));
		this.add(czasLabel);
		this.add(Box.createHorizontalStrut(70));
	}
	}

}
