package klientGUI.poziom3.cennik;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import cennik.PozycjaCennika;
import klientGUI.KlientDane;

public class PozycjaPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField uslugaField,kosztField,czasField;
	private JButton dodajButton;
	private CennikApp glownyPanel;
	
	public PozycjaPanel(CennikApp panelZawerajcy) {
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.glownyPanel =panelZawerajcy;
		init();
	}
private void init()
{
	uslugaField = new JTextField(30);
	kosztField = new JTextField(9);
	czasField = new JTextField(3);
	dodajButton = new JButton("Dodaj");
	dodajButton.addActionListener(dodajAl);
	
	this.add(Box.createHorizontalStrut(5));
	this.add(uslugaField);
	this.add(Box.createHorizontalStrut(5));
	this.add(Box.createHorizontalGlue());
	this.add(Box.createHorizontalStrut(5));
	this.add(kosztField);
	this.add(Box.createHorizontalStrut(5));
	this.add(czasField);
	this.add(Box.createHorizontalStrut(5));
	this.add(dodajButton);
	this.add(Box.createHorizontalStrut(5));

}

	ActionListener dodajAl = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			PozycjaCennika  pz = new PozycjaCennika();
			if(!uslugaField.getText().isEmpty())
			pz.setUsluga(uslugaField.getText());
			else pz.setUsluga(" ");
			try {
				String tmp="";
				tmp=kosztField.getText();
				if(tmp.length()>4){
					tmp=tmp.substring(0, 4);
					pz.setKoszt(Integer.parseInt(tmp));
				}else
					pz.setKoszt(Integer.parseInt(tmp));
				tmp=czasField.getText();
				if(tmp.length()>3){
					tmp=tmp.substring(0, 3);
					pz.setCzasRealizacji(Integer.parseInt(tmp));
				}else
					pz.setCzasRealizacji(Integer.parseInt(tmp));

			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				pz.setKoszt(0);
				pz.setCzasRealizacji(0);
			}
			uslugaField.setText("");
			kosztField.setText("");
			czasField.setText("");
			KlientDane.getGniazdoKlienta().dodajPozycjecennika(pz);
			glownyPanel.wyswietlNowaPozycje(pz);

		}
	};
}