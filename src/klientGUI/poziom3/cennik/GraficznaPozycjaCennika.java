package klientGUI.poziom3.cennik;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cennik.PozycjaCennika;
import klientGUI.KlientDane;

public class GraficznaPozycjaCennika extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PozycjaCennika pz = null;
	private JLabel uslugaLabel, kosztLabel, czasLabel;
	private Boolean usunMode = false;
	private Dimension rozmiarPanelu = new Dimension(300, 20);

	public GraficznaPozycjaCennika(PozycjaCennika pozycja, Boolean przyciskUsun) {
		this.pz = pozycja;
		this.usunMode = przyciskUsun;
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setPreferredSize(rozmiarPanelu);
		this.setMinimumSize(rozmiarPanelu);
		this.setMinimumSize(rozmiarPanelu);
		init();
	}

	private void ukryj() {
		this.setVisible(false);
	}
	private JPanel referencja()
	{
		return this;
	}

	private void init() {
		uslugaLabel = new JLabel(pz.getUsluga());
		kosztLabel = new JLabel(pz.getKoszt().toString());
		Integer tmp = pz.getCzas().toString().length();
		String stmp = "";
		if (tmp == 1)
			stmp = "    ";
		if (tmp == 2)
			stmp = "  ";
		czasLabel = new JLabel(new String(stmp + pz.getCzas().toString()));
		////
		this.add(Box.createHorizontalStrut(5));
		this.add(uslugaLabel);
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(5));
		this.add(kosztLabel);
		this.add(Box.createHorizontalStrut(75));
		this.add(czasLabel);
		this.add(Box.createHorizontalStrut(5));
		if (usunMode) {
			JButton usunButton = new JButton("Usun");
			ActionListener usunButtonAL = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (KlientDane.getGniazdoKlienta().usunPozycjecennika(pz)) {
						JOptionPane.showMessageDialog(referencja(), "Pozycja cennika usunieta !");
						ukryj();
					}
				}
			};
			usunButton.addActionListener(usunButtonAL);
			this.add(usunButton);
			this.add(Box.createHorizontalStrut(5));
		}
	}
}
