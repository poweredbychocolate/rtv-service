package klientGUI.poziom3.ZarzaniePracownikami;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Uzytkownicy.FabrykaUzytkownikow;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;

public class DodawaniePracownika extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel imieLabel,nazwiskoLabel,hasloLabel,loginLabel;
	private JTextField imieField,nazwiskoField,loginField;
	private JPasswordField passwordField;
	private JCheckBox rootCheck; 
	private JButton dodajButton;
	private JPanel imieBox,nazwiskoBox,loginBox,passwordBox,buttonBox;
	private PracownikApp parentPanel;

	public DodawaniePracownika(PracownikApp parentPanel) {
		this.parentPanel=parentPanel;
		initPanel();
	}

	private void initPanel()
	{
		this.initElemanty();
		this.formularz();
	}
	private void initElemanty()
	{
		this.imieLabel = new JLabel("Imie ");
		this.nazwiskoLabel = new JLabel("Nazwisko ");
		this.loginLabel = new JLabel(" Login ");
		this.hasloLabel = new JLabel("Haslo");
		////
		this.imieField = new JTextField(30);
		this.nazwiskoField = new JTextField(30);
		this.loginField = new JTextField(30);
		this.passwordField = new JPasswordField(30);
		this.rootCheck = new JCheckBox("Administrator ");
		////
		this.dodajButton = new JButton("Zatwierdz");
		this.dodajButton.addActionListener(dodajListener);
	}
	private void initBox()
	{
		this.imieBox =new JPanel();
		this.nazwiskoBox=new JPanel();
		this.loginBox=new JPanel();
		this.passwordBox=new JPanel();
		this.buttonBox = new JPanel();
		this.imieBox.setLayout(new BoxLayout(imieBox, BoxLayout.X_AXIS));
		this.nazwiskoBox.setLayout(new BoxLayout(nazwiskoBox, BoxLayout.X_AXIS));
		this.loginBox.setLayout(new BoxLayout(loginBox, BoxLayout.X_AXIS));
		this.passwordBox.setLayout(new BoxLayout(passwordBox, BoxLayout.X_AXIS));
		this.buttonBox.setLayout(new BoxLayout(buttonBox, BoxLayout.X_AXIS));
	}
	private void pracownikBox()
	{
		this.imieBox.add(Box.createHorizontalGlue());
		this.imieBox.add(Box.createHorizontalStrut(5));
		this.imieBox.add(imieLabel);
		this.imieBox.add(Box.createHorizontalStrut(3));
		this.imieBox.add(imieField);
		this.imieBox.add(Box.createHorizontalStrut(5));
		this.imieBox.add(Box.createHorizontalGlue());
		///
		this.nazwiskoBox.add(Box.createHorizontalGlue());
		this.nazwiskoBox.add(Box.createHorizontalStrut(5));
		this.nazwiskoBox.add(nazwiskoLabel);
		this.nazwiskoBox.add(Box.createHorizontalStrut(3));
		this.nazwiskoBox.add(nazwiskoField);
		this.nazwiskoBox.add(Box.createHorizontalStrut(5));
		this.nazwiskoBox.add(Box.createHorizontalGlue());
	}
	private void uzytkownikBox()
	{
		this.loginBox.add(Box.createHorizontalGlue());
		this.loginBox.add(Box.createHorizontalStrut(5));
		this.loginBox.add(loginLabel);
		this.loginBox.add(Box.createHorizontalStrut(3));
		this.loginBox.add(loginField);
		this.loginBox.add(Box.createHorizontalStrut(5));
		this.loginBox.add(Box.createHorizontalGlue());
		///
		this.passwordBox.add(Box.createHorizontalGlue());
		this.passwordBox.add(Box.createHorizontalStrut(5));
		this.passwordBox.add(hasloLabel);
		this.passwordBox.add(Box.createHorizontalStrut(3));
		this.passwordBox.add(passwordField);
		this.passwordBox.add(Box.createHorizontalStrut(5));
		this.passwordBox.add(Box.createHorizontalGlue());
	}
	private void buttonBox()
	{
		this.buttonBox.add(Box.createHorizontalGlue());
		this.buttonBox.add(Box.createHorizontalStrut(5));
		this.buttonBox.add(rootCheck);
		this.buttonBox.add(Box.createHorizontalStrut(3));
		this.buttonBox.add(dodajButton);
		this.buttonBox.add(Box.createHorizontalStrut(5));
		this.buttonBox.add(Box.createHorizontalGlue());
	}
	private void dodajPanele()
	{
		this.add(Box.createVerticalGlue());
		this.add(Box.createVerticalStrut(4));
		this.add(imieBox);
		this.add(Box.createVerticalStrut(4));
		this.add(nazwiskoBox);
		this.add(Box.createVerticalStrut(4));
		this.add(loginBox);
		this.add(Box.createVerticalStrut(4));
		this.add(passwordBox);
		this.add(Box.createVerticalStrut(4));
		this.add(buttonBox);
		this.add(Box.createVerticalStrut(4));
		this.add(Box.createVerticalGlue());
	}
	private void formularz()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.initBox();
		this.pracownikBox();
		this.uzytkownikBox();
		this.buttonBox();
		this.dodajPanele();
		
	}
	
private ActionListener dodajListener = new ActionListener() {
		Pracownik pracownik =null;
		Uzytkownik uzytkownik =null;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			GniazdoKlienta gk = KlientDane.getGniazdoKlienta();
			pracownik = (Pracownik) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.PRACOWNIK);
			uzytkownik = (Uzytkownik) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.UZYTKOWNIK);
			
			pracownik.setImie(imieField.getText());
			pracownik.setNazwisko(nazwiskoField.getText());
			pracownik.setAktywny();
			
			uzytkownik.setLogin(loginField.getText());
			uzytkownik.setHaslo(new String(passwordField.getPassword()));
			
			if(!rootCheck.isSelected())
			uzytkownik.setPoziomUprawnien(2);
			else
			uzytkownik.setPoziomUprawnien(3);		
			Boolean powodzenie=gk.dodajPracownika(uzytkownik, pracownik);
			if(!powodzenie)
			{
				JOptionPane.showMessageDialog(null, "Wprowadzony login jest zajety ");
				loginField.setText("");
			}
			else
			{
			JOptionPane.showMessageDialog(null, "Dodano nowego pracownika");
			parentPanel.przeladuj();
			loginField.setText("");
			imieField.setText("");
			nazwiskoField.setText("");
			passwordField.setText("");
			rootCheck.setSelected(false);
			}
		}
	};
}
