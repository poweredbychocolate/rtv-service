package klientGUI.poziom3.ZarzaniePracownikami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import Uzytkownicy.FabrykaUzytkownikow;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientDane;
import klientGUI.KlientPracownik;
import siec.GniazdoKlienta;

public class PracownikApp extends JFrame {

	private static final long serialVersionUID = 1L;
	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private GniazdoKlienta gniazdo = null;
	private ArrayList<Pracownik> pracownicy = null;
	////

	private static JFrame mainFrame;
	private Dimension size = new Dimension(600, 300);
	private Dimension pracownikPanelSize = new Dimension(800, 550);
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	////
	private JLabel infoLabel;
	private JPanel gornyPanel, logoBox, infoBox, srodkowyPanel, dodajPanel, pracownikPanel;
	private JScrollPane pracownikScrollPanel;

	public PracownikApp(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.pracownik = pracownik;
		this.uzytkownik = uzytkownik;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		// mainFrame.setMaximumSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());

		initGornyPanel();
		initSrodkowyPanel();

		///
		mainFrame.pack();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new PracownikApp(u, p));
	}

	private void initGornyPanel() {
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
	}

	private void initDodajPanel() {
		dodajPanel = new DodawaniePracownika(this);
		srodkowyPanel.add(dodajPanel, BorderLayout.NORTH);
	}

	private void initListaPracownikow() {
		pracownikPanel = new JPanel();
		pracownikPanel.setLayout(new GridLayout(0, 5, 4, 2));
		pracownicy = gniazdo.listaPracownikow();
		Iterator<Pracownik> iterator= pracownicy.iterator();
		while (iterator.hasNext()) {
			Pracownik tmpp = (Pracownik) FabrykaUzytkownikow.getUzytkownik("Pracownik");
			tmpp=iterator.next();
			pracownikPanel.add(new KartaPracownika(tmpp));
		}
		pracownikScrollPanel = new JScrollPane(pracownikPanel);
		pracownikScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pracownikScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pracownikScrollPanel.setMaximumSize(pracownikPanelSize);
		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		tmpPanel.add(pracownikScrollPanel);
		tmpPanel.add(Box.createVerticalStrut(1));
		tmpPanel.add(Box.createVerticalGlue());
		srodkowyPanel.add(tmpPanel, BorderLayout.CENTER);

	}

	private void initSrodkowyPanel() {
		srodkowyPanel = new JPanel();
		srodkowyPanel.setLayout(new BorderLayout());
		initDodajPanel();
		initListaPracownikow();
		// srodkowyPanel.add(menuPanel,BorderLayout.NORTH);
		mainFrame.add(srodkowyPanel, BorderLayout.CENTER);
	}
public void przeladuj()
{
	pracownikPanel.removeAll();
	pracownicy = gniazdo.listaPracownikow();
	Iterator<Pracownik> iterator= pracownicy.iterator();
	while (iterator.hasNext()) {
		Pracownik tmpp = (Pracownik) FabrykaUzytkownikow.getUzytkownik("Pracownik");
		tmpp=iterator.next();
		pracownikPanel.add(new KartaPracownika(tmpp));
	}
	mainFrame.validate();
	mainFrame.repaint();
}
private WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			new KlientPracownik(uzytkownik, pracownik);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};

}
