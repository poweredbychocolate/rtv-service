package klientGUI.poziom3.ZarzaniePracownikami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import Uzytkownicy.Pracownik;
import database.DBMenadzer;
import klientGUI.KlientDane;

public class KartaPracownika extends JPanel {
	private JPanel pracownikBox;
	private JButton usunButton = new JButton("Usun");
	private Color kolor=null;
	private JLabel imieLabel,nazwiskoLabel;
	private Dimension buttonSize = new Dimension(60, 30);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Pracownik p=null;

	
	public KartaPracownika(Pracownik pracownik) {
		this.p=pracownik;
		this.setLayout(new BorderLayout());
		this.setBackground(Color.YELLOW);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		if(p.getAktywny()) this.kolor = Color.YELLOW.brighter();
		else this.kolor = Color.RED;	
		initLabel();
		initPracownikBox();
		usunButton.addActionListener(usunListener);
		KlientDane.ustawRozmiarPrzyciskow(usunButton, buttonSize);
		this.add(usunButton,BorderLayout.CENTER);
	}
	private void initLabel()
	{
		imieLabel = new JLabel(p.getImie());
		nazwiskoLabel = new JLabel(p.getNazwisko());	
	}
	
	private void initPracownikBox()
	{
		pracownikBox = new JPanel();
		pracownikBox.setLayout(new BoxLayout(pracownikBox, BoxLayout.Y_AXIS));
		pracownikBox.setBackground(kolor);
		pracownikBox.add(Box.createHorizontalGlue());
		pracownikBox.add(Box.createHorizontalStrut(3));
		pracownikBox.add(imieLabel);
		pracownikBox.add(Box.createHorizontalStrut(3));
		pracownikBox.add(nazwiskoLabel);
		pracownikBox.add(Box.createHorizontalStrut(3));
		pracownikBox.add(Box.createHorizontalGlue());
	
		this.add(pracownikBox,BorderLayout.NORTH);
	}
	ActionListener usunListener = new ActionListener() {
		private DBMenadzer baza=null;
		@Override
		public void actionPerformed(ActionEvent e) {
			if(p.getAktywny()&&(!p.equals(KlientDane.getPracownik())))
			{
			try {
				baza= DBMenadzer.getInstance();

			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				baza.usunPracownika(p);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			p.setNieaktywny();
			kolor=Color.RED;
			pracownikBox.setBackground(kolor);

			// TODO Auto-generated method stub
			}
		}
	};

}
