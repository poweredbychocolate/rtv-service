package klientGUI.poziom3;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import klientGUI.KlientDane;
import klientGUI.KlientPracownik;
import klientGUI.poziom3.ZarzaniePracownikami.PracownikApp;
import klientGUI.poziom3.cennik.CennikApp;
import klientGUI.poziom3.statystykiGUI.StatystykiApp;

public class MenuPoziom3 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/////
	private JButton pracownicyButton,cennikButton,statystykiButton;
	/////

	public MenuPoziom3() {
	initPanel();
}
	private void  initButton()
	{
		this.pracownicyButton =new JButton("Pracownicy");
		this.pracownicyButton.addActionListener(pracownicyActionListener);
		this.cennikButton=new JButton("Cennik");
		this.cennikButton.addActionListener(cennikActionListener);
		this.statystykiButton=new JButton("Statystyki");
		this.statystykiButton.addActionListener(statystykiActionListener);
	}
	private  void addButton()
	{
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(5));
		this.add(pracownicyButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(statystykiButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(cennikButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalGlue());
	}
	
	private void initPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	
		this.initButton();
		this.ustawRozmiar();
		this.addButton();
	}
	static void ustawRozmiarPrzyciskow(JButton przycisk,Dimension buttonSize)
	{
		przycisk.setSize(buttonSize);
		przycisk.setPreferredSize(buttonSize);
		przycisk.setMinimumSize(buttonSize);
		przycisk.setMaximumSize(buttonSize);
	}
	private void ustawRozmiar()
	{
		Dimension size = new Dimension(130, 30);
		ustawRozmiarPrzyciskow(pracownicyButton, size);
		ustawRozmiarPrzyciskow(cennikButton, size);
		ustawRozmiarPrzyciskow(statystykiButton, size);
	}	

	private ActionListener pracownicyActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new PracownikApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	}; 
private ActionListener cennikActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new CennikApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	}; 
private ActionListener statystykiActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new StatystykiApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	}; 
}