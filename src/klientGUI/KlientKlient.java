package klientGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import Uzytkownicy.Klient;
import Uzytkownicy.Uzytkownik;
import klientGUI.poziom1.CennikPanel;
import klientGUI.poziom1.ZleceniaKlienta;
import siec.GniazdoKlienta;

/**
 * Klasa z panelem obslugi klienta
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class KlientKlient extends JFrame {
	private Klient klient = null;
	private GniazdoKlienta gniazdo = null;
	////
	private static final long serialVersionUID = 1L;
	private Dimension size = new Dimension(600, 500);
	private Dimension buttonSize = new Dimension(100, 30);
	private JFrame mainFrame;
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	private JLabel infoLabel;

	private JPanel gornyPanel, menuPanel, logoBox, infoBox;
	private JButton cennikButton, zleceniaButton;
	private CennikPanel cennikPanel;
	private ZleceniaKlienta zleceniaPanel;

	///
	/**
	 * Tworzy nowy obiekt z uzyciem {@link Uzytkownicy.Uzytkownik} i
	 * {@link Uzytkownicy.Klient}
	 * 
	 * @param uzytkownik
	 *            {@link Uzytkownicy.Uzytkownik}
	 * @param klient
	 *            {@link Uzytkownicy.Klient}
	 */
	public KlientKlient(Uzytkownik uzytkownik, Klient klient) {

		this.klient = klient;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		// mainFrame.setMaximumSize(size);
		// mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());
		///
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));
		infoBox.setBackground(Color.lightGray);
		infoLabel = new JLabel("Zalogowano jako klient " + klient.getImie() + " " + klient.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
		/// tworzenie panelu z menu
		menuPanel = new JPanel();
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));
		menuPanel.setBackground(Color.lightGray);
		//// przyciski
		zleceniaButton = new JButton("Zlecenia ");
		cennikButton = new JButton("Cennik");
		zleceniaButton.setSize(buttonSize);
		zleceniaButton.setPreferredSize(buttonSize);
		zleceniaButton.setMinimumSize(buttonSize);
		zleceniaButton.setMaximumSize(buttonSize);
		cennikButton.setSize(buttonSize);
		cennikButton.setPreferredSize(buttonSize);
		cennikButton.setMinimumSize(buttonSize);
		cennikButton.setMaximumSize(buttonSize);
		zleceniaButton.addActionListener(zleceniakAL);
		cennikButton.addActionListener(cennikAL);
		/////
		menuPanel.add(Box.createVerticalStrut(5));
		menuPanel.add(zleceniaButton);
		menuPanel.add(Box.createVerticalStrut(5));
		menuPanel.add(cennikButton);
		menuPanel.add(Box.createVerticalStrut(5));
		menuPanel.add(Box.createVerticalGlue());
		mainFrame.add(menuPanel, BorderLayout.WEST);
		initPanelRoboczy();
		mainFrame.pack();
	}

	private void initPanelRoboczy() {
		cennikPanel = new CennikPanel();
		mainFrame.add(cennikPanel, BorderLayout.CENTER);
	}

	public static void main(Uzytkownik u, Klient k) {
		SwingUtilities.invokeLater(() -> new KlientKlient(u, k));
	}

	ActionListener cennikAL = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
				if (zleceniaPanel != null)
					mainFrame.remove(zleceniaPanel);
				cennikPanel.setVisible(true);
				mainFrame.validate();
				mainFrame.repaint();
		}
	};
	ActionListener zleceniakAL = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			cennikPanel.setVisible(false);
			zleceniaPanel = new ZleceniaKlienta(klient);
			mainFrame.add(zleceniaPanel, BorderLayout.CENTER);
			mainFrame.validate();
			mainFrame.repaint();
			
		}
	};
	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			try {
				gniazdo.zamknij();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};
}
