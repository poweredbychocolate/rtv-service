package klientGUI.poziom2.zarzadzanieKlientami;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Uzytkownicy.FabrykaUzytkownikow;
import Uzytkownicy.Klient;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;

class KlientSzablon extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel imieLabel, nazwiskoLabel, hasloLabel, loginLabel, telefonLabel, trybLabel;
	private JTextField imieField, nazwiskoField, loginField, telefonField;
	private JPasswordField passwordField;
	private JButton zatwierdzButton, trybButton;
	private JPanel imieBox, nazwiskoBox, loginBox, passwordBox, buttonBox, telefonBox;
	private KlientApp parentFrame;

	GniazdoKlienta gk;

	KlientSzablon(KlientApp parentFrame) {
		this.parentFrame = parentFrame;
		gk = KlientDane.getGniazdoKlienta();
		initPanel();
	}

	private void initPanel() {
		this.initElemanty();
		this.formularz();
	}

	private void initElemanty() {
		this.imieLabel = new JLabel("Imie ");
		this.nazwiskoLabel = new JLabel("Nazwisko ");
		this.telefonLabel = new JLabel("Telefon :");
		this.loginLabel = new JLabel(" Login ");
		this.hasloLabel = new JLabel("Haslo");
		this.trybLabel = new JLabel("Rodzaj operacji : ");
		////
		this.imieField = new JTextField(30);
		this.nazwiskoField = new JTextField(30);
		this.telefonField = new JTextField(9);
		this.loginField = new JTextField(30);
		this.passwordField = new JPasswordField(30);
		////
		this.zatwierdzButton = new JButton("Zatwierdz");
		this.trybButton = new JButton("Dodaj klienta");

		this.trybButton.addActionListener(trybPracy);
		this.zatwierdzButton.addActionListener(dodajKlienta);

	}

	private void initBox() {
		this.imieBox = new JPanel();
		this.nazwiskoBox = new JPanel();
		this.telefonBox = new JPanel();
		this.loginBox = new JPanel();
		this.passwordBox = new JPanel();
		this.buttonBox = new JPanel();

		this.imieBox.setLayout(new BoxLayout(imieBox, BoxLayout.X_AXIS));
		this.nazwiskoBox.setLayout(new BoxLayout(nazwiskoBox, BoxLayout.X_AXIS));
		this.telefonBox.setLayout(new BoxLayout(telefonBox, BoxLayout.X_AXIS));
		this.loginBox.setLayout(new BoxLayout(loginBox, BoxLayout.X_AXIS));
		this.passwordBox.setLayout(new BoxLayout(passwordBox, BoxLayout.X_AXIS));
		this.buttonBox.setLayout(new BoxLayout(buttonBox, BoxLayout.X_AXIS));

	}

	private void klientBox() {
		this.imieBox.add(Box.createHorizontalGlue());
		this.imieBox.add(Box.createHorizontalStrut(5));
		this.imieBox.add(imieLabel);
		this.imieBox.add(Box.createHorizontalStrut(3));
		this.imieBox.add(imieField);
		this.imieBox.add(Box.createHorizontalStrut(5));
		this.imieBox.add(Box.createHorizontalGlue());
		///
		this.nazwiskoBox.add(Box.createHorizontalGlue());
		this.nazwiskoBox.add(Box.createHorizontalStrut(5));
		this.nazwiskoBox.add(nazwiskoLabel);
		this.nazwiskoBox.add(Box.createHorizontalStrut(3));
		this.nazwiskoBox.add(nazwiskoField);
		this.nazwiskoBox.add(Box.createHorizontalStrut(5));
		this.nazwiskoBox.add(Box.createHorizontalGlue());
		///
		this.telefonBox.add(Box.createHorizontalGlue());
		this.telefonBox.add(Box.createHorizontalStrut(5));
		this.telefonBox.add(telefonLabel);
		this.telefonBox.add(Box.createHorizontalStrut(3));
		this.telefonBox.add(telefonField);
		this.telefonBox.add(Box.createHorizontalStrut(5));
		this.telefonBox.add(Box.createHorizontalGlue());
	}

	private void uzytkownikBox() {
		this.loginBox.add(Box.createHorizontalGlue());
		this.loginBox.add(Box.createHorizontalStrut(5));
		this.loginBox.add(loginLabel);
		this.loginBox.add(Box.createHorizontalStrut(3));
		this.loginBox.add(loginField);
		this.loginBox.add(Box.createHorizontalStrut(5));
		this.loginBox.add(Box.createHorizontalGlue());
		///
		this.passwordBox.add(Box.createHorizontalGlue());
		this.passwordBox.add(Box.createHorizontalStrut(5));
		this.passwordBox.add(hasloLabel);
		this.passwordBox.add(Box.createHorizontalStrut(3));
		this.passwordBox.add(passwordField);
		this.passwordBox.add(Box.createHorizontalStrut(5));
		this.passwordBox.add(Box.createHorizontalGlue());
	}

	private void buttonBox() {
		this.buttonBox.add(Box.createHorizontalGlue());
		this.buttonBox.add(Box.createHorizontalStrut(5));
		this.buttonBox.add(trybLabel);
		this.buttonBox.add(Box.createHorizontalStrut(3));
		this.buttonBox.add(trybButton);
		this.buttonBox.add(Box.createHorizontalStrut(3));
		this.buttonBox.add(Box.createHorizontalGlue());
		this.buttonBox.add(zatwierdzButton);
		this.buttonBox.add(Box.createHorizontalStrut(5));
		this.buttonBox.add(Box.createHorizontalGlue());
	}

	private void dodajPanele() {
		this.add(Box.createVerticalGlue());
		this.add(Box.createVerticalStrut(4));
		this.add(imieBox);
		this.add(Box.createVerticalStrut(4));
		this.add(nazwiskoBox);
		this.add(Box.createVerticalStrut(4));
		this.add(telefonBox);
		this.add(Box.createVerticalStrut(4));
		this.add(loginBox);
		this.add(Box.createVerticalStrut(4));
		this.add(passwordBox);
		this.add(Box.createVerticalStrut(4));
		this.add(buttonBox);
		this.add(Box.createVerticalStrut(4));
		this.add(Box.createVerticalGlue());
	}

	private void formularz() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.initBox();
		this.klientBox();
		this.uzytkownikBox();
		this.buttonBox();
		this.dodajPanele();

	}

	ActionListener trybPracy = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (trybButton.getText().equals("Dodaj klienta")) {
				// telefonBox.setVisible(false);
				trybButton.setText("Resetuj dane Logowania");

				zatwierdzButton.removeActionListener(dodajKlienta);
				zatwierdzButton.addActionListener(resetKlienta);
			} else {
				// telefonBox.setVisible(true);
				trybButton.setText("Dodaj klienta");

				zatwierdzButton.removeActionListener(resetKlienta);
				zatwierdzButton.addActionListener(dodajKlienta);
			}

		}
	};

	ActionListener dodajKlienta = new ActionListener() {
		Klient klient = null;
		Uzytkownik uzytkownik = null;

		@Override
		public void actionPerformed(ActionEvent e) {

			klient = (Klient) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.KLIENT);
			uzytkownik = (Uzytkownik) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.UZYTKOWNIK);

			klient.setImie(imieField.getText());
			klient.setNazwisko(nazwiskoField.getText());
			String stmp = telefonField.getText();
			if (stmp.length() > 9)
				stmp = stmp.substring(0, 9);
			klient.setTelefon(stmp);

			uzytkownik.setLogin(loginField.getText());
			uzytkownik.setHaslo(new String(passwordField.getPassword()));

			Boolean powodzenie = gk.dodajKlienta(uzytkownik, klient);
			if (!powodzenie) {
				JOptionPane.showMessageDialog(null, "Wprowadzony login jest zajety ");
				loginField.setText("");
			} else {
				JOptionPane.showMessageDialog(null, "Dodano nowego klienta");
				loginField.setText("");
				imieField.setText("");
				telefonField.setText("");
				nazwiskoField.setText("");
				passwordField.setText("");
				parentFrame.przeladuj();
			}
		}
	};

	ActionListener resetKlienta = new ActionListener() {
		Klient klient = null;

		@Override
		public void actionPerformed(ActionEvent e) {

			klient = (Klient) FabrykaUzytkownikow.getUzytkownik(FabrykaUzytkownikow.KLIENT);
			klient.setImie(imieField.getText());
			klient.setNazwisko(nazwiskoField.getText());
			String stmp = telefonField.getText();
			if (stmp.length() > 9)
				stmp = stmp.substring(0, 9);
			klient.setTelefon(stmp);
			Boolean powodzenie = gk.resetujKlienta(klient, loginField.getText(),
					new String(passwordField.getPassword()));
			if (!powodzenie) {
				JOptionPane.showMessageDialog(null, "Wprowadzony login jest zajety ");
				loginField.setText("");
			} else {
				JOptionPane.showMessageDialog(null, "Zmieniono dane logowania");
				loginField.setText("");
				imieField.setText("");
				telefonField.setText("");
				nazwiskoField.setText("");
				passwordField.setText("");

			}
		}
	};

}
