package klientGUI.poziom2.zarzadzanieKlientami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;

class KlientLista extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GniazdoKlienta gniazdo = null;
	private Dimension rozmiarScroll = new Dimension(300, 300);
	/////
	/////
	JPanel topBar, klientPanel;
	JScrollPane klientScrollPanel;
	JLabel topLabel;

	KlientLista() {
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.setLayout(new BorderLayout());
		initKlientScrollPanel();
	}

	private void initKlientScrollPanel() {
		klientScrollPanel = new JScrollPane();
		klientScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		klientScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		klientScrollPanel.setPreferredSize(rozmiarScroll);
		klientPanel = new JPanel();
		klientPanel.setLayout(new GridLayout(0, 4, 4, 4));
		wczytajKlientow();
		klientScrollPanel.getViewport().add(klientPanel);
		klientScrollPanel.setPreferredSize(rozmiarScroll);
		this.add(klientScrollPanel, BorderLayout.CENTER);
	}

	public void wczytajKlientow() {
		klientPanel.removeAll();
		ArrayList<Klient> klienci = null;
		klienci = gniazdo.listaKlientow();
		Iterator<Klient> klientIterator = klienci.iterator();
		while (klientIterator.hasNext()) {
			klientPanel.add(new KartaKlienta(klientIterator.next()));
		}

	}

	class KartaKlienta extends JPanel {
		private Color kolor = Color.YELLOW.brighter();
		private JLabel imieLabel, nazwiskoLabel, telefonLabel;
		private static final long serialVersionUID = 1L;
		private Klient k = null;
		KartaKlienta(Klient klient) {
			this.k = klient;
			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			initElementy();
			initPanel();

		}

		private void initElementy() {
			imieLabel = new JLabel(k.getImie());
			nazwiskoLabel = new JLabel(k.getNazwisko());
			telefonLabel = new JLabel(k.getTelefon());
		}

		private void initPanel() {
			this.add(imieLabel);
			this.add(nazwiskoLabel);
			this.add(telefonLabel);
		}

	}
}