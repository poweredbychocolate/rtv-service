package klientGUI.poziom2.zarzadzanieKlientami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientPracownik;


public class KlientApp extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private static JFrame mainFrame;
	private Dimension size = new Dimension(600, 500);
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	////
	private JLabel infoLabel;
	private JPanel gornyPanel, logoBox, infoBox, srodkowyPanel;
	private KlientLista klienciPanel;

	public KlientApp(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.pracownik = pracownik;
		this.uzytkownik = uzytkownik;
		////
		initUI();
	}
	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		// mainFrame.setMaximumSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());

		initGornyPanel();
		initSrodkowyPanel();

		///
		mainFrame.pack();
	}
	public void przeladuj()
	{
		klienciPanel.wczytajKlientow();
		mainFrame.validate();
		mainFrame.repaint();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new KlientApp(u, p));
	}

	private void initGornyPanel() {
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
	}

	private void initSrodkowyPanel() {
		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		srodkowyPanel = new KlientSzablon(this);		
		klienciPanel = new KlientLista();
		tmpPanel.add(srodkowyPanel);
		tmpPanel.add(Box.createVerticalStrut(5));
		tmpPanel.add(klienciPanel);
		tmpPanel.add(Box.createVerticalStrut(2));
		mainFrame.add(tmpPanel, BorderLayout.CENTER);
	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			new KlientPracownik(uzytkownik, pracownik);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};

}
