package klientGUI.poziom2.raportyGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import klientGUI.KlientDane;
import raporty.Raport;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.czynnosci.Diagnoza;

class RaportPodsumownie extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RaportyApp panelZawierajacy;
	private Dimension rozmiarScroll = new Dimension(800, 500);
	private Dimension areaSize = new Dimension(100, 220);
	private Dimension dataSize = new Dimension(200, 40);
	/////
	/////
	private JPanel glownyPanel;
	private JScrollPane scrollPanel;
	private JTextArea naglowekField;
	private JTextField dataField;

	RaportPodsumownie(RaportyApp mainFrame) {
		this.panelZawierajacy=mainFrame;
		this.setLayout(new BorderLayout());
		initPanel();
	}

	private void initPanel() {
		glownyPanel = new JPanel();
		glownyPanel.setLayout(new BoxLayout(glownyPanel, BoxLayout.Y_AXIS));
		///
		wypelnijzawartosc();
		///
		scrollPanel = new JScrollPane(glownyPanel);
		scrollPanel.setPreferredSize(rozmiarScroll);
		scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.add(scrollPanel);

	}

	private void wypelnijzawartosc() {
		wypenijDaneSerwisu();
		wypelnijDaneKlienta();
		wypelnijCzynnosci();
		glownyPanel.add(Box.createVerticalGlue());
	}

	private void wypenijDaneSerwisu() {
		JPanel panelDatyINaglowka = new JPanel();
		panelDatyINaglowka.setLayout(new BoxLayout(panelDatyINaglowka, BoxLayout.X_AXIS));

		naglowekField = new JTextArea(KlientDane.getNaglowekRaportu(), 5, 20);
		naglowekField.setLineWrap(true);
		naglowekField.setPreferredSize(areaSize);
		naglowekField.setMaximumSize(areaSize);
		dataField = new JTextField(KlientDane.getCzasIMiejscowosc());
		dataField.setPreferredSize(dataSize);
		dataField.setMaximumSize(dataSize);

		panelDatyINaglowka.add(Box.createHorizontalStrut(2));
		panelDatyINaglowka.add(naglowekField);
		panelDatyINaglowka.add(Box.createHorizontalGlue());
		panelDatyINaglowka.add(Box.createHorizontalGlue());
		panelDatyINaglowka.add(dataField);
		panelDatyINaglowka.add(Box.createHorizontalStrut(2));
		glownyPanel.add(Box.createVerticalStrut(5));
		glownyPanel.add(panelDatyINaglowka);
	}

	private void wypelnijDaneKlienta() {
		Zlecenie zlecenie =panelZawierajacy.getRaport().getZlecenie();
		///
		JPanel poziomyPanel = new JPanel();
		poziomyPanel.setLayout(new BoxLayout(poziomyPanel, BoxLayout.X_AXIS));
		JPanel pionowyPanel = new JPanel();
		pionowyPanel.setLayout(new BoxLayout(pionowyPanel, BoxLayout.Y_AXIS));
		////
		JLabel naglowekLabel = new JLabel(new String("Zlecenie nr " + zlecenie.getZlecenieId()));
		naglowekLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		String string = new String(zlecenie.getKlienta().getImie() + "    " + zlecenie.getKlienta().getNazwisko());
		JLabel klientLabel = new JLabel(string);
		klientLabel.setFont(new Font(Font.SERIF, Font.BOLD, 25));
		string = new String(zlecenie.getUrzadzenie().getMarka() + "    " + zlecenie.getUrzadzenie().getModel());
		JLabel urzadzenieLabel = new JLabel(string);
		urzadzenieLabel.setFont(new Font(Font.SERIF, Font.BOLD, 25));
		
		////
		pionowyPanel.add(naglowekLabel);
		pionowyPanel.add(klientLabel);
		pionowyPanel.add(urzadzenieLabel);
		pionowyPanel.add(Box.createHorizontalStrut(3));
		////
		poziomyPanel.add(Box.createHorizontalGlue());
		// poziomyPanel.add(Box.createHorizontalGlue());
		poziomyPanel.add(pionowyPanel);
		// poziomyPanel.add(Box.createHorizontalGlue());
		poziomyPanel.add(Box.createHorizontalGlue());
		///
		glownyPanel.add(Box.createVerticalStrut(5));
		glownyPanel.add(poziomyPanel);
		// glownyPanel.add(klientPanel);
		glownyPanel.add(Box.createVerticalStrut(5));

	}

	private void wypelnijCzynnosci() {
		Czynnosci czynnosci = panelZawierajacy.getRaport().getCzynnosci();
		///
		JPanel poziomyPanel = new JPanel();
		poziomyPanel.setLayout(new BoxLayout(poziomyPanel, BoxLayout.X_AXIS));
		JPanel pionowyPanel = new JPanel();
		pionowyPanel.setLayout(new BoxLayout(pionowyPanel, BoxLayout.Y_AXIS));
		JPanel pionowyPanelNumeracji = new JPanel();
		pionowyPanelNumeracji.setLayout(new BoxLayout(pionowyPanelNumeracji, BoxLayout.Y_AXIS));
		////
		String tabela[][] =new String[czynnosci.iloscCzynosci()+1][3];
		String naglowki[] ={"L.P. ","Opis","Koszt"};
		Integer suma=0 ,i=0;
		for(i =0; i<czynnosci.iloscCzynosci();i++)
		{
			tabela[i][0]=new String((i+1 )+". ");
			tabela[i][1] =new String(czynnosci.getCzynnosc(i).getOpis());
			if(czynnosci.getCzynnosc(i) instanceof Diagnoza )tabela[i][2] = new String("");
			else{ tabela[i][2] = new String(czynnosci.getCzynnosc(i).getKoszt().toString()); 
			suma+=czynnosci.getCzynnosc(i).getKoszt();}
		}
		tabela[i][0]=new String("");
		tabela[i][1] =new String("");
		tabela[i][2] = new String(suma.toString());
		JTable tabelaPanel = new JTable(tabela,naglowki);
		tabelaPanel.setShowHorizontalLines(true);
		tabelaPanel.setShowVerticalLines(true);
		tabelaPanel.setCellSelectionEnabled(false);
		tabelaPanel.setEnabled(false);
		tabelaPanel.setBackground(Color.LIGHT_GRAY);
		tabelaPanel.getTableHeader().setBackground(Color.GRAY);
		tabelaPanel.setRowHeight(25);
		tabelaPanel.getColumn(naglowki[0]).setMaxWidth(30);
		DefaultTableCellRenderer rightRender = new DefaultTableCellRenderer();
		rightRender.setHorizontalAlignment( JLabel.RIGHT );		
		tabelaPanel.getColumn(naglowki[2]).setCellRenderer(rightRender);
		tabelaPanel.getColumn(naglowki[2]).setMaxWidth(50);	
		////
		pionowyPanel.add(Box.createHorizontalStrut(3));
		pionowyPanel.add(tabelaPanel.getTableHeader());
		pionowyPanel.add(tabelaPanel);
		pionowyPanel.add(Box.createHorizontalStrut(3));
		////
		poziomyPanel.add(Box.createHorizontalGlue());
		 poziomyPanel.add(Box.createHorizontalStrut(20));
		poziomyPanel.add(pionowyPanel);
		 poziomyPanel.add(Box.createHorizontalStrut(20));
		poziomyPanel.add(Box.createHorizontalGlue());
		///
		glownyPanel.add(Box.createVerticalStrut(5));
		glownyPanel.add(poziomyPanel);
		// glownyPanel.add(klientPanel);
		glownyPanel.add(Box.createVerticalStrut(5));

	}
	Raport generujRaport() {
		this.panelZawierajacy.zapiszDate(dataField.getText());
		this.panelZawierajacy.zapiszNaglowek(naglowekField.getText());
		return this.panelZawierajacy.getRaport();
	}
}