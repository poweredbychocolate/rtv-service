package klientGUI.poziom2.raportyGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientDane;
import klientGUI.KlientPracownik;
import raporty.GeneratorPDF;
import raporty.Raport;
import raporty.RaportBulider;
import zlecenia.Zlecenie;
import zlecenia.czynnosci.Czynnosci;

public class RaportyApp extends JFrame {

	private static final long serialVersionUID = 1L;
	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private static RaportyApp mainFrame;
	private Dimension size = new Dimension(900, 700);
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	private RaportBulider budowniczyRaportu;
	private JPanel panelPrzyciskow, panelZlecenia;
	private JButton dalejButton, wsteczButton;
	private Integer pozycja = 1, zlecenieId = 0;
	private Klient klient;
	private RaportCzynnosci panelCzynnosci;
	private RaportPodsumownie panelPodsumowania;
	private Boolean wybranoZlecenie = false, wybranoCzynnosci = false;
	private RaportKlient panelKlienta;
	private JLabel infoLabel;
	private JPanel gornyPanel, logoBox, infoBox;
	private GeneratorPDF generatorPDF;

	public RaportyApp(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.pracownik = pracownik;
		this.uzytkownik = uzytkownik;
		this.budowniczyRaportu = new RaportBulider();
		this.budowniczyRaportu.setPracownik(pracownik);
		KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	private void initUI() {
		mainFrame = this;
		this.setLayout(new BorderLayout());
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		this.setMaximumSize(size);
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(wa);
		this.setVisible(true);
		this.setLayout(new BorderLayout());

		initGornyPanel();
		initSrodkowyPanel();
		initPanelPrzyciskow();
		///
		mainFrame.pack();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new RaportyApp(u, p));
	}

	private void initGornyPanel() {
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
	}

	private void initSrodkowyPanel() {
		panelKlienta = new RaportKlient(mainFrame);
		this.add(panelKlienta, BorderLayout.CENTER);
	}

	private void initPanelPrzyciskow() {
		panelPrzyciskow = new JPanel();
		panelPrzyciskow.setLayout(new BoxLayout(panelPrzyciskow, BoxLayout.X_AXIS));
		////
		wsteczButton = new JButton("<< Wstecz");
		dalejButton = new JButton("Dalej >>");
		////
		wsteczButton.addActionListener(wsteczAction);
		dalejButton.addActionListener(dalejAction);
		////
		panelPrzyciskow.add(Box.createHorizontalGlue());
		panelPrzyciskow.add(wsteczButton);
		panelPrzyciskow.add(Box.createHorizontalStrut(150));
		panelPrzyciskow.add(dalejButton);
		panelPrzyciskow.add(Box.createHorizontalGlue());
		mainFrame.add(panelPrzyciskow, BorderLayout.SOUTH);
	}

	public void zapiszKlienta(Klient wybranyKlient) {
		this.klient = wybranyKlient;
	}

	public void zapiszZlecenie(Zlecenie wybraneZlecenie) {
		this.budowniczyRaportu.setZlecenie(wybraneZlecenie);
		this.zlecenieId = wybraneZlecenie.getZlecenieId();
		this.wybranoZlecenie = true;
	}

	public void zapiszCzynnosci(Czynnosci czynnosci) {
		this.budowniczyRaportu.setCzynnosci(czynnosci);
		this.wybranoCzynnosci = true;
	}

	public void zapiszNaglowek(String naglowek) {
		this.budowniczyRaportu.setNaglowek(naglowek);
	}

	public void zapiszDate(String data) {
		this.budowniczyRaportu.setData(data);
	}

	public Raport getRaport() {
		return this.budowniczyRaportu.getRaport();
	}

	private WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			new KlientPracownik(uzytkownik, pracownik);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};
	private ActionListener dalejAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (pozycja == 1 && klient != null) {
				mainFrame.remove(panelKlienta);
				panelZlecenia = new RaportyZlecenie(mainFrame, klient);
				mainFrame.add(panelZlecenia);
				mainFrame.validate();
				mainFrame.repaint();
				pozycja++;
			} else if (pozycja == 2 && wybranoZlecenie) {
				mainFrame.remove(panelZlecenia);
				panelCzynnosci = new RaportCzynnosci(mainFrame, pracownik, zlecenieId);
				mainFrame.add(panelCzynnosci);
				mainFrame.validate();
				mainFrame.repaint();
				pozycja++;
			} else if (pozycja == 3) {
				panelCzynnosci.zapiszCzynnosci();
				if (wybranoCzynnosci) {
					mainFrame.remove(panelCzynnosci);
					panelPodsumowania = new RaportPodsumownie(mainFrame);
					mainFrame.add(panelPodsumowania);
					dalejButton.setText("Zapisz");
					mainFrame.validate();
					mainFrame.repaint();
					pozycja++;
				}
			} else if (pozycja == 4) {
				generatorPDF = new GeneratorPDF(panelPodsumowania.generujRaport());
				// okienko z wyborem miejsca zapisu pliku
				RaportyZapis rz = new RaportyZapis(generatorPDF);
				if(rz.zapiszLokalizacje())
				{
				generatorPDF.budujPDF();
				if (generatorPDF.zapiszPDF()) {
					JOptionPane.showMessageDialog(mainFrame, "Raport zapisany");
					mainFrame.remove(panelPodsumowania);
					panelKlienta = new RaportKlient(mainFrame);
					mainFrame.add(panelKlienta);
					dalejButton.setText("Dalej >>");
					mainFrame.validate();
					mainFrame.repaint();
					pozycja=1;
				}	
				}
				rz.zamknij();
			}
		}
	};
	private ActionListener wsteczAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (pozycja == 2) {
				mainFrame.remove(panelZlecenia);
				panelKlienta = new RaportKlient(mainFrame);
				mainFrame.add(panelKlienta);
				mainFrame.validate();
				mainFrame.repaint();
				pozycja--;
			}
			if (pozycja == 3) {
				mainFrame.remove(panelCzynnosci);
				panelZlecenia = new RaportyZlecenie(mainFrame, klient);
				mainFrame.add(panelZlecenia);
				mainFrame.validate();
				mainFrame.repaint();
				pozycja--;
			}
			if (pozycja == 4) {
				mainFrame.remove(panelPodsumowania);
				panelCzynnosci = new RaportCzynnosci(mainFrame, pracownik, zlecenieId);
				mainFrame.add(panelCzynnosci);
				dalejButton.setText("Dalej >>");
				mainFrame.validate();
				mainFrame.repaint();
				pozycja--;
			}
		}
	};
}
