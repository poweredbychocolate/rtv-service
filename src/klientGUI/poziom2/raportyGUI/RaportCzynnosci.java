package klientGUI.poziom2.raportyGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import Uzytkownicy.Pracownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.czynnosci.Czynnosc;
import zlecenia.czynnosci.Czynnosci;
import zlecenia.czynnosci.Diagnoza;
import zlecenia.czynnosci.Naprawa;

class RaportCzynnosci extends JPanel {
	private static final long serialVersionUID = 1L;
	private final Integer obcinanieOpisu = 1000;
	private final Integer dlugoscEtykiety = 60;
	private RaportyApp raportyPanel;
	private GniazdoKlienta gniazdo;
	private CzynnosciPanel panelCzynnosci;
	private Integer zlecenieId;
	private Czynnosci noweCzynnosci;
	private Czynnosci wczytaneCzynnosci = null;
	private Czynnosci wybraneCzynnosci;

RaportCzynnosci(RaportyApp raportyPanel,Pracownik pracownik,Integer zlecenieId) {
		this.raportyPanel=raportyPanel;
		this.zlecenieId=zlecenieId;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.panelCzynnosci = new CzynnosciPanel();
		this.add(panelCzynnosci);
	}

	
	 void zapiszCzynnosci() {
		panelCzynnosci.zapiszNoweCzynnosci(zlecenieId);
		wybraneCzynnosci =new Czynnosci();
		for(int i=0;i<wczytaneCzynnosci.iloscCzynosci();i++)
			wybraneCzynnosci.dodajCzynnosc(wczytaneCzynnosci.getCzynnosc(i));
		for(int i=0;i<noweCzynnosci.iloscCzynosci();i++)
			wybraneCzynnosci.dodajCzynnosc(noweCzynnosci.getCzynnosc(i));
		raportyPanel.zapiszCzynnosci(wybraneCzynnosci);
	}
	


	private class CzynnosciPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private DodajPanel panelDodawania;
		private JPanel panel;
		private JScrollPane scrollPanel;
		private Dimension rozmiar = new Dimension(800, 500);

		CzynnosciPanel() {
			noweCzynnosci = new Czynnosci();
				this.panelDodawania = new DodajPanel();
			this.panel = new JPanel();
			this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));
			panel.add(Box.createVerticalStrut(10));
			wczytaj();
				panel.add(panelDodawania);
			scrollPanel = new JScrollPane(this.panel);
			scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPanel.setPreferredSize(rozmiar);
			scrollPanel.setMinimumSize(rozmiar);
			scrollPanel.setBorder(BorderFactory.createEmptyBorder());
			this.add(scrollPanel, BorderLayout.CENTER);
		}

		 void wczytaj() {
			wczytaneCzynnosci = gniazdo.wczytajCzynnosci(zlecenieId);
			if (wczytaneCzynnosci != null) {
				for (int i = 0; i < wczytaneCzynnosci.iloscCzynosci(); i++)
					panel.add(new CzynnoscPanel(wczytaneCzynnosci.getCzynnosc(i)));
				panel.add(Box.createVerticalStrut(5));
			}
		}

		void dodaj(CzynnoscPanel panel) {
			this.panel.add(panel);
			this.panel.add(Box.createVerticalStrut(5));
			noweCzynnosci.dodajCzynnosc(panel.getCzynnosc());
			this.validate();
			this.repaint();
		}

		 void usunPanelDodawania() {
				this.panel.remove(panelDodawania);

		}

		 void dodajPanelDodawania() {
				this.panel.add(panelDodawania);
				this.validate();
				this.repaint();
			}


		 Boolean zapiszNoweCzynnosci(Integer zlecenieId) {
			Boolean b = false;
			b = gniazdo.zapiszCzynosci(noweCzynnosci, zlecenieId);
			return b;
		}
}


	
	private class DodajPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Czynnosc cz;

		private JTextField kosztField;
		private JTextArea opisField;
		private Dimension size = new Dimension(400, 100);

		DodajPanel() {
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.add(Box.createVerticalStrut(5));
			panelPrzyciskow();
			panelOpisu();
			this.add(Box.createVerticalStrut(5));
		}

		void panelPrzyciskow() {
			JPanel przyciski = new JPanel();
			przyciski.setLayout(new BoxLayout(przyciski, BoxLayout.X_AXIS));
			JButton naprawaButton = new JButton("Dodaj Naprawa");
			JButton diagnozaButton = new JButton("Dodaj Diagnoza");
			naprawaButton.addActionListener(naprawaAl);
			diagnozaButton.addActionListener(diagnozaAl);
			kosztField = new JTextField();

			przyciski.add(Box.createHorizontalGlue());
			przyciski.add(Box.createHorizontalStrut(10));
			przyciski.add(diagnozaButton);
			przyciski.add(Box.createHorizontalStrut(5));
			przyciski.add(new JLabel("Koszt "));
			przyciski.add(Box.createHorizontalStrut(5));
			przyciski.add(kosztField);
			przyciski.add(Box.createHorizontalStrut(5));
			przyciski.add(naprawaButton);
			przyciski.add(Box.createHorizontalStrut(10));
			przyciski.add(Box.createHorizontalGlue());
			this.add(przyciski);

		}

		void panelOpisu() {
			JPanel opis = new JPanel();
			opis.setLayout(new BoxLayout(opis, BoxLayout.X_AXIS));
			opisField = new JTextArea("Opis czynnosci", 5, 50);
			opisField.setLineWrap(true);
			opisField.setSize(size);
			opis.add(Box.createHorizontalGlue());
			opis.add(Box.createHorizontalStrut(10));
			opis.add(opisField);
			opis.add(Box.createHorizontalStrut(10));
			opis.add(Box.createHorizontalGlue());
			this.add(opis);
		}

		ActionListener naprawaAl = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cz = new Naprawa();
				String tmp = opisField.getText();
				if (tmp.length() <= obcinanieOpisu)
					cz.setOpis(tmp);
				else
					cz.setOpis(tmp.substring(0, obcinanieOpisu));
				tmp = kosztField.getText();
				if (tmp.length() > 5)
					tmp = tmp.substring(0, 5);
				try {
					cz.setKoszt(Integer.parseInt(tmp));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					cz.setKoszt(0);
				}
				panelCzynnosci.usunPanelDodawania();
				panelCzynnosci.dodaj(new CzynnoscPanel(cz));
				panelCzynnosci.dodajPanelDodawania();
				opisField.setText("");
				kosztField.setText("");
			}
		};
		ActionListener diagnozaAl = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cz = new Diagnoza();
				String tmp = opisField.getText();
				if (tmp.length() <= obcinanieOpisu)
					cz.setOpis(tmp);
				else
					cz.setOpis(tmp.substring(0, obcinanieOpisu));
				tmp = kosztField.getText();
				if (tmp.length() > 5)
					tmp = tmp.substring(0, 5);
				try {
					cz.setKoszt(Integer.parseInt(tmp));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					cz.setKoszt(0);
				}
				panelCzynnosci.usunPanelDodawania();
				panelCzynnosci.dodaj(new CzynnoscPanel(cz));
				panelCzynnosci.dodajPanelDodawania();
				opisField.setText("");
				kosztField.setText("");
			}
		};

	}

	private class CzynnoscPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Czynnosc czynnosc;

		CzynnoscPanel(Czynnosc czynnosc) {
			this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			this.czynnosc = czynnosc;
			JLabel tmpLabel =new JLabel(czynnosc.getClass().getSimpleName());
			tmpLabel.setForeground(Color.BLUE);
			this.add(tmpLabel);
			this.add(Box.createHorizontalStrut(10));
			String tmp = czynnosc.getOpis();
			if (tmp.length() > dlugoscEtykiety)
				tmp = tmp.substring(0, dlugoscEtykiety + 1) + "...";

			this.add(new JLabel(tmp));
			this.add(Box.createHorizontalStrut(5));
			//this.add(Box.createHorizontalGlue());
			if (czynnosc instanceof Naprawa)
				tmpLabel =new JLabel(" Koszt : ");
			else
				tmpLabel = new JLabel("Szacowany koszt : ");
			tmpLabel.setForeground(Color.BLUE);
			this.add(tmpLabel);
			this.add(Box.createHorizontalStrut(5));
			this.add(new JLabel(czynnosc.getKoszt().toString()));
			this.add(Box.createHorizontalStrut(5));
			this.add(Box.createHorizontalGlue());
		}

		 Czynnosc getCzynnosc() {
			return this.czynnosc;
		}
	
}
}
