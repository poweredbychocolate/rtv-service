package klientGUI.poziom2.raportyGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;

class RaportyZlecenie extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RaportyApp mainFrame;
	private GniazdoKlienta gniazdo = null;
	private KartaZlecenia wybraneZlecenie = null;
	//private Dimension rozmiarScroll = new Dimension(300, 600);
	private Klient klient;
	/////
	/////
	JPanel topBar, zleceniePanel;
	JScrollPane zlecenieScrollPanel;
	JLabel topLabel;

	 RaportyZlecenie(RaportyApp mainFrame, Klient klient) {
		this.mainFrame = mainFrame;
		this.klient = klient;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.setLayout(new BorderLayout());
		initTopBar();
		initzlecenieScrollPanel();
	}

	private void initTopBar() {
		topBar = new JPanel();
		topLabel = new JLabel("Panel wyboru klienta ");
		topBar.setLayout(new BoxLayout(topBar, BoxLayout.X_AXIS));

		topBar.add(Box.createHorizontalGlue());
		topBar.add(topLabel);
		topBar.add(Box.createHorizontalGlue());
		this.add(topBar, BorderLayout.NORTH);
	}

	private void initzlecenieScrollPanel() {
		zlecenieScrollPanel = new JScrollPane();
		zlecenieScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		zlecenieScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		zleceniePanel = new JPanel();
		zleceniePanel.setLayout(new GridLayout(0, 6, 4, 4));
		wczytajZlecenia(klient);
		zlecenieScrollPanel.getViewport().add(zleceniePanel);
	//	zlecenieScrollPanel.setMaximumSize(rozmiarScroll);
		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		tmpPanel.add(zlecenieScrollPanel);
		tmpPanel.add(Box.createVerticalGlue());
		this.add(tmpPanel, BorderLayout.CENTER);
	}

	private void wczytajZlecenia(Klient klient) {
		this.removeAll();
		ArrayList<Zlecenie> zlecenia=gniazdo.listaZlecenKlienta(klient);
		Iterator<Zlecenie> zleceniaIterator = zlecenia.iterator();
		while (zleceniaIterator.hasNext()) {
			zleceniePanel.add(new KartaZlecenia(zleceniaIterator.next()));
		}
	}

	class KartaZlecenia extends JPanel {
		private JButton wybierzButton = new JButton("Wybierz");
		private Color kolor = Color.YELLOW.brighter();
		private Color kolorWybranego = Color.GREEN.brighter();
		private JLabel idLabel, markaLabel, modelLabel;
		private Dimension buttonSize = new Dimension(60, 30);
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Zlecenie z = null;
		private KartaZlecenia kz = null;

		public KartaZlecenia(Zlecenie zlecenie) {
			this.z = zlecenie;
			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			this.kz = this;
			initElementy();
			initPanel();

		}

		private void initElementy() {
			idLabel = new JLabel(new String("Identyfikator : " + this.z.getZlecenieId()));
			markaLabel = new JLabel(new String("Urzadzenie : " + this.z.getUrzadzenie().getMarka()));
			modelLabel = new JLabel(new String("Model : " + this.z.getUrzadzenie().getModel()));
			////
			wybierzButton.addActionListener(wybierzListener);
			KlientDane.ustawRozmiarPrzyciskow(wybierzButton, buttonSize);
		}

		private void initPanel() {
			this.add(idLabel);
			this.add(markaLabel);
			this.add(modelLabel);
			this.add(wybierzButton);
		}

		void reset() {
			this.setBackground(kolor);
		}

		ActionListener wybierzListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				kz.setBackground(kolorWybranego);
				if (wybraneZlecenie != null)
					wybraneZlecenie.reset();
				wybraneZlecenie = kz;
				mainFrame.zapiszZlecenie(z);
			}
		};

	}

}