package klientGUI.poziom2.raportyGUI;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import raporty.GeneratorPDF;

class RaportyZapis extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GeneratorPDF generator;
	private JFileChooser zapis;

	RaportyZapis(GeneratorPDF generator) {
		this.generator = generator;
	}

	Boolean zapiszLokalizacje() {
		zapis = new JFileChooser();
		zapis.setDialogTitle("Zapisz raport");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF", "pdf");
		zapis.setFileFilter(filter);
		zapis.setCurrentDirectory(
				new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));

		if (zapis.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {

			generator.setSciezka(zapis.getCurrentDirectory().toString());
			generator.setNazwa(zapis.getSelectedFile().getName());
			return true;
		}
		return false;
	}

	void zamknij() {
		this.dispose();
	}

}
