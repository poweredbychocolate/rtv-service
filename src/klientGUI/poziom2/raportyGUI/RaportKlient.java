package klientGUI.poziom2.raportyGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;

class RaportKlient extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RaportyApp mainFrame;
	private GniazdoKlienta gniazdo = null;
	private KartaKlienta wybranaKarta = null;
	/////
	JPanel topBar, klientPanel;
	JScrollPane klientScrollPanel;
	JLabel topLabel;

	RaportKlient(RaportyApp mainFrame) {
		this.mainFrame = mainFrame;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.setLayout(new BorderLayout());
		initTopBar();
		initKlientScrollPanel();
	}

	private void initTopBar() {
		topBar = new JPanel();
		topLabel = new JLabel("Panel wyboru klienta ");
		topBar.setLayout(new BoxLayout(topBar, BoxLayout.X_AXIS));

		topBar.add(Box.createHorizontalGlue());
		topBar.add(topLabel);
		topBar.add(Box.createHorizontalGlue());
		this.add(topBar, BorderLayout.NORTH);
	}

	private void initKlientScrollPanel() {
		klientScrollPanel = new JScrollPane();
		klientScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		klientScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		klientPanel = new JPanel();
		klientPanel.setLayout(new GridLayout(0, 6, 4, 4));
		wczytajKlientow();
		klientScrollPanel.getViewport().add(klientPanel);
		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		tmpPanel.add(klientScrollPanel);
		tmpPanel.add(Box.createVerticalGlue());
		this.add(tmpPanel, BorderLayout.CENTER);
	}

	private void wczytajKlientow() {
		ArrayList<Klient> klienci = null;
		klienci = gniazdo.listaKlientow();
		Iterator<Klient> klientIterator = klienci.iterator();
		while (klientIterator.hasNext()) {
			klientPanel.add(new KartaKlienta(klientIterator.next()));
		}

	}

	class KartaKlienta extends JPanel {
		private JButton wybierzButton = new JButton("Wybierz");
		private Color kolor = Color.YELLOW.brighter();
		private Color kolorWybranego = Color.GREEN.brighter();
		private JLabel imieLabel, nazwiskoLabel, telefonLabel;
		private Dimension buttonSize = new Dimension(60, 30);
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Klient k = null;
		private KartaKlienta kk = null;
		public KartaKlienta(Klient klient) {
			this.k = klient;
			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			this.kk = this;
			initElementy();
			initPanel();
		}

		private void initElementy() {
			imieLabel = new JLabel(k.getImie());
			nazwiskoLabel = new JLabel(k.getNazwisko());
			telefonLabel = new JLabel(k.getTelefon());
			////
			wybierzButton.addActionListener(wybierzListener);
			KlientDane.ustawRozmiarPrzyciskow(wybierzButton, buttonSize);
		}

		private void initPanel() {
			this.add(imieLabel);
			this.add(nazwiskoLabel);
			this.add(telefonLabel);
			this.add(wybierzButton);
		}

		void reset() {
			this.setBackground(kolor);
		}

		ActionListener wybierzListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				kk.setBackground(kolorWybranego);
				if (wybranaKarta == null) {
					wybranaKarta = kk;
					mainFrame.zapiszKlienta(k);
				} else {
					wybranaKarta.reset();
					wybranaKarta = kk;
					mainFrame.zapiszKlienta(k);
				}
			}
		};

	}
}