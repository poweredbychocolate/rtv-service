package klientGUI.poziom2;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import klientGUI.KlientDane;
import klientGUI.KlientPracownik;
import klientGUI.poziom2.raportyGUI.RaportyApp;
import klientGUI.poziom2.zarzadzanieKlientami.KlientApp;
import klientGUI.poziom2.zarzadzanieZleceniami.ZleceniaApp;

public class MenuPoziom2 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/////
	private JButton klienciButton,zleceniaButton,raportyButton;
	/////

	public MenuPoziom2() {
		
	initPanel();
}
	private void  initButton()
	{
		this.klienciButton =new JButton("Klienci");
		this.zleceniaButton=new JButton("Zlecenia");
		this.raportyButton=new JButton("Raporty");
		
		this.klienciButton.addActionListener(klienciActionListener);
		this.zleceniaButton.addActionListener(zleceniaActionListener);
		this.raportyButton.addActionListener(raportyActionListener);
	}
	private  void addButton()
	{
		this.add(Box.createHorizontalGlue());
		this.add(Box.createHorizontalStrut(5));
		this.add(klienciButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(zleceniaButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(raportyButton);
		this.add(Box.createHorizontalStrut(5));
		this.add(Box.createHorizontalGlue());
	}
	
	private void initPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		initButton();
		ustawRozmiar();
		addButton();
		
	}

	private void ustawRozmiarPrzyciskow(JButton przycisk, Dimension buttonSize) {
		przycisk.setSize(buttonSize);
		przycisk.setPreferredSize(buttonSize);
		przycisk.setMinimumSize(buttonSize);
		przycisk.setMaximumSize(buttonSize);
	}
	private void ustawRozmiar()
	{
		Dimension size = new Dimension(130, 30);
		this.ustawRozmiarPrzyciskow(klienciButton, size);
		this.ustawRozmiarPrzyciskow(raportyButton, size);
		this.ustawRozmiarPrzyciskow(zleceniaButton, size);
	}
	
	private ActionListener klienciActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new KlientApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	};
	private ActionListener zleceniaActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new ZleceniaApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	};
private ActionListener raportyActionListener =new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			KlientPracownik.zamkijOkno();
			new RaportyApp(KlientDane.getUzytkownik(), KlientDane.getPracownik());
			
		}
	};
}