package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Uzytkownicy.Pracownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;
import zlecenia.statusy.Przyjete;

class PodsumowniePanel  extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	////
	private Zlecenie zlecenie =null;
	private Pracownik pracownik;
	private GniazdoKlienta gniazdo;
	private JPanel panel,pionowyPanel,poziomyPanel;
	////
	private JLabel imie,nazwisko,gwaracja,model,marka,status,data;

	PodsumowniePanel(Pracownik pracownik,Zlecenie zlecenie) {
		this.zlecenie = zlecenie;
		this.pracownik = pracownik;
		uzupelnijZlecenie();		
		initPanel();
	}
	private void uzupelnijZlecenie(){
		Przyjete status = new Przyjete();
		status.setPracownik(pracownik);
		status.setDataRozpoczecia(KlientDane.getBiezacyCzas());
		this.zlecenie.setStatus(status);
	}
	private void initPanel()
	{
		this.setLayout(new BorderLayout());
		initElemanty();
		skladanie();
	}
	private void initElemanty(){
		imie = new JLabel(zlecenie.getKlienta().getImie());
		nazwisko = new JLabel(zlecenie.getKlienta().getNazwisko());
		marka = new JLabel(zlecenie.getUrzadzenie().getMarka());
		model = new JLabel(zlecenie.getUrzadzenie().getModel());
		if (zlecenie.getGwaracjaProducenta())
		{
			gwaracja = new JLabel("Gwarancja Producenta");
		}
		else{
			gwaracja = new JLabel("Naprawa Pogwarancyjna");
		}
		if (zlecenie.getStatus() instanceof Przyjete )
		{
			status= new JLabel("Przyjete do realizacji");
			data = new JLabel(zlecenie.getStatus().getDataRozpoczecia());
		}
	}
	private void skladanie()
	{	
		setLabelSize();
		panel = new JPanel();
		panel.setLayout(new GridLayout(0, 3,1,1));
		panel.add(new JLabel("Klient :"));
		panel.add(imie);
		panel.add(nazwisko);
		panel.add(new JLabel("Urzadzenie :"));
		panel.add(marka);
		panel.add(model);
		panel.add(data);
		panel.add(status);
		panel.add(gwaracja);
		pionowyPanel =new JPanel();
		pionowyPanel.setLayout(new BoxLayout(pionowyPanel,BoxLayout.Y_AXIS));
		pionowyPanel.add(Box.createVerticalGlue());
		pionowyPanel.add(panel);
		pionowyPanel.add(Box.createVerticalStrut(4));
		poziomyPanel =new JPanel();
		poziomyPanel.setLayout(new BoxLayout(poziomyPanel, BoxLayout.X_AXIS));
		poziomyPanel.add(Box.createHorizontalGlue());
		poziomyPanel.add(pionowyPanel);
		poziomyPanel.add(Box.createHorizontalGlue());
		this.add(poziomyPanel,BorderLayout.CENTER);
	}
	void save()
	{
		gniazdo = KlientDane.getGniazdoKlienta();
		gniazdo.dodajStatus(zlecenie.getStatus());
		gniazdo.dodajUrzadzenie(zlecenie.getUrzadzenie());
		gniazdo.dodajZlecenie(zlecenie);
	}
private void setLabelSize(){
	Dimension size =new Dimension(200, 30);
	imie.setPreferredSize(size);
	nazwisko.setPreferredSize(size);
	gwaracja.setPreferredSize(size);
	model.setPreferredSize(size);
	marka.setPreferredSize(size);
	status.setPreferredSize(size);
	data.setPreferredSize(size);
	//imie,nazwisko,gwaracja,model,marka,status,data

}

}
