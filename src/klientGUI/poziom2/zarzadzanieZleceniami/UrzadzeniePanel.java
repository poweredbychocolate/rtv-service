package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import zlecenia.Urzadzenie;
import zlecenia.Zlecenie;

class UrzadzeniePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	////
	private JLabel markaLabel,modelLabel;
	private JTextField markaField,modelField;
	private JCheckBox gwaracjaBox;
	private JPanel pionowyPanel,poziomyPanel,panel;	
	private Zlecenie zlecenie;
	
	UrzadzeniePanel(Zlecenie zlecenie) {
		initPanel();
		this.zlecenie=zlecenie;
	}
	private void initPanel(){
		this.setLayout(new BorderLayout());
		initElemanty();
		setSize();
		panel =new JPanel();
		panel.setLayout(new GridLayout(0, 2 ,1, 1));
		panel.add( markaLabel);
		panel.add( markaField);
		panel.add(modelLabel );
		panel.add( modelField);
		panel.add(new JLabel(""));
		panel.add(gwaracjaBox);
		
		pionowyPanel = new JPanel();
		pionowyPanel.setLayout(new BoxLayout(pionowyPanel, BoxLayout.Y_AXIS));
		pionowyPanel.add(Box.createVerticalGlue());
		pionowyPanel.add(Box.createVerticalGlue());
		pionowyPanel.add(panel);
		pionowyPanel.add(Box.createVerticalStrut(2));
		
		poziomyPanel = new JPanel();
		poziomyPanel.setLayout(new BoxLayout(poziomyPanel, BoxLayout.X_AXIS));
		poziomyPanel.add(Box.createHorizontalGlue());
		poziomyPanel.add(pionowyPanel);
		poziomyPanel.add(Box.createHorizontalGlue());
		
		this.add(poziomyPanel,BorderLayout.CENTER);
		
		
	}
	private void initElemanty()
	{
		markaLabel = new JLabel("Marka");
		modelLabel = new JLabel("Model");
		markaField = new JTextField(100);
		modelField = new JTextField(100);
		gwaracjaBox = new JCheckBox("Gwarancja Producenta");

	}
	void save()
	{
		Urzadzenie u = new Urzadzenie();
		u.setMarka(markaField.getText());
		u.setModel(modelField.getText());
		zlecenie.setUrzadzenie(u);
		zlecenie.setGwaracjaProducenta(gwaracjaBox.isSelected());
	}
	Boolean isSet()
	{
		Boolean b=false;
		if(markaField.getText().isEmpty()||modelField.getText().isEmpty())
			b=false;
		else b=true;
		return b;
		
	}
	private void setSize(){
		Dimension size =new Dimension(100, 20);
		markaField.setPreferredSize(size);
		modelField.setPreferredSize(size);
		markaField.setMaximumSize(size);
		modelField.setMaximumSize(size);
		markaField.setMinimumSize(size);
		modelField.setMinimumSize(size);
		}
}
