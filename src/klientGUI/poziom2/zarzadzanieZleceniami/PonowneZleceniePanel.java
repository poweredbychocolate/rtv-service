package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import Uzytkownicy.Pracownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.KopiaZlecenia;
import zlecenia.Zlecenie;

class PonowneZleceniePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Pracownik pracownik;
	private Zlecenie stareZlecenie, noweZlecenie;
	private KartaKlienta wybranaKarta;
	private KartaZlecenia wybraneZlecenie;
	private GniazdoKlienta gniazdo;
	private KopiaZlecenia kopiaZlecenia;
	///
	private JScrollPane klientScrollPanel, zleceniaScrollPanel;
	private JPanel klientPanel, zleceniaPanel, przyciskPanel, srodkowyPanel;
	private PodsumowniePanel panelPodsumowania;
	private JButton zatwierdzButton;
	private Dimension rozmiarScroll = new Dimension(300, 600);

	PonowneZleceniePanel(Pracownik pracownik) {
		this.pracownik = pracownik;
		this.stareZlecenie = new Zlecenie();
		this.gniazdo = KlientDane.getGniazdoKlienta();
		this.kopiaZlecenia = new KopiaZlecenia();
		///
		this.setLayout(new BorderLayout());
		initPanel();

	}

	private void initPanel() {
		srodkowyPanel = new JPanel();
		srodkowyPanel.setLayout(new GridLayout(0, 1,5,5));
		initKlientPanel();
		initZleceniePanel();
		this.add(srodkowyPanel, BorderLayout.CENTER);
		initPrzycikiPanel();
	}

	private void initKlientPanel() {
		klientPanel = new JPanel();
		klientPanel.setLayout(new GridLayout(2, 0, 5, 5));
		wczytajKlientow();
		////
		klientScrollPanel = new JScrollPane();
		klientScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		klientScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		klientScrollPanel.getViewport().add(klientPanel);
		klientScrollPanel.setPreferredSize(rozmiarScroll);
		////
		srodkowyPanel.add(klientScrollPanel);
	}

	private void initZleceniePanel() {
		zleceniaPanel = new JPanel();
		zleceniaPanel.setLayout(new GridLayout(1, 0, 5, 5));
		////
		zleceniaScrollPanel = new JScrollPane();
		zleceniaScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		zleceniaScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		zleceniaScrollPanel.getViewport().add(zleceniaPanel);
		zleceniaScrollPanel.setPreferredSize(rozmiarScroll);
		////
		srodkowyPanel.add(zleceniaScrollPanel);
	}

	private void initPodsumowaniePanel() {
		if (panelPodsumowania != null)
			srodkowyPanel.remove(panelPodsumowania);
		else
			zatwierdzButton.addActionListener(zatwierdzAL);
		panelPodsumowania = new PodsumowniePanel(pracownik, noweZlecenie);
		srodkowyPanel.add(panelPodsumowania);
		this.validate();
		this.repaint();

	}

	private void initPrzycikiPanel() {
		// this.resetButton = new JButton("Reset");
		this.zatwierdzButton = new JButton("Zatwierdz");
		////
		this.przyciskPanel = new JPanel();
		przyciskPanel.setLayout(new BoxLayout(przyciskPanel, BoxLayout.X_AXIS));
		przyciskPanel.add(Box.createHorizontalGlue());
		przyciskPanel.add(zatwierdzButton);
		przyciskPanel.add(Box.createHorizontalStrut(5));
		// przyciskPanel.add(resetButton);
		przyciskPanel.add(Box.createHorizontalStrut(5));
		this.add(przyciskPanel, BorderLayout.SOUTH);
	}

	private void wczytajKlientow() {
		ArrayList<Klient> klienci = null;
		klienci = gniazdo.listaKlientow();
		Iterator<Klient> klientIterator = klienci.iterator();
		while (klientIterator.hasNext()) {
			klientPanel.add(new KartaKlienta(klientIterator.next()));
		}

	}

	private void wczytajZlecenia(Klient klient) {
		zleceniaPanel.removeAll();
		kopiaZlecenia.wczytajZlecenia(klient);
		ArrayList<Zlecenie> zlecenia = kopiaZlecenia.pobierzListe();
		Iterator<Zlecenie> zleceniaIterator = zlecenia.iterator();
		while (zleceniaIterator.hasNext()) {
			zleceniaPanel.add(new KartaZlecenia(zleceniaIterator.next()));
		}
		this.validate();
		this.repaint();

	}

	ActionListener zatwierdzAL = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			panelPodsumowania.save();
			JOptionPane.showMessageDialog(null, "Zlecenie zapisane");
		}
	};

	class KartaKlienta extends JPanel {
		private JButton wybierzButton = new JButton("Wybierz");
		private Color kolor = Color.YELLOW.brighter();
		private Color kolorWybranego = Color.GREEN.brighter();
		private JLabel imieLabel, nazwiskoLabel, telefonLabel;
		private Dimension buttonSize = new Dimension(60, 30);
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Klient k = null;
		private KartaKlienta kk = null;

		public KartaKlienta(Klient klient) {
			this.k = klient;
			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			this.kk = this;
			initElementy();
			initPanel();

		}

		private void initElementy() {
			imieLabel = new JLabel(k.getImie());
			nazwiskoLabel = new JLabel(k.getNazwisko());
			telefonLabel = new JLabel(k.getTelefon());
			////
			wybierzButton.addActionListener(wybierzListener);
			KlientDane.ustawRozmiarPrzyciskow(wybierzButton, buttonSize);
		}

		private void initPanel() {
			this.add(imieLabel);
			this.add(nazwiskoLabel);
			this.add(telefonLabel);
			this.add(wybierzButton);
		}

		void reset() {
			this.setBackground(kolor);
		}

		ActionListener wybierzListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				kk.setBackground(kolorWybranego);
				if (wybranaKarta == null) {
					wybranaKarta = kk;
					stareZlecenie.setKlient(k);
					wczytajZlecenia(stareZlecenie.getKlienta());
				} else {
					wybranaKarta.reset();
					wybranaKarta = kk;
					stareZlecenie.setKlient(k);
					wczytajZlecenia(stareZlecenie.getKlienta());
				}
			}
		};

	}

	class KartaZlecenia extends JPanel {
		private JButton wybierzButton = new JButton("Wybierz");
		private JCheckBox gwarancjaCheck = new JCheckBox("Gwarancja", false);
		private Color kolor = Color.YELLOW.brighter();
		private Color kolorWybranego = Color.GREEN.brighter();
		private JLabel idLabel, markaLabel, modelLabel;
		private Dimension buttonSize = new Dimension(60, 30);
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Zlecenie z = null;
		private KartaZlecenia kz = null;

		KartaZlecenia(Zlecenie zlecenie) {
			this.z = zlecenie;
			this.setLayout(new GridLayout(0, 1));
			this.setBackground(kolor);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			this.kz = this;
			initElementy();
			initPanel();

		}

		private void initElementy() {
			idLabel = new JLabel(new String("Identyfikator : " + this.z.getZlecenieId()));
			markaLabel = new JLabel(new String("Urzadzenie : " + this.z.getUrzadzenie().getMarka()));
			modelLabel = new JLabel(new String("Model : " + this.z.getUrzadzenie().getModel()));
			////
			wybierzButton.addActionListener(wybierzListener);
			KlientDane.ustawRozmiarPrzyciskow(wybierzButton, buttonSize);
		}

		private void initPanel() {
			this.add(idLabel);
			this.add(markaLabel);
			this.add(modelLabel);
			this.add(gwarancjaCheck);
			this.add(wybierzButton);
		}

		void reset() {
			this.setBackground(kolor);
		}

		ActionListener wybierzListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				kz.setBackground(kolorWybranego);
				if (wybraneZlecenie != null)
					wybraneZlecenie.reset();
				wybraneZlecenie = kz;
				stareZlecenie = z;
				try {
					noweZlecenie = kopiaZlecenia.kopiujZlecenie(z.getZlecenieId());
				} catch (CloneNotSupportedException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				noweZlecenie.setGwaracjaProducenta(gwarancjaCheck.isSelected());
				initPodsumowaniePanel();
			}
		};

	}

}
