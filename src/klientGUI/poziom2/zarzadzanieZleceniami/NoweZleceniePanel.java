package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Uzytkownicy.Pracownik;
import zlecenia.Zlecenie;

class NoweZleceniePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	///
	private JPanel buttonBar, kreatorPanel;
	private JButton wsteczButton, dalejButton;
	private KlientPanel panelWyboruKlienta;
	private UrzadzeniePanel panelWyboruUrzadzenia;
	private PodsumowniePanel panelPodsumoania;
	private Dimension rozmiarPanelu = new Dimension(300, 600);
	///
	private Pracownik pracownik = null;
	private Zlecenie zlecenie = null;
	private Integer pozycja = 1;

	///
	NoweZleceniePanel(Pracownik pracownik) {
		this.setLayout(new BorderLayout());
		this.pracownik = pracownik;
		this.zlecenie = new Zlecenie();
		initButtonBar();
		initWorkLayout();
	}

	private void initButtonBar() {
		buttonBar = new JPanel();
		buttonBar.setLayout(new BoxLayout(buttonBar, BoxLayout.X_AXIS));
		////
		wsteczButton = new JButton("<< Wstecz");
		dalejButton = new JButton("Dalej >>");
		////
		wsteczButton.addActionListener(wsteczAction);
		dalejButton.addActionListener(dalejAction);
		////
		buttonBar.add(Box.createHorizontalGlue());
		buttonBar.add(wsteczButton);
		buttonBar.add(Box.createHorizontalStrut(150));
		buttonBar.add(dalejButton);
		buttonBar.add(Box.createHorizontalGlue());
		this.add(buttonBar, BorderLayout.SOUTH);

	}

	private void initWorkLayout() {
		kreatorPanel = new JPanel();
		kreatorPanel.setLayout(new GridLayout(1, 0));
		panelWyboruKlienta = new KlientPanel(zlecenie);
		panelWyboruKlienta.setPreferredSize(rozmiarPanelu);
		panelWyboruUrzadzenia = new UrzadzeniePanel(zlecenie);
		kreatorPanel.add(panelWyboruKlienta);
		this.add(kreatorPanel, BorderLayout.CENTER);
	}

	private ActionListener wsteczAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (pozycja == 2) {
				kreatorPanel.remove(panelWyboruUrzadzenia);
				kreatorPanel.add(panelWyboruKlienta);
				kreatorPanel.validate();
				kreatorPanel.repaint();
				pozycja--;

			} else if (pozycja == 3) {
				kreatorPanel.remove(panelPodsumoania);
				dalejButton.setText("Dalej >>");
				kreatorPanel.add(panelWyboruUrzadzenia);
				kreatorPanel.validate();
				kreatorPanel.repaint();
				pozycja--;
			}
		}
	};

private	ActionListener dalejAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (pozycja == 1) {
				if (zlecenie.getKlienta() != null) {
					kreatorPanel.remove(panelWyboruKlienta);
					kreatorPanel.add(panelWyboruUrzadzenia);
					kreatorPanel.validate();
					kreatorPanel.repaint();
					pozycja++;
				}
			} else if (pozycja == 2) {
				if (panelWyboruUrzadzenia.isSet()) {
					panelWyboruUrzadzenia.save();
					kreatorPanel.remove(panelWyboruUrzadzenia);
					panelPodsumoania = new PodsumowniePanel(pracownik, zlecenie);
					kreatorPanel.add(panelPodsumoania);
					dalejButton.setText("Zakoncz");
					kreatorPanel.validate();
					kreatorPanel.repaint();
					pozycja++;
				}
			} else if (pozycja == 3) {
				///
				panelPodsumoania.save();
				JOptionPane.showMessageDialog(panelPodsumoania, "Dodano nowe zlecenie");
				kreatorPanel.remove(panelPodsumoania);
				zlecenie = new Zlecenie();
				panelWyboruKlienta = new KlientPanel(zlecenie);
				panelWyboruUrzadzenia = new UrzadzeniePanel(zlecenie);
				kreatorPanel.add(panelWyboruKlienta);
				dalejButton.setText("Dalej >>");
				kreatorPanel.validate();
				kreatorPanel.repaint();
				pozycja = 1;
			}
		}
	};

}
