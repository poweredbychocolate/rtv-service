package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Uzytkownicy.Klient;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;

class KlientPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Zlecenie zlecenie =null;
	private GniazdoKlienta gniazdo=null;
	private KartaKlienta wybranaKarta=null;
	/////
	JPanel topBar, klientPanel;
	JScrollPane klientScrollPanel;
	JLabel topLabel;

	KlientPanel(Zlecenie zlecenie) {
		this.zlecenie=zlecenie;
		this.gniazdo=KlientDane.getGniazdoKlienta();
		this.setLayout(new BorderLayout());
		initTopBar();
		initKlientScrollPanel();
	}

	private void initTopBar() {
		topBar = new JPanel();
		topLabel = new JLabel("Panel wyboru klienta ");
		topBar.setLayout(new BoxLayout(topBar, BoxLayout.X_AXIS));

		topBar.add(Box.createHorizontalGlue());
		topBar.add(topLabel);
		topBar.add(Box.createHorizontalGlue());
		this.add(topBar, BorderLayout.NORTH);
	}

	private void initKlientScrollPanel() {
		klientScrollPanel = new JScrollPane();
		klientScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		klientScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		klientPanel = new JPanel();
		klientPanel.setLayout(new GridLayout(0, 6, 4, 4));
		wczytajKlientow();
		klientScrollPanel.getViewport().add(klientPanel);
		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		tmpPanel.add(klientScrollPanel);
		tmpPanel.add(Box.createVerticalGlue());
		this.add(tmpPanel, BorderLayout.CENTER);
	}

	private void wczytajKlientow() {
		ArrayList<Klient> klienci=null;
		klienci= gniazdo.listaKlientow();
		Iterator<Klient> klientIterator = klienci.iterator();
		while(klientIterator.hasNext()){
			Klient k =klientIterator.next();
			klientPanel.add(new KartaKlienta(k,gniazdo.wczytajLiczbeZlecen(k)));
		}

	}


class KartaKlienta extends JPanel {
	private JButton wybierzButton = new JButton("Wybierz");
	private Color kolor = Color.YELLOW.brighter();
	private Color kolorWybranego = Color.GREEN.brighter();
	private JLabel imieLabel, nazwiskoLabel, telefonLabel,iloscLabel;
	private Dimension buttonSize = new Dimension(60, 30);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Klient k = null;
	private KartaKlienta kk=null;
	private Integer liczbaZlecen =0;

	KartaKlienta(Klient klient,Integer liczbaZlecen) {
		this.k = klient;
		this.liczbaZlecen=liczbaZlecen;
		this.setLayout(new GridLayout(0, 1));
		this.setBackground(kolor);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.kk =this;
		initElementy();
		initPanel();

	}

	private void initElementy() {
		imieLabel = new JLabel(k.getImie());
		nazwiskoLabel = new JLabel(k.getNazwisko());
		telefonLabel = new JLabel(k.getTelefon());
		iloscLabel = new JLabel(new String("Zlecenia : "+liczbaZlecen.toString()));
		////
		wybierzButton.addActionListener(wybierzListener);
		wybierzButton.setMinimumSize(buttonSize);
	}

	private void initPanel() {
		this.add(imieLabel);
		this.add(nazwiskoLabel);
		this.add(telefonLabel);
		this.add(iloscLabel);
		this.add(wybierzButton);
	}

	void reset()
	{
		this.setBackground(kolor);
	}
	ActionListener wybierzListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			kk.setBackground(kolorWybranego);
			if(wybranaKarta==null){
				wybranaKarta=kk;
				zlecenie.setKlient(k);				
			}
			else{
				wybranaKarta.reset();
				wybranaKarta=kk;
				zlecenie.setKlient(k);
			}
		}
	};

}}