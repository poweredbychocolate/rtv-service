package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.KlientPracownik;

public class ZleceniaApp extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private JFrame mainFrame;
	private Dimension size = new Dimension(1000, 700);
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	////
	private JLabel infoLabel;
	private JPanel gornyPanel, logoBox, infoBox, srodkowyPanel, menuPanel;
	private JPanel zleceniaPanel, noweZleceniePanel, ponowneZleceniePanel, listaZlecenPanel;
	private CardLayout zleceniaLayout;
	private JToggleButton noweButton, ponowneButton, zleceniaButton;
	private Dimension rozmiarMain = new Dimension(400, 600);

	public ZleceniaApp(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.pracownik = pracownik;
		this.uzytkownik = uzytkownik;
		////
		initUI();
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		// mainFrame.setMaximumSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setPreferredSize(rozmiarMain);
		initGornyPanel();
		initSrodkowyPanel();

		///
		mainFrame.pack();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new ZleceniaApp(u, p));
	}

	private void initGornyPanel() {
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
	}

	private void initSrodkowyPanel() {
		srodkowyPanel = new JPanel();
		srodkowyPanel.setLayout(new BorderLayout());
		initMenuPanel();
		addWorkPanel();
		mainFrame.add(srodkowyPanel, BorderLayout.CENTER);
	}

	private void initMenuPanel() {
		menuPanel = new JPanel();
		menuPanel.setLayout(new GridLayout(1, 0, 5, 10));

		noweButton = new JToggleButton("Nowe Zlecenie", true);
		ponowneButton = new JToggleButton("Ponowna Naprawa");
		zleceniaButton = new JToggleButton("Lista Zlecen");

		ponowneButton.setToolTipText("Nowe zlecenie na podstawie porzedniego ");
		zleceniaButton.setToolTipText("Wyswietlenie listy aktywnych zlecen");

		noweButton.addActionListener(noweButtonButtonActionListener);
		ponowneButton.addActionListener(ponowneButtonActionListener);
		zleceniaButton.addActionListener(zleceniaButtonActionListener);

		menuPanel.add(noweButton);
		menuPanel.add(ponowneButton);
		menuPanel.add(zleceniaButton);

		srodkowyPanel.add(menuPanel, BorderLayout.NORTH);
	}

	public void addWorkPanel() {
		zleceniaPanel = new JPanel();
		zleceniaLayout = new CardLayout(4, 2);
		zleceniaPanel.setLayout(zleceniaLayout);

		noweZleceniePanel = new NoweZleceniePanel(pracownik);
		ponowneZleceniePanel = new PonowneZleceniePanel(pracownik);
		listaZlecenPanel = new ZleceniaPanel();
		zleceniaPanel.add(noweZleceniePanel, "noweZlecenie");
		zleceniaPanel.add(ponowneZleceniePanel, "ponowneZlecenie");
		zleceniaPanel.add(listaZlecenPanel, "listaZlecen");
		srodkowyPanel.add(zleceniaPanel, BorderLayout.CENTER);

	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			new KlientPracownik(uzytkownik, pracownik);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};
	ActionListener noweButtonButtonActionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			noweButton.setSelected(true);
			ponowneButton.setSelected(false);
			zleceniaButton.setSelected(false);

			////
			zleceniaLayout.show(zleceniaPanel, "noweZlecenie");
		}
	};
	ActionListener ponowneButtonActionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			noweButton.setSelected(false);
			ponowneButton.setSelected(true);
			zleceniaButton.setSelected(false);

			////
			zleceniaLayout.show(zleceniaPanel, "ponowneZlecenie");
		}
	};
	ActionListener zleceniaButtonActionListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			noweButton.setSelected(false);
			ponowneButton.setSelected(false);
			zleceniaButton.setSelected(true);

			//// zaladoanie zlecen jezeli zaostaly wczytane
			((ZleceniaPanel) listaZlecenPanel).wczytajZlecenia();
			((ZleceniaPanel) listaZlecenPanel).wczytajZlecenia();
			zleceniaLayout.show(zleceniaPanel, "listaZlecen");

		}
	};

}
