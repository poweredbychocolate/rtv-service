package klientGUI.poziom2.zarzadzanieZleceniami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import klientGUI.KlientDane;
import klientGUI.zlecenieG.GraficzneZlecenie;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;
import zlecenia.Iterator.Iterator;
import zlecenia.Iterator.IteratoryListyZlecen;

class ZleceniaPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private Zlecenie zlecenie =null;
	private GniazdoKlienta gniazdo=null;
	private KartaZlecenia wybranaKarta=null;
	private Dimension rozmiarScroll = new Dimension(300, 600);
	/////
	/////
	private JPanel topBar, zleceniePanel;
	private JScrollPane zlecenieScrollPanel;
	private JLabel topLabel;
	private IteratoryListyZlecen listyZlecen;
	private GraficzneZlecenie zlecenieGr;

	ZleceniaPanel() {
		this.gniazdo=KlientDane.getGniazdoKlienta();
		this.setLayout(new BorderLayout());
		listyZlecen =new IteratoryListyZlecen(this.gniazdo);
		initTopBar();

		initKlientScrollPanel();
	}

	private void initTopBar() {
		topBar = new JPanel();
		topLabel = new JLabel("Lista Zlecen ");
		topBar.setLayout(new BoxLayout(topBar, BoxLayout.X_AXIS));

		topBar.add(Box.createHorizontalGlue());
		topBar.add(topLabel);
		topBar.add(Box.createHorizontalGlue());
		this.add(topBar, BorderLayout.NORTH);
	}
	
	private void initKlientScrollPanel() {
		zlecenieScrollPanel = new JScrollPane();
		zlecenieScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		zlecenieScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		zleceniePanel = new JPanel();
		zleceniePanel.setLayout(new GridLayout(0, 6, 4, 4));
		//wczytajZlecenia();
		zlecenieScrollPanel.getViewport().add(zleceniePanel);
		zlecenieScrollPanel.setPreferredSize(rozmiarScroll);
		this.add(zlecenieScrollPanel, BorderLayout.CENTER);
	}
	void wczytajZlecenia() {
		zleceniePanel.removeAll();
		this.validate();
		this.repaint();
		Iterator iterator;
		while (!listyZlecen.postepne()) {};
		iterator = listyZlecen.getIterator(IteratoryListyZlecen.PRZYJETE);
		while(iterator.nastepny()){
			zleceniePanel.add(new KartaZlecenia(iterator.zlecenie()));
		}
		iterator = listyZlecen.getIterator(IteratoryListyZlecen.ANULOWANE);
		while(iterator.nastepny()){
			zleceniePanel.add(new KartaZlecenia(iterator.zlecenie()));
		}
		iterator = listyZlecen.getIterator(IteratoryListyZlecen.ZAKONCZONE);
		while(iterator.nastepny()){
			zleceniePanel.add(new KartaZlecenia(iterator.zlecenie()));
		}
		this.validate();
		this.repaint();
		listyZlecen =new IteratoryListyZlecen(this.gniazdo);
	}
	

class KartaZlecenia extends JPanel {
	private JButton wybierzButton = new JButton("Wybierz");
	private Color kolor = Color.YELLOW.brighter();
	private Color kolorWybranego = Color.GREEN.brighter();
	private JLabel imieLabel, nazwiskoLabel, markaLabel,modelLabel,statusLabel,dataLabel;
	private Dimension buttonSize = new Dimension(60, 30);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Zlecenie z = null;
	private KartaZlecenia kz=null;

	KartaZlecenia(Zlecenie wczytaneZlecenie) {
		this.z = wczytaneZlecenie;

		this.setLayout(new GridLayout(0, 1));
		this.setBackground(kolor);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.kz =this;
		initElementy();
		initPanel();

	}

	private void initElementy() {
		imieLabel = new JLabel(z.getKlienta().getImie());
		nazwiskoLabel = new JLabel(z.getKlienta().getNazwisko());
		modelLabel = new JLabel(z.getUrzadzenie().getModel());
		markaLabel = new JLabel(z.getUrzadzenie().getMarka());
		statusLabel = new JLabel(z.getStatus().getClass().getSimpleName());
		dataLabel = new JLabel(z.getStatus().getDataRozpoczecia());
		////
		wybierzButton.addActionListener(wybierzListener);
		KlientDane.ustawRozmiarPrzyciskow(wybierzButton, buttonSize);
	}

	private void initPanel() {
		this.add(new JLabel("KLIENT"));
		this.add(imieLabel);
		this.add(nazwiskoLabel);
		this.add(new JLabel("URZADZENIE"));
		this.add(markaLabel);
		this.add(modelLabel);
		this.add(statusLabel);
		this.add(dataLabel);
		this.add(wybierzButton);
	}

	void reset()
	{
		this.setBackground(kolor);
	}
	ActionListener wybierzListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			//kz.setBackground(kolorWybranego);
			if(wybranaKarta==null){
				wybranaKarta=kz;
				zlecenie=z;	
				zlecenieGr=new GraficzneZlecenie(zlecenie,KlientDane.getPracownik());
			}
			else{
				wybranaKarta.reset();
				wybranaKarta=kz;
				zlecenie=z;	
				zlecenieGr.zamkijOkno();
				zlecenieGr=new GraficzneZlecenie(zlecenie,KlientDane.getPracownik());
			}
			kz.setBackground(kolorWybranego);
		}
	};

}}