package klientGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Uzytkownicy.Pracownik;
import Uzytkownicy.Uzytkownik;
import klientGUI.poziom2.MenuPoziom2;
import klientGUI.poziom3.MenuPoziom3;
import siec.GniazdoKlienta;

/**
 * Klasa z panelem obslugi pracownika
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class KlientPracownik extends JFrame {
	private Uzytkownik uzytkownik = null;
	private Pracownik pracownik = null;
	private GniazdoKlienta gniazdo = null;
	private static final long serialVersionUID = 1L;
	private Dimension size = new Dimension(600, 500);
	private static JFrame mainFrame;
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	private JLabel infoLabel;
	private JPanel gornyPanel, menuPanel, menu2Panel, menu3Panel, logoBox, infoBox;

	/**
	 * Tworzy nowy obiekt z uzyciem {@link Uzytkownicy.Uzytkownik} i
	 * {@link Uzytkownicy.Pracownik}
	 * 
	 * @param uzytkownik {@link Uzytkownicy.Uzytkownik}
	 * @param pracownik {@link Uzytkownicy.Pracownik}
	 */
	public KlientPracownik(Uzytkownik uzytkownik, Pracownik pracownik) {
		this.uzytkownik = uzytkownik;
		this.pracownik = pracownik;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		initUI();
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());
		///
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));

		if (uzytkownik.getPoziomUprawnien() == 2)
			infoBox.setBackground(Color.GREEN);
		if (uzytkownik.getPoziomUprawnien() == 3)
			infoBox.setBackground(Color.orange);

		infoLabel = new JLabel("Zalogowano jako pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu
		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
		/// tworzenie panelu z menu
		menuPanel = new JPanel();
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));

		menu2Panel = new MenuPoziom2();
		// menu2Panel.setBackground(Color.lightGray);
		menuPanel.add(menu2Panel);
		if (uzytkownik.getPoziomUprawnien() == 3) {
			menu3Panel = new MenuPoziom3();
			// menu3Panel.setBackground(Color.lightGray);
			menuPanel.add(menu3Panel);
		}

		menuPanel.add(Box.createVerticalGlue());
		mainFrame.add(menuPanel, BorderLayout.CENTER);
		///
		mainFrame.pack();
	}

	public static void main(Uzytkownik u, Pracownik p) {
		SwingUtilities.invokeLater(() -> new KlientPracownik(u, p));
	}

	public static void zamkijOkno() {
		KlientPracownik.mainFrame.dispose();
	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			try {
				gniazdo.zamknij();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};
}
