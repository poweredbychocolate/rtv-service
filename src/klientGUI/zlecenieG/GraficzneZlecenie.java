package klientGUI.zlecenieG;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import Uzytkownicy.Pracownik;
import klientGUI.KlientDane;
import siec.GniazdoKlienta;
import zlecenia.Zlecenie;
import zlecenia.statusy.Anulowane;
import zlecenia.statusy.Status;
import zlecenia.statusy.Zakonczone;

/**
 * Graficzna reprezentacjia {@link zlecenia.Zlecenie}
 * 
 * @author Dawid Brelak
 * @version 1.0
 */

public class GraficzneZlecenie extends JFrame {
	private GniazdoKlienta gniazdo = null;
	private Zlecenie zlecenie = null;
	private Pracownik pracownik = null;
	private Boolean trybPracownika = false;
	private Color kolor1 = Color.GRAY;
	private Color kolor2 = Color.LIGHT_GRAY;
	////
	private static final long serialVersionUID = 1L;
	private Dimension size = new Dimension(800, 600);
	private static JFrame mainFrame;
	private JPanel mainPanel;
	private GraficzneCzynnosci gcz;

	///
	/**
	 * Tworzy obiekt w trybie pracownika
	 * 
	 * @param zlecenie
	 *            {@link zlecenia.Zlecenie}
	 * @param pracownik
	 *            {@link Uzytkownicy.Pracownik}
	 */
	public GraficzneZlecenie(Zlecenie zlecenie, Pracownik pracownik) {
		this.zlecenie = zlecenie;
		this.pracownik = pracownik;
		this.trybPracownika = true;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	/**
	 * Tworzy obiekt w trybie klienta
	 * 
	 * @param zlecenie
	 *            {@link zlecenia.Zlecenie}
	 */
	public GraficzneZlecenie(Zlecenie zlecenie) {
		this.zlecenie = zlecenie;
		this.trybPracownika = false;
		this.gniazdo = KlientDane.getGniazdoKlienta();
		////
		initUI();
	}

	private void initUI() {
		mainFrame = this;
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		mainFrame.setPreferredSize(size);
		mainFrame.setResizable(true);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		peneleEtykiet();
		///
		mainFrame.add(mainPanel);
		mainFrame.pack();
	}

	private void peneleEtykiet() {
		mainPanel.add(new ZlecenieKlient());
		// mainPanel.add(Box.createVerticalStrut(5));
		mainPanel.add(new ZlecenieUrzadzenie());
		// mainPanel.add(Box.createVerticalStrut(5));
		mainPanel.add(new ZlecenieSzczeguly());
		mainPanel.add(Box.createVerticalGlue());
	}

	public static void main(Zlecenie z, Pracownik p) {
		SwingUtilities.invokeLater(() -> new GraficzneZlecenie(z, p));
	}

	public static void main(Zlecenie z) {
		SwingUtilities.invokeLater(() -> new GraficzneZlecenie(z));
	}

	/**
	 * Zamyka okno zlecenia zapisujac wprowadzone czynnosci serwisowe
	 */
	public void zamkijOkno() {
		gcz.zapiszCzynnosci();
		gcz = null;
		mainFrame.dispose();
	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			if (gcz != null)
				gcz.zapiszCzynnosci();
			super.windowClosing(e);
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};

	private class ZlecenieUrzadzenie extends JPanel {
		private static final long serialVersionUID = 1L;
		private JPanel infoP, urzadzenieP;
		private JLabel infoL, markaL, modelL;

		ZlecenieUrzadzenie() {
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.setBackground(kolor2);
			this.infoL = new JLabel("Urzadzenie");
			this.infoL.setForeground(Color.BLUE);
			this.markaL = new JLabel(zlecenie.getUrzadzenie().getMarka());
			this.modelL = new JLabel(zlecenie.getUrzadzenie().getModel());

			this.infoP = new JPanel();
			this.infoP.setLayout(new BoxLayout(this.infoP, BoxLayout.X_AXIS));
			this.infoP.setBackground(kolor2);
			this.urzadzenieP = new JPanel();
			this.urzadzenieP.setLayout(new BoxLayout(this.urzadzenieP, BoxLayout.X_AXIS));
			this.urzadzenieP.setBackground(kolor2);

			this.infoP.add(Box.createHorizontalGlue());
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(infoL);
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(Box.createHorizontalGlue());

			this.urzadzenieP.add(Box.createHorizontalGlue());
			this.urzadzenieP.add(Box.createHorizontalStrut(5));
			this.urzadzenieP.add(this.markaL);
			this.urzadzenieP.add(Box.createHorizontalStrut(2));
			this.urzadzenieP.add(this.modelL);
			this.urzadzenieP.add(Box.createHorizontalStrut(5));
			this.urzadzenieP.add(Box.createHorizontalGlue());

			// this.add(Box.createVerticalStrut(2));
			this.add(this.infoP);
			// this.add(Box.createVerticalStrut(1));
			this.add(this.urzadzenieP);
			// this.add(Box.createVerticalStrut(2));
		}
	}

	private class ZlecenieKlient extends JPanel {
		private static final long serialVersionUID = 1L;
		private JPanel infoP, klientP;
		private JLabel infoL, nazwiskoL, imieL;

		ZlecenieKlient() {
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.setBackground(kolor1);
			this.infoL = new JLabel("Klient");
			this.infoL.setForeground(Color.BLUE);
			this.imieL = new JLabel(zlecenie.getKlienta().getImie());
			this.nazwiskoL = new JLabel(zlecenie.getKlienta().getNazwisko());

			this.infoP = new JPanel();
			this.infoP.setLayout(new BoxLayout(this.infoP, BoxLayout.X_AXIS));
			this.infoP.setBackground(kolor1);
			this.klientP = new JPanel();
			this.klientP.setLayout(new BoxLayout(this.klientP, BoxLayout.X_AXIS));
			this.klientP.setBackground(kolor1);

			this.infoP.add(Box.createHorizontalGlue());
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(infoL);
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(Box.createHorizontalGlue());

			this.klientP.add(Box.createHorizontalGlue());
			this.klientP.add(Box.createHorizontalStrut(5));
			this.klientP.add(this.imieL);
			this.klientP.add(Box.createHorizontalStrut(2));
			this.klientP.add(this.nazwiskoL);
			this.klientP.add(Box.createHorizontalStrut(5));
			this.klientP.add(Box.createHorizontalGlue());

			// this.add(Box.createVerticalStrut(2));
			this.add(this.infoP);
			// this.add(Box.createVerticalStrut(1));
			this.add(this.klientP);
			// this.add(Box.createVerticalStrut(2));
		}
	}

	private class ZlecenieSzczeguly extends JPanel {
		private static final long serialVersionUID = 1L;
		private ArrayList<Status> statusy;
		private JPanel infoP, dataP, historiaP;
		private JLabel infoL, dataRL, dataZL;
		private JPanel tmp;
		private JButton przycisk;

		ZlecenieSzczeguly() {
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			this.setBackground(kolor2);
			initPanele();
			infoPanel();
			dataPanel();
			historiaPanel();
			glownyPanel();

		}

		private void initPanele() {
			this.infoP = new JPanel();
			this.infoP.setLayout(new BoxLayout(this.infoP, BoxLayout.X_AXIS));
			this.infoP.setBackground(kolor1);
			this.dataP = new JPanel();
			this.dataP.setLayout(new BoxLayout(this.dataP, BoxLayout.X_AXIS));
			this.dataP.setBackground(kolor1);
			this.historiaP = new JPanel();
			this.historiaP.setLayout(new BoxLayout(historiaP, BoxLayout.Y_AXIS));
			this.historiaP.setBackground(kolor2);
		}

		private void infoPanel() {
			this.infoL = new JLabel("Dodatkowe informacje");
			this.infoL.setForeground(Color.BLUE);
			this.infoP.add(Box.createHorizontalGlue());
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(infoL);
			this.infoP.add(Box.createHorizontalStrut(5));
			this.infoP.add(Box.createHorizontalGlue());
		}

		private void dataPanel() {
			this.statusy = gniazdo.statusyzlecenia(zlecenie.getZlecenieId());
			this.dataRL = new JLabel(statusy.get(0).getDataRozpoczecia());
			this.dataZL = new JLabel(statusy.get(statusy.size() - 1).getDataRozpoczecia());
			this.dataP.add(Box.createHorizontalGlue());
			this.dataP.add(Box.createHorizontalStrut(5));
			this.dataP.add(new JLabel("Data rozpoczecia : "));
			this.dataP.add(Box.createHorizontalStrut(2));
			this.dataP.add(this.dataRL);
			this.dataP.add(Box.createHorizontalStrut(2));
			this.dataP.add(new JLabel("Data zakonczenia : "));
			this.dataP.add(Box.createHorizontalStrut(2));
			this.dataP.add(this.dataZL);
			this.dataP.add(Box.createHorizontalStrut(2));
			this.dataP.add(Box.createHorizontalGlue());
		}

		private void historiaPanel() {

			tmp = new JPanel();
			tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
			tmp.setBackground(kolor2);
			tmp.add(Box.createHorizontalStrut(2));
			JLabel tmpLabel = new JLabel("Historia Zlecenia ");
			tmpLabel.setForeground(Color.BLUE);
			tmp.add(tmpLabel);
			tmp.add(Box.createHorizontalStrut(2));
			historiaP.add(tmp);
			// historiaP.add(Box.createVerticalStrut(2));
			int i = 0;
			for (Status st : statusy) {
				tmp = new JPanel();
				tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
				tmp.setBackground(kolor2);
				String stmp = new String(
						(++i) + ". " + st.getClass().getSimpleName() + " : " + st.getDataRozpoczecia() + ", ");
				tmp.add(new JLabel(stmp));
				tmp.add(Box.createHorizontalStrut(2));
				historiaP.add(tmp);
				// historiaP.add(Box.createVerticalStrut(2));
			}
			if (trybPracownika
					&& (!(zlecenie.getStatus() instanceof Zakonczone || zlecenie.getStatus() instanceof Anulowane))) {
				tmp = new JPanel();
				tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
				tmp.setBackground(kolor2);
				ActionListener przyciskAL = new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Zakonczone zak = new Zakonczone();
						zak.setDataRozpoczecia(KlientDane.getBiezacyCzas());
						zak.setPracownik(pracownik);
						if (gniazdo.zmienStatusZlecenia(zlecenie, zak))
							JOptionPane.showMessageDialog(historiaP, "Zmieniono Status Zlecenia");
						historiaP.remove(tmp);
						historiaP.validate();
						historiaP.repaint();
						
					}
				};

				tmp.add(Box.createHorizontalStrut(2));
				tmp.add(new JLabel("Zmien Status "));
				tmp.add(Box.createHorizontalStrut(2));
				przycisk = new JButton("Zakoncz");
				przycisk.addActionListener(przyciskAL);
				tmp.add(przycisk);
				tmp.add(Box.createHorizontalStrut(2));

				historiaP.add(tmp);
				// historiaP.add(Box.createVerticalStrut(2));

			}
			if (!trybPracownika
					&& (!(zlecenie.getStatus() instanceof Zakonczone || zlecenie.getStatus() instanceof Anulowane))) {
				tmp = new JPanel();
				tmp.setLayout(new BoxLayout(tmp, BoxLayout.X_AXIS));
				tmp.setBackground(kolor2);
				ActionListener przyciskAL = new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Anulowane anu = new Anulowane();
						anu.setDataRozpoczecia(KlientDane.getBiezacyCzas());
						anu.setPracownik(pracownik);
						if (gniazdo.anulujZlecenie(zlecenie.getKlienta(), zlecenie))
							JOptionPane.showMessageDialog(historiaP, "Zlecenie anulowane przez klienta");
						historiaP.remove(tmp);
						historiaP.validate();
						historiaP.repaint();
					}
				};

				tmp.add(Box.createHorizontalStrut(2));
				tmp.add(new JLabel("Zmien Status "));
				tmp.add(Box.createHorizontalStrut(2));
				przycisk = new JButton("Anuluj Zlecenie");
				przycisk.addActionListener(przyciskAL);
				tmp.add(przycisk);
				tmp.add(Box.createHorizontalStrut(2));

				historiaP.add(tmp);
				// historiaP.add(Box.createVerticalStrut(2));
			}

		}

		private void glownyPanel() {
			// this.add(Box.createVerticalStrut(5));
			this.add(this.infoP);
			// this.add(Box.createVerticalStrut(5));
			this.add(this.dataP);
			// this.add(Box.createVerticalStrut(2));
			this.add(this.historiaP);
			// this.add(Box.createVerticalStrut(2));
			gcz = new GraficzneCzynnosci(zlecenie, gniazdo, trybPracownika);
			this.add(gcz);
			// this.add(Box.createVerticalStrut(2));
		}
	}

}
