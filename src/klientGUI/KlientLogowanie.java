package klientGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import database.DBMenadzer;
import siec.GniazdoKlienta;

/**
 * Klasa z panelem logowania
 * 
 * @author Dawid Brelak
 * @version 1.0
 */
public class KlientLogowanie extends JFrame {
	private static final long serialVersionUID = 1L;
	private Dimension size = new Dimension(600, 400);
	private Dimension fieldSize = new Dimension(400, 30);
	private JFrame mainFrame;
	private JLabel serwisLabel = new JLabel("Serwis RTV");
	private JLabel infoLabel = new JLabel("Logowanie do systemu");
	private JLabel loginLabel = new JLabel("Login :");
	private JTextField loginField = new JTextField(30);
	private JLabel hasloLabel = new JLabel("Haslo :");
	private JPasswordField hasloField = new JPasswordField(30);
	private JPanel gornyPanel, logowaniePanel, logoBox, infoBox, loginBox, hasloBox, przyciskiBox;
	private JButton zalogujButton = new JButton("Zaloguj");
	////

	private Socket klientSocket = null;
	private GniazdoKlienta gniazdo = null;

	public KlientLogowanie() {
		try {
			DBMenadzer.getInstance();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			klientSocket = new Socket(KlientDane.getLokalnySerwerInetAdress(), KlientDane.getSerwerPort());
		} catch (IOException e) {
			try {
				klientSocket = new Socket(KlientDane.getLokalnySerwerInetAdress(), KlientDane.getSerwerPort());
			} catch (IOException e1) {
				new KlientBrakDostepu();
				mainFrame.dispose();
			}
		}
		try {
			gniazdo = new GniazdoKlienta(klientSocket);
		} catch (IOException e) {
			e.printStackTrace();
		}
		KlientDane.setGniazdoKlienta(gniazdo);
		initUI();
		BottonAction();
	}

	private void BottonAction() {
		zalogujButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Boolean dostep = false;
				Integer id = -100;
				String login = loginField.getText();
				String haslo = new String(hasloField.getPassword());
				Uzytkownicy.Uzytkownik u = null;
				Uzytkownicy.Pracownik p = null;
				Uzytkownicy.Klient k = null;
				////
				dostep = gniazdo.sprawdzUzytkownika(login, haslo);
				////
				if (!dostep) {
					// brak uzytkownika
					infoBox.setBackground(Color.RED.brighter());
					infoLabel.setText("Niepoprawne dane logowania");
					loginField.setText("");
					hasloField.setText("");
				} else {
					// wczytanie uzytkownika z bazy i wybor okna

					infoBox.setBackground(Color.GREEN);

					id = gniazdo.sprawdzDaneLogowania(login, haslo);
					u = gniazdo.wczytajUzytkownika(id);

					if (u.isKlient()) {
						k = gniazdo.wczytajKlienta(id);
						// k = bazaDanych.wczytajKlienta(id);
						infoLabel.setText("Logowanie jako " + k.getImie() + " " + k.getNazwisko());

						new KlientKlient(u, k);
						mainFrame.dispose();

					} else {
						p = gniazdo.wczytajPracownika(id);
						if (p.getAktywny()) {
							infoLabel.setText("Logowanie jako " + p.getImie() + " " + p.getNazwisko());
							KlientDane.setPracownik(p);
							KlientDane.setUzytkownik(u);
							new KlientPracownik(u, p);
							mainFrame.dispose();
						} else {
							infoLabel.setText("Odmowa dostepu : By�y Pracownik");
							loginField.setText("");
							hasloField.setText("");
						}
					}

					// tak zamyak si� okno mainFrame.dispose();

				}
			}
		});
	}

	private void initUI() {
		mainFrame = new JFrame();
		mainFrame.setSize(size);
		mainFrame.setMinimumSize(size);
		mainFrame.setMaximumSize(size);
		mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(wa);
		mainFrame.setVisible(true);
		mainFrame.setLayout(new BorderLayout());
		///
		gornyPanel = new JPanel();
		gornyPanel.setLayout(new BoxLayout(gornyPanel, BoxLayout.Y_AXIS));
		//// dodanie loga
		logoBox = new JPanel();
		logoBox.setLayout(new BoxLayout(logoBox, BoxLayout.X_AXIS));
		logoBox.setBackground(Color.BLUE.brighter());
		serwisLabel.setFont(new Font(Font.SERIF, Font.BOLD, 50));
		logoBox.add(Box.createHorizontalGlue());
		logoBox.add(serwisLabel);
		logoBox.add(Box.createHorizontalGlue());
		//// dodanie panelu informacujnego
		infoBox = new JPanel();
		infoBox.setLayout(new BoxLayout(infoBox, BoxLayout.X_AXIS));
		infoBox.setBackground(Color.lightGray);
		infoLabel.setFont(new Font(Font.SERIF, Font.ITALIC, 25));
		infoBox.add(Box.createHorizontalStrut(10));
		infoBox.add(infoLabel);
		infoBox.add(Box.createHorizontalGlue());
		//// zlozenie gornego panelu

		gornyPanel.add(logoBox);
		gornyPanel.add(infoBox);
		gornyPanel.add(Box.createVerticalStrut(5));
		mainFrame.add(gornyPanel, BorderLayout.NORTH);
		///
		logowaniePanel = new JPanel();
		logowaniePanel.setLayout(new BoxLayout(logowaniePanel, BoxLayout.Y_AXIS));
		//// wprowadzanie loginu
		loginBox = new JPanel();
		loginBox.setLayout(new BoxLayout(loginBox, BoxLayout.X_AXIS));
		loginBox.add(Box.createHorizontalGlue());
		loginBox.add(loginLabel);
		loginBox.add(Box.createHorizontalStrut(5));
		loginField.setPreferredSize(fieldSize);
		loginField.setMinimumSize(fieldSize);
		loginField.setMaximumSize(fieldSize);
		loginBox.add(loginField);
		loginBox.add(Box.createHorizontalGlue());
		//// wprowadzanie hasla
		hasloBox = new JPanel();
		hasloBox.setLayout(new BoxLayout(hasloBox, BoxLayout.X_AXIS));
		hasloBox.add(Box.createHorizontalGlue());
		hasloBox.add(hasloLabel);
		hasloBox.add(Box.createHorizontalStrut(5));
		hasloField.setPreferredSize(fieldSize);
		hasloField.setMinimumSize(fieldSize);
		hasloField.setMaximumSize(fieldSize);
		hasloBox.add(hasloField);
		hasloBox.add(Box.createHorizontalGlue());

		//// elemant z przyciskami
		przyciskiBox = new JPanel();
		przyciskiBox.setLayout(new BoxLayout(przyciskiBox, BoxLayout.X_AXIS));
		przyciskiBox.add(Box.createHorizontalGlue());
		przyciskiBox.add(zalogujButton);
		przyciskiBox.add(Box.createHorizontalStrut(110));
		//// skladanie elemantu z logowaniem
		logowaniePanel.add(Box.createVerticalGlue());
		logowaniePanel.add(Box.createVerticalStrut(3));
		logowaniePanel.add(loginBox);
		logowaniePanel.add(hasloBox);
		logowaniePanel.add(przyciskiBox);
		logowaniePanel.add(Box.createVerticalGlue());
		mainFrame.add(logowaniePanel, BorderLayout.CENTER);
		mainFrame.pack();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new KlientLogowanie());
	}

	WindowAdapter wa = new WindowAdapter() {

		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			try {
				gniazdo.zamknij();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			mainFrame.dispose();
		}

		public void windowClosed(WindowEvent e) {
			super.windowClosed(e);

		}
	};

}
