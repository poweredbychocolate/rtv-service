CREATE TABLE Uzytkownicy
(
UzytkownikId NUMBER(10) NOT NULL,
Login VARCHAR2(30) UNIQUE NOT NULL,
Haslo VARCHAR2(30) NOT NULL,
Uprawnienia NUMBER(1) DEFAULT 1 NOT NULL,
PRIMARY KEY (UzytkownikId),
CONSTRAINT uprawnienia_CH CHECK (Uprawnienia BETWEEN 1 AND 3)
);

CREATE TABLE Cennik
(
CennikId NUMBER(2) NOT NULL,
Czynnosc VARCHAR2(1000) NOT NULL,
Cenna NUMBER(6,2) NOT NULL,
Czas NUMBER(3) NOT NULL,
PRIMARY KEY (CennikId)
);

CREATE TABLE Pracownicy
(
PracownikId NUMBER(4) NOT NULL,
Imie VARCHAR2(30) NOT NULL,
Nazwisko VARCHAR2(30) NOT NULL,
Aktywny VARCHAR2(3) DEFAULT 'TAK' NOT NULL,
PRIMARY KEY (PracownikId),
CONSTRAINT aktywny_pCH CHECK (Aktywny IN('TAK','NIE'))
);
CREATE TABLE Klienci
(
KlientId NUMBER(4) NOT NULL,
Imie VARCHAR2(30) NOT NULL,
Nazwisko VARCHAR2(30) NOT NULL,
Telefon VARCHAR2(9) NOT NULL,
PRIMARY KEY (KlientId)
);

CREATE TABLE Urzadzenia
(
UrzadzenieId NUMBER(5) NOT NULL,
Marka VARCHAR2(100) NOT NULL,
Model_Urzadzenia VARCHAR2(100) NOT NULL,
Ilosc_Serwisowanych NUMBER(5) DEFAULT 1 NOT NULL,
PRIMARY KEY (UrzadzenieId)
);
CREATE TABLE Statusy
(
StatusId NUMBER(12) NOT NULL,
Data_Rozpoczecia DATE DEFAULT(SYSDATE) NOT NULL,
Status VARCHAR2(12) DEFAULT 'PRZYJETE' NOT NULL ,
PracownikId NUMBER(4) NOT NULL,
Data_Zakonczenia DATE, 
PRIMARY KEY(StatusId),
CONSTRAINT pracownik_sFK FOREIGN KEY (PracownikId) REFERENCES Pracownicy(PracownikId),
CONSTRAINT status_sCH CHECK (Status IN('PRZYJETE','PO_DIAGNOZIE','W_REALIZACJI','ANULOWANE','ZAKONCZONE'))
);

CREATE TABLE Zlecenia
(
ZlecenieId NUMBER(10) NOT NULL,
KlientId NUMBER(4) NOT NULL,
UrzadzenieId NUMBER(5) NOT NULL,
Gwarancja VARCHAR2(3) DEFAULT 'NIE' NOT NULL,
PRIMARY KEY (ZlecenieId),
CONSTRAINT klient_zFK FOREIGN KEY (KlientId) REFERENCES Klienci(KlientId),
CONSTRAINT urzadzenie_zFK FOREIGN KEY (UrzadzenieId) REFERENCES Urzadzenia(UrzadzenieId),
CONSTRAINT gwarancja_zCH CHECK (Gwarancja IN('TAK','NIE'))
);
CREATE TABLE Statusy_Zlecenia
(
ZlecenieId NUMBER(10) NOT NULL,
StatusId NUMBER(12) NOT NULL,
PRIMARY KEY (ZlecenieId,StatusId),
CONSTRAINT zlecenie_szFK FOREIGN KEY (ZlecenieId) REFERENCES Zlecenia(ZlecenieId),
CONSTRAINT status_szFK FOREIGN KEY (StatusId) REFERENCES Statusy(StatusId)
);
CREATE TABLE Czynnosci
(
CzynnoscId NUMBER(10) NOT NULL,
ZlecenieId NUMBER(10) NOT NULL,
Typ VARCHAR2(8) NOT NULL,
Opis VARCHAR2(1000) NOT NULL,
Koszt NUMBER(5) NOT NULL,
PRIMARY KEY (CzynnoscId),
CONSTRAINT typ_cCH CHECK (Typ IN('DIAGNOZA','NAPRAWA')),
CONSTRAINT czynnosci_czFK FOREIGN KEY (ZlecenieId) REFERENCES Zlecenia(ZlecenieId)
);

CREATE SEQUENCE Uzytkownik_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE Cennik_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE Czynnosc_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE Urzadzenia_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE Zlecenie_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE SEQUENCE Status_SEQ 
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

CREATE OR REPLACE PROCEDURE Dodaj_Pracownika
(pracownikId OUT NUMBER, imie IN VARCHAR2,nazwisko IN VARCHAR2,login IN VARCHAR2,haslo IN VARCHAR2,uprawnienia IN NUMBER)
AS
  up NUMERIC;
BEGIN
  IF uprawnienia =3 
    THEN up:=3;
    ELSE up:=2;
  END IF;
  INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,login,haslo,up);
  INSERT INTO Pracownicy VALUES(Uzytkownik_SEQ.CURRVAL,Imie,Nazwisko,'TAK');
  pracownikId:=Uzytkownik_SEQ.CURRVAL;
END Dodaj_Pracownika;
/

CREATE OR REPLACE PROCEDURE insert_root(imie IN VARCHAR2, nazwisko IN VARCHAR2)
AS
BEGIN
  INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,'root','root',3);
  INSERT INTO Pracownicy VALUES(Uzytkownik_SEQ.CURRVAL,imie,nazwisko,'TAK');
END insert_root;
/
CREATE OR REPLACE PROCEDURE Dodaj_Klienta
(klientId OUT NUMBER ,imie IN VARCHAR2,nazwisko IN VARCHAR2,login IN VARCHAR2,haslo IN VARCHAR2,telefon IN VARCHAR2)
AS
BEGIN
  IF LENGTH(telefon)=9
  THEN
    INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,login,haslo,'1');
    INSERT INTO Klienci VALUES(Uzytkownik_SEQ.CURRVAL,Imie,Nazwisko,telefon);
    klientId:=Uzytkownik_SEQ.CURRVAL;
  END IF;
END Dodaj_Klienta;
/
CREATE OR REPLACE PROCEDURE Dodaj_Status
(statusId OUT NUMBER,status IN VARCHAR2,data_Rozpoczecia IN VARCHAR2,pracownikId IN NUMBER)
AS
BEGIN
  INSERT INTO Statusy(StatusId,Data_Rozpoczecia,Status,PracownikId) VALUES(Status_SEQ.NEXTVAL,data_Rozpoczecia,status,pracownikId);
  statusId:=Status_SEQ.CURRVAL;
END Dodaj_Status;
/
CREATE OR REPLACE PROCEDURE Dodaj_Urzadenie
(marka IN VARCHAR2,model_urzadzenia IN VARCHAR2)
AS
BEGIN
  INSERT INTO URZADZENIA(URZADZENIEID,MARKA,MODEL_URZADZENIA) VALUES(Urzadzenia_SEQ.NEXTVAL,marka,model_urzadzenia);
END Dodaj_Urzadenie;
/
CREATE OR REPLACE PROCEDURE Dodaj_Zlecenie
(zlecenieId OUT NUMBER,klientId IN NUMBER,urzadzenieId IN NUMBER,statusId IN NUMBER,gwarancja IN VARCHAR2)
AS
BEGIN
  INSERT INTO Zlecenia(ZlecenieId ,KlientId,UrzadzenieId,Gwarancja) VALUES(Zlecenie_SEQ.NEXTVAL,klientId,urzadzenieId,gwarancja);
  INSERT INTO Statusy_Zlecenia VALUES(Zlecenie_SEQ.CURRVAL,statusId);
  zlecenieId:=Zlecenie_SEQ.CURRVAL;
END Dodaj_Zlecenie;
/
CREATE OR REPLACE PROCEDURE Dodaj_Pozycje_Cennika
(usluga IN VARCHAR2,koszt IN NUMBER,czas_realizacji IN NUMBER)
AS
BEGIN
  INSERT INTO Cennik(CennikId, Czynnosc, Cenna, Czas) VALUES(Cennik_SEQ.NEXTVAL,usluga,koszt,czas_realizacji);
END Dodaj_Pozycje_Cennika;
/
CREATE OR REPLACE PROCEDURE Dodaj_Czynnosc(Typ IN VARCHAR2,Opis IN VARCHAR2, Koszt IN NUMBER, ZlecenieId IN NUMBER)
AS
BEGIN
  INSERT INTO Czynnosci VALUES(Czynnosc_SEQ.NEXTVAL,ZlecenieId,Typ,Opis,Koszt);
  END Dodaj_Czynnosc;
/
CALL insert_root('Wlasciciel','Serwisu');
COMMIT;