CREATE OR REPLACE PROCEDURE Dodaj_Pracownika
(pracownikId OUT NUMBER, imie IN VARCHAR2,nazwisko IN VARCHAR2,login IN VARCHAR2,haslo IN VARCHAR2,uprawnienia IN NUMBER)
AS
  up NUMERIC;
BEGIN
  IF uprawnienia =3 
    THEN up:=3;
    ELSE up:=2;
  END IF;
  INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,login,haslo,up);
  INSERT INTO Pracownicy VALUES(Uzytkownik_SEQ.CURRVAL,Imie,Nazwisko,'TAK');
  pracownikId:=Uzytkownik_SEQ.CURRVAL;
END Dodaj_Pracownika;
/

CREATE OR REPLACE PROCEDURE insert_root(imie IN VARCHAR2, nazwisko IN VARCHAR2)
AS
BEGIN
  INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,'root','root',3);
  INSERT INTO Pracownicy VALUES(Uzytkownik_SEQ.CURRVAL,imie,nazwisko,'TAK');
END insert_root;
/
CREATE OR REPLACE PROCEDURE Dodaj_Klienta
(klientId OUT NUMBER ,imie IN VARCHAR2,nazwisko IN VARCHAR2,login IN VARCHAR2,haslo IN VARCHAR2,telefon IN VARCHAR2)
AS
BEGIN
  IF LENGTH(telefon)=9
  THEN
    INSERT INTO Uzytkownicy VALUES(Uzytkownik_SEQ.NEXTVAL,login,haslo,'1');
    INSERT INTO Klienci VALUES(Uzytkownik_SEQ.CURRVAL,Imie,Nazwisko,telefon);
    klientId:=Uzytkownik_SEQ.CURRVAL;
  END IF;
END Dodaj_Klienta;
/
CREATE OR REPLACE PROCEDURE Dodaj_Status
(statusId OUT NUMBER,status IN VARCHAR2,data_Rozpoczecia IN VARCHAR2,pracownikId IN NUMBER)
AS
BEGIN
  INSERT INTO Statusy(StatusId,Data_Rozpoczecia,Status,PracownikId) VALUES(Status_SEQ.NEXTVAL,data_Rozpoczecia,status,pracownikId);
  statusId:=Status_SEQ.CURRVAL;
END Dodaj_Status;
/
CREATE OR REPLACE PROCEDURE Dodaj_Urzadenie
(marka IN VARCHAR2,model_urzadzenia IN VARCHAR2)
AS
BEGIN
  INSERT INTO URZADZENIA(URZADZENIEID,MARKA,MODEL_URZADZENIA) VALUES(Urzadzenia_SEQ.NEXTVAL,marka,model_urzadzenia);
END Dodaj_Urzadenie;
/
CREATE OR REPLACE PROCEDURE Dodaj_Zlecenie
(zlecenieId OUT NUMBER,klientId IN NUMBER,urzadzenieId IN NUMBER,statusId IN NUMBER,gwarancja IN VARCHAR2)
AS
BEGIN
  INSERT INTO Zlecenia(ZlecenieId ,KlientId,UrzadzenieId,Gwarancja) VALUES(Zlecenie_SEQ.NEXTVAL,klientId,urzadzenieId,gwarancja);
  INSERT INTO Statusy_Zlecenia VALUES(Zlecenie_SEQ.CURRVAL,statusId);
  zlecenieId:=Zlecenie_SEQ.CURRVAL;
END Dodaj_Zlecenie;
/
CREATE OR REPLACE PROCEDURE Dodaj_Pozycje_Cennika
(usluga IN VARCHAR2,koszt IN NUMBER,czas_realizacji IN NUMBER)
AS
BEGIN
  INSERT INTO Cennik(CennikId, Czynnosc, Cenna, Czas) VALUES(Cennik_SEQ.NEXTVAL,usluga,koszt,czas_realizacji);
END Dodaj_Pozycje_Cennika;
/
CREATE OR REPLACE PROCEDURE Dodaj_Czynnosc(Typ IN VARCHAR2,Opis IN VARCHAR2, Koszt IN NUMBER, ZlecenieId IN NUMBER)
AS
BEGIN
  INSERT INTO Czynnosci VALUES(Czynnosc_SEQ.NEXTVAL,ZlecenieId,Typ,Opis,Koszt);
  END Dodaj_Czynnosc;
/
