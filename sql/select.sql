SELECT * FROM Uzytkownicy;
SELECT * FROM Pracownicy;
SELECT * FROM Klienci;

SELECT * FROM Urzadzenia;
SELECT * FROM Statusy;
SELECT * FROM Zlecenia;
SELECT * FROM Statusy_Zlecenia;

SELECT * FROM Cennik;
SELECT * FROM Czynnosci;

SELECT ZlecenieId,Typ,Opis,Koszt FROM Czynnosci WHERE ZlecenieId =59 ORDER BY Typ ASC;

SELECT Imie,Nazwisko,Aktywny FROM Pracownicy ORDER BY Nazwisko ASC;

SELECT MAX(Statusy.StatusId) FROM Statusy,Statusy_Zlecenia,Zlecenia
WHERE Statusy_Zlecenia.StatusId = Statusy.StatusId
AND Statusy_Zlecenia.ZlecenieId = Zlecenia.ZlecenieId
GROUP BY Zlecenia.ZlecenieId;

SELECT COUNT(zl.ZlecenieId)
FROM Klienci kl, Zlecenia zl
WHERE kl.KlientId=zl.KlientId
GROUP BY kl.KlientId;

SELECT COUNT(zl.ZlecenieId)
FROM Klienci kl, Zlecenia zl
WHERE kl.KlientId=zl.KlientId
AND kl.KLIENTID=21
GROUP BY kl.KlientId;

SELECT zl.ZlecenieId, kl.Imie ,kl.Nazwisko, urz.Marka, urz.Model_Urzadzenia, zl.Gwarancja, sta.StatusID, sta.PracownikId, sta.Status, sta.Data_Rozpoczecia
FROM Zlecenia zl, Klienci kl, Urzadzenia urz, Statusy sta, Statusy_Zlecenia stz
WHERE zl.KlientId = kl.KlientId
AND zl.UrzadzenieId = urz.UrzadzenieId
AND zl.ZlecenieId = stz.ZlecenieId
AND stz.StatusId = sta.StatusId
AND sta.StatusId IN ( SELECT MAX (Statusy.StatusId) FROM Statusy, Statusy_Zlecenia, Zlecenia
                      WHERE Statusy_Zlecenia.StatusId = Statusy.StatusId
                       AND Statusy_Zlecenia.ZlecenieId = Zlecenia.ZlecenieId
                      GROUP BY Zlecenia.ZlecenieId )
ORDER BY  sta.Status ASC, kl.Imie ASC, kl.Nazwisko ASC;

INSERT INTO Statusy VALUES (Status_SEQ.NEXTVAL,SYSDATE,'ZAKONCZONE',1,SYSDATE);
INSERT INTO Statusy_Zlecenia VALUES(4,Status_SEQ.CURRVAL);
CALL Dodaj_Czynnosc('DIAGNOZA','Rozpeknol sie','300', 16);

SELECT ZlecenieId,KlientId,UrzadzenieId,Gwarancja FROM Zlecenia WHERE ZlecenieId = 11;

SELECT Marka,SUM(Ilosc_Serwisowanych) AS Ilosc
FROM Urzadzenia
GROUP BY Marka
ORDER BY Marka ASC;

SELECT s.Data_Rozpoczecia,s.Status,s.PracownikId,s.Data_Zakonczenia
FROM Zlecenia z,Statusy s ,Statusy_Zlecenia sz
WHERE z.ZlecenieId = sz.ZlecenieId
AND s.StatusId = sz.StatusId
ORDER BY s.Data_Rozpoczecia ASC, s.Status ASC;